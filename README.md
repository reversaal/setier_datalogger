# SETIER : Suivi et Evaluation des capTeurs low-cost pour les Installations de traitement des Eaux usées : qualité des Eaux et énErgie


En partenariat avec l'Agence de l'eau Rhône-Méditérranée-Corse, INRAE et l'IRD ont initié le projet SETIER en 2019. Coordonné par INRAE (Stéphanie Prost-Boucle et Rémi Clément), le projet Setier s'appuie sur le développement mondial de l'électronique open source et des capteurs low-cost, miniaturisés et portatifs. Cette tendance rend les mesures autour de la qualité de l’eau (conductivité, pH, turbidité, DCO, température, redox, O2 dissous, énergie) de plus en plus accessibles. Leur développement ouvre de nouvelles perspectives en matière de suivi et de pilotage des installations de traitement et valorisation des eaux résiduaires grâce à une caractérisation spatio-temporelle plus détaillée. Ce type d’approche, dite « low-tech » se développe considérablement dans d’autres domaines d’applications environnementales, par exemple en pollution de l’air et en hydrologie. L'avantage majeur de ces outils open source est d'offrir une forte flexibilité (choix des mesures, nombre de capteurs, fréquence de mesure), mais également de s'inclure dans une démarche plus respectueuse de l'environnement avec la possibilité de mieux connaître les outils utilisés et de les réparer en ne remplaçant que les composants défectueux, ou en réutilisant certains composants dans d'autres applications.

Sur le Gitlab, vous trouverez trois types de centrales de mesures qui ont été développées dans nos laboratoires, avec une philosophie dite "Lego". Cette philosophie s'appuie sur:

- des capteurs qui ont été évalués en laboratoire pour s'assurer de leur performance et qui sont en adéquation avec les besoins identifiés pour les eaux usées ;
- une stratégie d'assemblage simple sans soudure, sur la base de modules commerciaux disponibles en France chez les revendeurs spécialistes de l’électronique ;
- une notice bien documentée répertoriée ici.


**La centrale n°1**, est la centrale de mesure de débit et de hauteur d'eau ; elle inclut un capteur ultrason dont la plage de mesure est comprise entre 10 et 150 cm.

**La centrale n°2** est la centrale physico-chimique ; elle inclut une sonde de température, une sonde de conductivité électrique, une sonde redox, une sonde pH, et une sonde d’oxygène.

**La centrale n°3**, est la centrale de mesure d’énergie ; elle inclut jusqu'à 4 pinces ampérométriques.

# Documentation

Vous trouverez en ligne des documents nécessaires à l'assemblage et à l'utilisation de la centrale SETIER. 
Ces documents sont les suivants :
- flowmeter_datalogger_hole.pdf, Modèle de perçage et de fixation des différents composants du datalogger débitmètre.

- physico_chemical_datalogger_hole.pdf, Modèle de perçage et de fixation des différents composants du datalogger physico-chimique.

- energy_datalogger_hole.pdf, Modèle de perçage et de fixation des différents composants du datalogger énergie.

- flowmeter_datalogger_connexion.pdf, Modèle de connexion des différents composants du datalogger débitmètre.

- physico_chemical_datalogger_connexion.pdf, Modèle de connexion des différents composants du datalogger physico-chimique.

- energy_datalogger_connexion.pdf, Modèle de connexion des différents composants du datalogger énergie.

- logigramme_soft_setier.pdf, logigramme décisionnel d'aide à l'utilisation du software arduino sur les différents datalogger.

- setier-fichier-calibration.xlsm, fichier excel avec macro, à utiliser avec le datalogger physico-chimique. Ce fichier permet de procéder à la calibration des 4 capteurs physico-chimiques (redox, O2, pH et conductivité), en utilisant en parallèle le wifi du datalogger. 

- setier_datalogger_code, dossier dans lequel se trouve l'ensemble du code permettant le paramètrage et l'utilisation des différents dataloggers. Ce dossier est à télécharger intégralement.

[Documentation en PDF](https://reversaal.gitlab.irstea.page/setier_datalogger/setier_FR.pdf)


# Références


Articles
========
Low-cost sensors and datalogger open hardware for wastewaters: Setier project, S. Prost-Boucle, H. Kamgang Nzeufo, T. Bardo, S. Moreau, H. Guyard, C. Duwig, B. Kim, V. Dubois, S. Gillot, R. Clément

Communications
==============

Prost-Boucle S., Sudre J., Imig A-F., Clément R. (2022) Le projet SETIER : capteurs bon marché pour l’analyse des eaux usées - Des outils complémentaires à la gestion des stations. Réseau des exploitants de station d’épuration - GRAIE, Aix-les-Bains, FRA

Prost-Boucle S., Clément R. (2022) Le projet SETIER : capteurs bon marché pour l’analyse des eaux usées - Des outils complémentaires à la gestion des stations. Inovations digitales - RESEED, Villeurbanne, FRA

Prost-Boucle S., Clément R. (2021) Intérêt des capteurs bon marché pour l’analyse des eaux usées : un outil complémentaire à la gestion des stations ? 100ème congrès de l’ASTEE, Paris, FRA




# SETIER : Suivi et Evaluation des capTeurs low-cost pour les Installations de traitement des Eaux usées : qualité des Eaux et énErgie


In partnetship with the Rhône-Méditérranée-Corse Water Agency, INRAE and IRD, the SETIER project start in 2019. Coordinated by INRAE ( Stéphanie Prost-Boucle and Rémi Clément), this Setier project is based on the worldwide dévelopment of open-source électronic and on the Low-Tech sensors, miniaturised and portatif. This trend make the water quality measurement (conductivity, pH, turbidity, DCO, température, redox,dissolved O2, energy) more accessible. Their development opens up new prospects for monitoring and steering wastewater treatment and recovery facilities thanks to more detailed spatio-temporal characterisation (meters, minutes). this kind of approach, named « SMART Project » is developing in other field of environmental application, like air pollution or hydrology. The avantage of that open-source tools is that they offer a very high flexibility (Measure choice, number of sensors, measurement frequency), and is also part of an more environmentally friendly approach with the opportunity to know better the used tools and to repair by replacing only the defective components, or by reusing some of the components in other application fields.

On the Gitlab, you can found how to make and use 3 different kinf of measurement datalogger, developped in our laboratory, with a "Lego" philosophy. This philosophy is based on:

- Some sensors which were evaluate in a laboratory to be sure that their performances will be ok with the needs identified for wastewater.
- A easy strategy of assembling without soldering, based on business modules available in France at specialist electronics retailers.
- A manual well completed, which was listed their.


**The Datalogger n°1** was the flowmeter datalogger, it's include a precision ultrasonic sensor, with a measurement field between 10 and 150 cm.

**The Datalogger n°2** was the physico-chemical datalogger, it's include a temperature sensor, a conductivity sensor, a redox sensor,a pH sensor and a dissolved oxygen sensor.

**The Datalogger n°3**, was the energy datalogger, it's include up to 4 current clamps.

# Documentation

You can found online some documents that will be usefull for the assembling and the using of the different kind of Datalogger SETIER. 
Those documents were them :
- flowmeter_datalogger_hole.pdf, drilling and fixing pattern for the differents components of the flowmeter datalogger.

- physico_chemical_datalogger_hole.pdf, drilling and fixing pattern for the differents components of the physico-chemical datalogger.

- energy_datalogger_hole.pdf, drilling and fixing pattern for the differents components of the energy datalogger.

- flowmeter_datalogger_connexion.pdf, connection pattern of the differents components of the flowmeter datalogger.

- physico_chemical_datalogger_connexion.pdf, connection pattern of the differents components of the physico-chemical datalogger.

- energy_datalogger_connexion.pdf, connection pattern of the differents components of the energy datalogger.

- logigramme_soft_setier.pdf, flowchart to help for the using of the software and the code for the differents kinds of datalogger.

- setier-fichier-calibration.xlsm, excel file with Macro, to use with the physico-chemical datalogger. That file is used to calibration the 4 differents physico-chemical sensors (redox, O2, pH and conductivité), by using the excel file and the datalogger wifi.

- setier_datalogger_code, folder inside which there is all the code we use for the setting and the using of the different datalogger. This folder must be download on your computer.


[Documentation en PDF](https://reversaal.gitlab.irstea.page/setier_datalogger/setier_EN.pdf)

# Références


Articles
========
Low-cost sensors and datalogger open hardware for wastewaters: Setier project, S. Prost-Boucle, H. Kamgang Nzeufo, T. Bardo, S. Moreau, H. Guyard, C. Duwig, B. Kim, V. Dubois, S. Gillot, R. Clément

Communications
==============

Prost-Boucle S., Sudre J., Imig A-F., Clément R. (2022) Le projet SETIER : capteurs bon marché pour l’analyse des eaux usées - Des outils complémentaires à la gestion des stations. Réseau des exploitants de station d’épuration - GRAIE, Aix-les-Bains, FRA

Prost-Boucle S., Clément R. (2022) Le projet SETIER : capteurs bon marché pour l’analyse des eaux usées - Des outils complémentaires à la gestion des stations. Inovations digitales - RESEED, Villeurbanne, FRA

Prost-Boucle S., Clément R. (2021) Intérêt des capteurs bon marché pour l’analyse des eaux usées : un outil complémentaire à la gestion des stations ? 100ème congrès de l’ASTEE, Paris, FRA


