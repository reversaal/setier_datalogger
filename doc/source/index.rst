.. Datalogger setier documentation master file, created by
   sphinx-quickstart on Tue Jun 30 20:22:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Easylog: Open source and open hardware datalogger 
===================================================== 



.. sidebar:: Summary

    :Release: |release|
    :Date: |today|
    :Date start: June 2022	
    :Authors: **Rémi Clément,Hélène Guyard, Arnold Imig, Sylvain Moreau, Stéphanie Prost-Boucle, Julien Sudre**
    :Target: Users, researchers and developers 
    :status: Some mature, some in progress

.. topic:: SETIER Document Center

    * EasyLog offcial documents
    * Release guidelines
    * General tutorials

.. toctree:: 
   :maxdepth: 2 

   intro_FR.rst
   2_Datalogger_SETIER_débitmètre_FR.rst      
   2_Datalogger_SETIER_physico-chimique_FR.rst
   2_Datalogger_SETIER_energie_FR.rst
   2_Datalogger_SETIER_Software_FR.rst

   intro_EN.rst
   1_Datalogger_SETIER_flowmeter_EN.rst 
   1_Datalogger_SETIER_physico-chemical_EN.rst
   1_Datalogger_SETIER_energy_EN.rst
   1_Datalogger_SETIER_Software_EN.rst


