**********************************************
DataLogger SETIER Flowmeter V0.0 Hardware EN
**********************************************

     .. figure:: Image/Flowmeter/Hardware/Flowmeter_final.JPG
	   :width: 900px
	   :align: center
	   :height: 676px
	   :alt: alternate text
	   :figclass: align-center

.. attention::
	We invite you to refer to this document to assemble Datalogger SETIER V0.00 FLOWMETER Hardware Part.
	
.. attention::
	**SETIER is a participative project open to all, it requires skills in electronics and to respect the safety rules. SETIER must be assembled in a professional context and by people competent in electronics. The SETIER team cannot be held responsible for any material or human damage which would be associated with the use or the assembly of Datalogger SETIER. The SETIER team cannot be held responsible if the equipment does not work after assembly. You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRENTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.**

Technical Data
*******************

That data represent different using parameters of the flowmeter datalogger.

+-------------------------------+--------------------+-----------+
|**Parameters**                 |**Specifications**  |Units      |
+-------------------------------+--------------------+-----------+
|Supply voltage (VCC)           |7 to 15             |V          |
+-------------------------------+--------------------+-----------+
|Effective measuring range      |100 to 1500         |mm         |
+-------------------------------+--------------------+-----------+
|Accuracy                       |1                   |mm         |             
+-------------------------------+--------------------+-----------+
|Resolution                     |0.1                 |mm         |
+-------------------------------+--------------------+-----------+
|error percentage               |+/- 0.1             |%          |
+-------------------------------+--------------------+-----------+
|Temperature resolution         |0.1                 |°C         |
+-------------------------------+--------------------+-----------+
|Temperature error              |+/-1                |°C         |
+-------------------------------+--------------------+-----------+
|Using temperature              |-20 to 80           |°C         |
+-------------------------------+--------------------+-----------+
|Measurement frequency          |30                  |Hz         |
+-------------------------------+--------------------+-----------+
|Emission cone                  |12 +/-2             |°          |
+-------------------------------+--------------------+-----------+
|Sealing standard               |IP65                | -         |
+-------------------------------+--------------------+-----------+
|Communication interface        |RS485               | -         |
+-------------------------------+--------------------+-----------+
|Data storage                   |micro SD card       | -         |
+-------------------------------+--------------------+-----------+

Datalogger Assembly
**************************

Step 1 : Electronic part assembly
===============================================

The first part consists of assembling the different electronic board together.

Material List
-------------------

+-------------------------------+--------------------+---------------------------------------------------+
|Name                           |How Many            |Picture						 | 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR MEM Shield                 |1                   |.. image:: Image/General/Electronic/MKR_SD.JPG     | 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR Connector Carrier          |1                   |.. image:: Image/General/Electronic/MKR_CARRIER.JPG| 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR WIFI 1010	                |1                   |.. image:: Image/General/Electronic/MKR_WIFI.JPG   | 
+-------------------------------+--------------------+---------------------------------------------------+
|µSD card                       |1                   |.. image:: Image/General/Electronic/SD.JPG         | 
+-------------------------------+--------------------+---------------------------------------------------+
|RS485 communication board      |1                   |.. image:: Image/General/Electronic/RS485.JPG      | 
+-------------------------------+--------------------+---------------------------------------------------+
|RTC Module                     |1                   |.. image:: Image/General/Electronic/RTC.JPG        | 
+-------------------------------+--------------------+---------------------------------------------------+
|Voltage divider board		|1                   |.. image:: Image/General/Electronic/VOLTAGE.JPG    | 
+-------------------------------+--------------------+---------------------------------------------------+
|CR1220 lithium battery         |1                   |.. image:: Image/General/Electronic/Battery.JPG    | 
+-------------------------------+--------------------+---------------------------------------------------+

.. sidebar:: Be careful, the electronic boards needs to be connect on the right side (GND Pin on GND Pin for example).

             .. image:: Image/General/Electronic/WARNING_1.JPG
	      
1.	Connect the **MKR MEM Shield** on the **MKR Connector Carrier**.

        .. figure:: Image/General/Electronic/MKR_MEM_On_CARRIER.JPG
	  :width: 1037px
	  :align: center
	  :height: 577px
          :alt: alternate text
          :figclass: align-center

2.	Connect the **MKR WIFI 1010** on the **MKR MEM Shield**.  

	 .. figure:: Image/General/Electronic/MKR_WIFI_On_CARRIER.JPG
	   :width: 996px
	   :align: center
	   :height: 502px
	   :alt: alternate text
	   :figclass: align-center

3.	Insert the **µSD** card inside the **MKR MEM SHIELD**.

	.. figure:: Image/General/Electronic/SD_On_CARRIER.JPG
	   :width: 925px
	   :align: center
	   :height: 461px
	   :alt: alternate text
	   :figclass: align-center

.. sidebar::  The wire need to be connect in the right side.

             .. image:: Image/General/Electronic/WARNING_2.JPG

4.	Take the **RS485 communication board** and connect its wire on.

        .. figure:: Image/Flowmeter/Hardware/RS485_Wire.JPG
	   :width: 780px
	   :align: center
	   :height: 560px
	   :alt: alternate text
	   :figclass: align-center	

5.	Connect the other side of the wire from the **RS485 communication board** to the **SERIAL** port of the **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/RS485_On_CARRIER.JPG
	   :width: 704px
	   :align: center
	   :height: 385px
	   :alt: alternate text
	   :figclass: align-center

6.	Take the **RTC module** and plug its wire on.

        .. figure:: Image/Flowmeter/Hardware/RTC_Wire.JPG
	   :width: 500px
	   :align: center
	   :height: 343px
	   :alt: alternate text
	   :figclass: align-center

7.	Take the **CR1220 lithium battery** and insert it on the **RTC module**.

	.. figure:: Image/Physico_Chemical/Hardware/RTC_battery.JPG
	   :width: 15cm
	   :align: center
	   :alt: RTC_battery
	   :figclass: align-center

8.	Connect the other side of the wire from the **RTC module** to the **TWI** port of the **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/RTC_On_CARRIER.JPG
	   :width: 693px
	   :align: center
	   :height: 416px
	   :alt: alternate text
	   :figclass: align-center

9.	Take the **voltage divider board** and plug its wire on.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_Wire.JPG
	   :width: 1023px
	   :align: center
	   :height: 561px
	   :alt: alternate text
	   :figclass: align-center

10.	Connect the other side of the wire from the **voltage divider board** to the **A5-A6** of the **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_On_CARRIER.JPG
	   :width: 940px
	   :align: center
	   :height: 371px
	   :alt: alternate text
	   :figclass: align-center

Step 2 : Mechanical assembly
===============================

Material list
-------------------

+----------------------------------------------------+--------------------+----------------------------------------------------+
|Name                                                |How many            |Picture                                             |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|PVC support                                         |1                   |.. image:: Image/General/Electronic/PVC_Flow.JPG    |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Box 201x163x98mm                                    |1                   |.. image:: Image/General/Electronic/flow_Box.JPG    |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|SEN0358 sensor (distance) -external diameter 20 mm  |1                   |.. image:: Image/General/Electronic/SEN0358.JPG     |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Spacer (M2 et M3) 20 mm long                        |+/-10 of each       |.. image:: Image/General/Electronic/entretoise.JPG  |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Screw (M2 et M3)                                    |+/-10 of each       |.. image:: Image/General/Electronic/vis.JPG         |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Nuts (M2 et M3)                                     |+/-10 of each       |.. image:: Image/General/Electronic/ecrou.JPG       |   
+----------------------------------------------------+--------------------+----------------------------------------------------+
|12 cm red/black multi-strand cable diameter 1,6 mm  |1 of each color     |.. image:: Image/General/Electronic/cable-fin.JPG   |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|12 cm red/black multi-strand cable diameter 3 mm    |1 of each color     |.. image:: Image/General/Electronic/cable-épais.JPG |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Crimp lug                                           |2                   |.. image:: Image/General/Electronic/Pod.JPG         |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Red banana plug (external diameter 12 mm)           |1                   |.. image:: Image/General/Electronic/Red_Banana.JPG  |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Black banana plug (external diameter 12 mm)         |1                   |.. image:: Image/General/Electronic/Black_Banana.JPG|
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Voltcraft power supply                              |1                   |.. image:: Image/General/Electronic/Alimentation.JPG|
+----------------------------------------------------+--------------------+----------------------------------------------------+
|male µ USB/male USB-A wire                          |1                   |.. image:: Image/General/Electronic/USB-Cable.JPG   |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Push button (external diameter 14 mm)               |1                   |.. image:: Image/General/Electronic/Bouton.JPG      |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Grove screw terminal                                |1                   |.. image:: Image/General/Electronic/Screw_Gr.JPG    |
+----------------------------------------------------+--------------------+----------------------------------------------------+

#.	Take a piece of wood, PVC, or some other non-conductive element, and cut it following the folder "**Flowmeter_datalogger_hole.pdf**" to do the fixing plate of the different datalogger board. Drill hole to put on the spacer inside it, then fix it.

        .. figure:: Image/Flowmeter/Hardware/PVC_Hole.JPG
	   :width: 633px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill a 20 mm diameter hole inside the **box 201x163x98mm** following the folder "**Flowmeter_datalogger_hole.pdf**". 

        .. figure:: Image/Flowmeter/Hardware/Box_Hole.JPG
	   :width: 663px
	   :align: center
	   :height: 278px
	   :alt: alternate text
	   :figclass: align-center


#.	Insert the **SEN0358 sensor** inside the box hole, and fix it with one nuts on both side of the box (the nuts are provided with the sensor).

        .. figure:: Image/Flowmeter/Hardware/SEN_0358_FIXED.JPG
	   :width: 800px
	   :align: center
	   :height: 400px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **Arduino MKR Carrier** on the spacer provided for that purpose, with some screw (following the folder "**Flowmeter_datalogger_hole.pdf**"). 

        .. figure:: Image/Flowmeter/Hardware/arduino_fix.JPG
	   :width: 590px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **RTC module** on the spacer provided for that purpose with some screw (following the folder "**Flowmeter_datalogger_hole.pdf**").  

        .. figure:: Image/Flowmeter/Hardware/rtc_fix.JPG
	   :width: 650px
	   :align: center
	   :height: 706px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **Grove screw terminal** on the spacer provided for that purpose with some screw (following the folder "**Flowmeter_datalogger_hole.pdf**"). This module must be put on top of the **RTC module**. 

        .. figure:: Image/Flowmeter/Hardware/push_fix.JPG
	   :width: 626px
	   :align: center
	   :height: 712px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix  the **PVC plate** on the bottom of the box, with screw, by routing the sensor wire through the **PVC plate** hole.

        .. figure:: Image/Flowmeter/Hardware/SEN_0358_WIRE.JPG
	   :width: 605px
	   :align: center
	   :height: 724px
	   :alt: alternate text
	   :figclass: align-center

#.	Take an independant red wire, and the brown wire from the **SEN0358 sensor**, and connect them both inside the **VOL** port of the **voltage divider board**. Screw them.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_connexion_1.JPG
	   :width: 867px
	   :align: center
	   :height: 681px
	   :alt: alternate text
	   :figclass: align-center

#.	Take a independant black wire, and the black wire from the **SEN0358 sensor**, and connect them both inside the **GND** port fo the **voltage divider board**. Screw them.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_connexion_2.JPG
	   :width: 800px
	   :align: center
	   :height: 600px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the **RS485 communication board** and connect the **SEN0358 sensor** wire inside using a flat screwdriver : the blue wire must be connect on the **B** port and the wire must be connect on the **A** port.

        .. figure:: Image/Flowmeter/Hardware/RS485_connexion.JPG
	   :width: 776px
	   :align: center
	   :height: 710px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **RS485 communication board** on the spacer provided with screw.

        .. figure:: Image/Flowmeter/Hardware/RS485_fix.JPG
	   :width: 900px
	   :align: center
	   :height: 706px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **voltage divider board** on the spacer provided for that purpose. This module must be put on top of the **RS485 communication board**.

        .. figure:: Image/Flowmeter/Hardware/divider_fix.JPG
	   :width: 900px
	   :align: center
	   :height: 700px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill two 12 mm diameter hole on one side of the **box**. Take one **red banana plug** and **black banana plug**. Insert it inside that hole, then fix it (a nuts is provided with the banana plug).

        .. figure:: Image/Flowmeter/Hardware/banana_plug.JPG
	   :width: 599px
	   :align: center
	   :height: 743px
	   :alt: alternate text
	   :figclass: align-center

#.	Cut 12cm of **red wire diameter 3 mm**. Use a **wire stripper** to strip 1 cm on both side of the wire.

        .. figure:: Image/Flowmeter/Hardware/red_wire_stripped.JPG
	   :width: 816px
	   :align: center
	   :height: 778px
	   :alt: alternate text
	   :figclass: align-center

#.	Cut 12cm of **black wire diameter 3 mm**. Use a **wire stripper** to strip 1 cm on both side of the wire.

        .. figure:: Image/Flowmeter/Hardware/Black_wire_stripped.JPG
	   :width: 867px
	   :align: center
	   :height: 827px
	   :alt: alternate text
	   :figclass: align-center

#.	Take a **crimp lug**. Use a **crimping tool** to fix the **crimp lug** on one side  of the **red wire diameter 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/red_wire_pod.JPG
	   :width: 792px
	   :align: center
	   :height: 682px
	   :alt: alternate text
	   :figclass: align-center

#.	Take a **crimp lug**. Use a **crimping tool** to fix the **crimp lug** on one side of the **black wire diameter 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/black_wire_pod.JPG
	   :width: 740px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **crimp lug** side of the **red wire diameter 3 mm** on the **red banana plug** with a nut (provided with the **red banana plug**). 

        .. figure:: Image/Flowmeter/Hardware/red_pod_on_banana_plug.JPG
	   :width: 705px
	   :align: center
	   :height: 738px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **crimp lug** side of the **black wire diameter 3 mm** on the **black banana plug** with a nut (provided with the **black banana plug**). 

        .. figure:: Image/Flowmeter/Hardware/black_pod_on_banana_plug.JPG
	   :width: 687px
	   :align: center
	   :height: 733px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the other side of the **red wire diameter 3 mm**, and connect it on the **Vin** port of the **MKR CONNECTOR CARRIER** with the other side of the red wire connected to the **voltage divider board**.

        .. figure:: Image/Flowmeter/Hardware/Arduino_CARRIER_connexion.JPG
	   :width: 660px
	   :align: center
	   :height: 730px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the other side of the **black wire diameter 3 mm**, and connect it on the **GND** port of the **MKR CONNECTOR CARRIER** with the other side of the black wire connected to the **voltage divider board**.

        .. figure:: Image/Flowmeter/Hardware/Arduino_CARRIER_connexion_2.JPG
	   :width: 574px
	   :align: center
	   :height: 723px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill a 14 mm diameter hole on one side of the **box**, and insert in the **push button**. Use a nut to fix it (the nut is provided with the **push button**).

        .. figure:: Image/Flowmeter/Hardware/push_button_fix.JPG
	   :width: 599px
	   :align: center
	   :height: 737px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect both wire from the **push button** on the **Grove screw terminal** (one of the wire must be connect on the **VCC** port, and the other on the **D2** port), then, screw it.
        
	.. figure:: Image/Flowmeter/Hardware/push_button_connect.JPG
	   :width: 865px
	   :align: center
	   :height: 712px
	   :alt: alternate text
	   :figclass: align-center

#.	Close the box. The flowmeter datalogger is ready to be use. **Be careful, the datalogger need an 12V DC power supply, use an adapted power supply**.

     .. figure:: Image/Flowmeter/Hardware/Flowmeter_final2.JPG
	   :width: 583px
	   :align: center
	   :height: 733px
	   :alt: alternate text
	   :figclass: align-center

