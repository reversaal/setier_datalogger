import PyPDF2 as pyPdf
from PyPDF2 import PdfWriter, PdfReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
existing_pdf = PdfReader(open("destination_FR.pdf", "rb"))
output = PdfWriter()

def nbpagespdf(fichierpdf):
    """retourne le nombre de pages du fichier pdf"""
    with open(fichierpdf, "rb") as f:
        return len(existing_pdf.pages)
        #return pyPdf.PdfReader(f).getNumPages()
 


A=nbpagespdf("destination_FR.pdf")

for i in range(nbpagespdf("destination_FR.pdf")):
    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=letter)
    can.drawString(300, 15, (str(i)+" / "+str(A)))
    can.save()

#move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfReader(packet)
# read your existing PDF

# add the "watermark" (which is the new pdf) on the existing page

    page = existing_pdf.pages[i]
    page.merge_page(new_pdf.pages[0])
    output.add_page(page)
# finally, write "output" to a real file
outputStream = open("setier_FR.pdf", "wb")
output.write(outputStream)
outputStream.close()

A=nbpagespdf("destination_EN.pdf")

for i in range(nbpagespdf("destination_EN.pdf")):
    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=letter)
    can.drawString(300, 15, (str(i)+" / "+str(A)))
    can.save()

#move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfReader(packet)
# read your existing PDF

# add the "watermark" (which is the new pdf) on the existing page

    page = existing_pdf.pages[i]
    page.merge_page(new_pdf.pages[0])
    output.add_page(page)
# finally, write "output" to a real file
outputStream = open("setier_EN.pdf", "wb")
output.write(outputStream)
outputStream.close()


