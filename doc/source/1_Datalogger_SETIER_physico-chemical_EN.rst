****************************************************
DataLogger SETIER PHYSICO-CHEMICAL V0.0 Hardware EN
****************************************************

     .. figure:: Image/Physico_Chemical/Hardware/Physico_final.JPG
	   :width: 887px
	   :align: center
	   :height: 754px
	   :alt: alternate text
	   :figclass: align-center

.. Attention::
	We invite you to refer to this document to assemble Datalogger SETIER V0.00 FLOWMETER Hardware Part. Be careful, when you use this datalogger, please not immerge totally the different physico-chemical sensors (except temperature sensor).
	
.. attention::
	**SETIER is a participative project open to all, it requires skills in electronics and to respect the safety rules. SETIER must be assembled in a professional context and by people competent in electronics. The SETIER team cannot be held responsible for any material or human damage which would be associated with the use or the assembly of Datalogger SETIER. The SETIER team cannot be held responsible if the equipment does not work after assembly.You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.**

Technical data
********************
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|**Technical sensor properties**|**Specifications**  |Units      |**Probe**          |Associate probe    |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Supply voltage (VCC)           |3.3 to 5.5          |V          |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Effective measuring range      |0 to 14             |pH unit    |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Temperature range              |0 to 60             |°C         |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Resolution                     |0.1                 |pH unit    |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|response time                  |< 1                 |min        |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Probe lifetime                 |6                   |month      |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Cable lenght                   |5                   |m          |SEN0169-V2 Sensor  |**pH**             |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Supply voltage (VCC)           |3 to 5              |V          |DFR0300 Sensor     |**Conductivité**   |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Effective measuring range      |0 to 20             |mS/cm      |DFR0300 Sensor     |**Conductivité**   |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Temperature range              |0 to 40             |°C         |DFR0300 Sensor     |**Conductivité**   |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Probe lifetime                 |6                   |month      |DFR0300 Sensor     |**Conductivité**   |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|cable lenght                   |1                   |m          |DFR0300 Sensor     |**Conductivité**   |
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Supply voltage (VCC)           |5                   |V          |SEN0464 Sensor     |**Potentiel Redox**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Effective measuring range      |-2000 to 2000       |mV         |SEN0464 Sensor     |**Potentiel Redox**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Temperature range              |5 to 70             |°C         |SEN0464 Sensor     |**Potentiel Redox**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Accuracy (at 25°C)             |10                  |mV         |SEN0464 Sensor     |**Potentiel Redox**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Supply voltage (VCC)           |3.3 à 5             |V          |SEN0237 Sensor     |**Oxygène Dissous**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Effective measuring range      |0 to 20             |mg/L       |SEN0237 Sensor     |**Oxygène Dissous**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Probe lifetime                 |12                  |month      |SEN0237 Sensor     |**Oxygène Dissous**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Membrane lifetime              |1-2                 |month      |SEN0237 Sensor     |**Oxygène Dissous**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|NAOH lifetime                  |1                   |month      |SEN0237 Sensor     |**Oxygène Dissous**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+
|Cable lenght                   |2                   |m          |SEN0237 Sensor     |**Oxygène Dissous**|
+-------------------------------+--------------------+-----------+-------------------+-------------------+


Assembly of the datalogger
**************************

Electronic board assembly
======================================

La première partie consiste à assembler les différentes cartes électronique composant le datalogger.

Material list
---------------

+-------------------------------+--------------------+---------------------------------------------------+
|Name                           |How many            |Picture                                            |
+-------------------------------+--------------------+---------------------------------------------------+
|MKR MEM Shield                 |1                   |.. image:: Image/General/Electronic/MKR_SD.JPG     | 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR Connector Carrier          |1                   |.. image:: Image/General/Electronic/MKR_CARRIER.JPG| 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR WIFI 1010	                |1                   |.. image:: Image/General/Electronic/MKR_WIFI.JPG   | 
+-------------------------------+--------------------+---------------------------------------------------+
|µSD card                       |1                   |.. image:: Image/General/Electronic/SD.JPG         | 
+-------------------------------+--------------------+---------------------------------------------------+
|I2C Hub                        |1                   |.. image:: Image/General/Electronic/I2C_Hub.JPG    |
+-------------------------------+--------------------+---------------------------------------------------+
|RTC Module                     |1                   |.. image:: Image/General/Electronic/RTC.JPG        | 
+-------------------------------+--------------------+---------------------------------------------------+
|Voltage divider board          |1                   |.. image:: Image/General/Electronic/VOLTAGE.JPG    | 
+-------------------------------+--------------------+---------------------------------------------------+
|temperature sensor             |1                   |.. image:: Image/General/Electronic/Temperature.JPG|
+-------------------------------+--------------------+---------------------------------------------------+
|Grove screw terminal           |1                   |.. image:: Image/General/Electronic/Screw_Gr.JPG   |
+-------------------------------+--------------------+---------------------------------------------------+
|4.7 kOhm resistor              |1                   |.. image:: Image/General/Electronic/4.7kOhm.JPG    |
+-------------------------------+--------------------+---------------------------------------------------+
|CR1220 lithium battery         |1                   |.. image:: Image/General/Electronic/Battery.JPG    |
+-------------------------------+--------------------+---------------------------------------------------+

.. sidebar:: Be careful, boards need to be connect on the right side (Pin 5V on Pin 5V for example).

             .. image:: Image/General/Electronic/WARNING_1.JPG

1.	Fix the **MKR MEM Shield** on the **MKR Connector Carrier**.

     .. figure:: Image/General/Electronic/MKR_MEM_On_CARRIER.JPG
	   :width: 1037px
	   :align: center
	   :height: 577px
	   :alt: alternate text
	   :figclass: align-center

2.	Fix the **MKR WIFI 1010** on the **MKR MEM Shield**.

	 .. figure:: Image/General/Electronic/MKR_WIFI_On_CARRIER.JPG
	   :width: 996px
	   :align: center
	   :height: 502px
	   :alt: alternate text
	   :figclass: align-center

3.	Insert the **µSD** card inside the **MKR MEM SHIELD**.

	.. figure:: Image/General/Electronic/SD_On_CARRIER.JPG
	   :width: 925px
	   :align: center
	   :height: 461px
	   :alt: alternate text
	   :figclass: align-center
 
4.	connect on the Serial Port of the **MKR CARRIER** the **I2C Hub** with a wire.

	.. figure:: Image/Physico_Chemical/Hardware/4Divider_on_carrier.JPG
	   :width: 900px
	   :align: center
	   :height: 475px
	   :alt: alternate text
	   :figclass: align-center

5.	Take the **RTC module** and connect its wire inside. The order of connecting on the **4 divider board** is not important.

	.. figure:: Image/Flowmeter/Hardware/RTC_Wire.JPG
	   :width: 500px
	   :align: center
	   :height: 343px
	   :alt: alternate text
	   :figclass: align-center
 
6.	Take the CR1220 lithium battery and set it on the RTC board.

	.. figure:: Image/Physico_Chemical/Hardware/RTC_battery.JPG
	   :width: 618px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

7.	Connect the otherside of the **RTC wire** on the **I2C Hub**. The connection order on the "I2C Hub" is not important.

	.. figure:: Image/Physico_Chemical/Hardware/wire_and_rtc_on_carrier.JPG
	   :width: 723px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

8.	Take the **Voltage divider board** and connect its wire inside.

	.. figure:: Image/Flowmeter/Hardware/Voltage_Divider_Wire.JPG
	   :width: 900px
	   :align: center
	   :height: 492px
	   :alt: alternate text
	   :figclass: align-center

9.	Connect the other side of the **Voltage divider wire** inside the A5-A6 port of the **MKR CONNECTOR CARRIER**.

	.. figure:: Image/Physico_Chemical/Hardware/voltage_divider_on_carrier.JPG
	   :width: 913px
	   :align: center
	   :height: 727px
	   :alt: alternate text
	   :figclass: align-center

10.	Take the temperature sensor, the **grow screw terminal** and the **4.7 kOhm resistor**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature.JPG
	   :width: 722px
	   :align: center
	   :height: 734px
	   :alt: alternate text
	   :figclass: align-center

11.	Fix the black wire on the **GND port** of the **Grove Screw terminal**, the **red wire** on the **VCC** port, and the white wire on the **D1** port.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_plug.JPG
	   :width: 744px
	   :align: center
	   :height: 737px
	   :alt: alternate text
	   :figclass: align-center

12.	Connect one side of the **resistor** with the **white wire** on the **D1** port and the other side one on the **VCC** port with the **red wire**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_resistor_plug.JPG
	   :width: 767px
	   :align: center
	   :height: 698px
	   :alt: alternate text
	   :figclass: align-center 

13.	Screw the **grove screw terminal port** with the different wire inside it.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_resistor_plug_screw.JPG
	   :width: 862px
	   :align: center
	   :height: 744px
	   :alt: alternate text
	   :figclass: align-center 

14.	Connect the **grove screw terminal wire** inside it, and the other side on the D0 port of the **Arduino MKR CARRIER**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_carrier_plug.JPG
	   :width: 900px
	   :align: center
	   :height: 506px
	   :alt: alternate text
	   :figclass: align-center 

Mechanical Assembly
=====================

Material List
-------------------

+--------------------------------------------------+--------------------+------------------------------------------------------+
|Name                                  		   |How many            |Picture                                               |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|4 relay bord                         	 	   |2                   |.. image:: Image/General/Electronic/relai.JPG         |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|SEN0169-V2(pH sensor)            		   |1                   |.. image:: Image/General/Electronic/SEN0169.JPG       |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|DFR0300 (Conductivity sensor)                 	   |1                   |.. image:: Image/General/Electronic/DFR0300.JPG       |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|SEN0464(ORP sensor)                     	   |1                   |.. image:: Image/General/Electronic/SEN0464.JPG       |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|SEN0237-A(O2 sensor)           	           |1                   |.. image:: Image/General/Electronic/SEN0237.JPG       |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Wire Grove/Picot                         	   |4                   |.. image:: Image/General/Electronic/Grove_picot.JPG   |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Spacer (M2 and M3) 20 mm de long                  |+/-10 of each       |.. image:: Image/General/Electronic/entretoise.JPG    |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Screw (M2 and M3)                                 |+/-10 of each       |.. image:: Image/General/Electronic/vis.JPG           |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Nuts (M2 and M3)                                  |+/-10 of each       |.. image:: Image/General/Electronic/ecrou.JPG         |     
+--------------------------------------------------+--------------------+------------------------------------------------------+
|12 cm red/black multi-strand cable diameter 1,6 mm|1 of each color     |.. image:: Image/General/Electronic/cable-fin.JPG     |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|12 cm red/black multi-strand cable diameter 3 mm  |1 of each color     |.. image:: Image/General/Electronic/cable-épais.JPG   |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Crimp lug                                	   |2                   |.. image:: Image/General/Electronic/Pod.JPG           |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Red banana plug (external diameter 12 mm)         |1                   |.. image:: Image/General/Electronic/Red_Banana.JPG    |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Black banana plug (external diameter 12 mm) 	   |1                   |.. image:: Image/General/Electronic/Black_Banana.JPG  |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Cable gland (external diameter 16 mm)		   |2                   |.. image:: Image/General/Electronic/Cable_Gl.JPG      |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Box 344 x 289 x 117mm                  	   |1                   |.. image:: Image/General/Electronic/pc_Box.JPG        |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|PVC plate                              	   |1                   |.. image:: Image/General/Electronic/PVC_pc.JPG        |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Voltcraft power supply                   	   |1                   |.. image:: Image/General/Electronic/Alimentation.JPG  |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Male µ USB/male USB-A wire               	   |1                   |.. image:: Image/General/Electronic/USB-Cable.JPG     |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Push button (external diameter 14 mm) 		   |1                   |.. image:: Image/General/Electronic/Bouton.JPG        |
+--------------------------------------------------+--------------------+------------------------------------------------------+
|Wire Picot/Picot (red and black)         	   |10                  |.. image:: Image/General/Electronic/cable-picot.JPG   |
+--------------------------------------------------+--------------------+------------------------------------------------------+

**For all this part, you can use the physico_chemical_datalogger_connexion.pdf file to see where you have to connect the different board of the datalogger.**

#.	Unstick the brown scotch of the different side of protection box of the **4 relay board**.

        .. figure:: Image/General/Electronic/Relay_box_unstick.JPG
	   :width: 900px
	   :align: center
	   :height: 484px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the electronic board of the **4 relay board** and fix it on the support provided with, using screw.

        .. figure:: Image/General/Electronic/Relay_box_1.JPG
	   :width: 909px
	   :align: center
	   :height: 721px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the 4 vertical side of the protection box, and insert it on the **4 relay board** support. Then, take the top part and fix it using spacer provided with the **4 relay board**.       

        .. figure:: Image/Physico_Chemical/Hardware/relay_box_closed.JPG
	   :width: 574px
	   :align: center
	   :height: 695px
	   :alt: alternate text
	   :figclass: align-center


#.	**Do the step 1 to 3 twice to have 2 **4 relay board**.

#.	Take a piece of wood, PVC, or some other non-conductive element, and cut it following the folder "**Physico_Chemical_datalogger_hole.pdf**" to do the fixing plate of the different datalogger board. 

        .. figure:: Image/Physico_Chemical/Hardware/PVC_columns.JPG
	   :width: 900px
	   :align: center
	   :height: 595px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill the spacer hole, then fix the M2 and M3 spacer with nut for the M3 spacer and with screw for M2.

        .. figure:: Image/Physico_Chemical/Hardware/PVC-fixing.JPG
	   :width: 432px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **first relay board**, with spacer at the back side ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/relay-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 580px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **second relay board** on the back side of the first one. For the 2 others holes, fix them on the spacer we have set before ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/relay2-fixing.JPG
	   :width: 880px
	   :align: center
	   :height: 693px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **Arduino MKR Carrier** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/arduino-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 650px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **temperature sensor grove screw terminal** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/temperature-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 662px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **I2C Hub** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/4divider-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 637px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **Push-button sensor grove screw terminal** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/push-button-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 749px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **RTC module** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/RTC-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 750px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **voltage divider board** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/voltage-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 719px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the wire from the **I2C Hub** to the 2 relay.

        .. figure:: Image/Physico_Chemical/Hardware/relay_connect.JPG
	   :width: 855px
	   :align: center
	   :height: 762px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the conductivity sensor **DFR0300**. Connect on the board the BNC wire and the white side wire.

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor.JPG
	   :width: 900px
	   :align: center
	   :height: 454px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the picot/grove wire. Connect the yellow part on the blue output of the **DFR0300 board wire**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Take 2 Picot/Picot wire (one black and one red). Connect one side of each of them on the red ouptut of the **DFR0300 board wire** for the red one, and on the black output of the **DFR0300 board wire** for the black one.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotch this 3 wire together for safety.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the black wire from the sensor board to the left input of the **first Relay board**, first relay (SW1). Then, connect the black wire from the**MKR carrier connector** to the middle input of the **first Relay board**, first relay (SW1).

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor_on_relay1.JPG
	   :width: 900px
	   :align: center
	   :height: 632px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the red wire from the sensor board to the left input of the **second Relay board**, first relay (SW1). Then, connect the red wire from the carrier connector to the middle input of the **second Relay bord**, first relay (SW1).

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 616px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **sensor carrier connector** on the **A3** input of the **MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor_on_carrier.JPG
	   :width: 900px
	   :align: center
	   :height: 664px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the **O2 sensor**. Connect on its electronics baord the BNC wire and the white side of the wire.

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor.JPG
	   :width: 900px
	   :align: center
	   :height: 438px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the Picot/grove wire you see bellow. Connect the yellow part on the blue output of the **O2 board wire**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Take two picot/picot wire (one black and one red wire). Connect one side of each of them on the red ouptut of the **O2 board wire** for the red one, and on the black output of the **O2 board wire** for the black one.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotch this 3 wires together.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the black wire from the sensor board to the left input of the **first Relay board**, second relay (SW2). Then connect the black wire from the carrier connector to the middle input of the **first Relay bord**, second relay (SW2).

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor_on_relay1.JPG
	   :width: 900px
	   :align: center
	   :height: 636px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the red wire from the sensor board to the left input of the **second Relay board**, second relay (SW2). Then connect the red wire from the carrier connector to the middle input of the **second Relay bord**, second relay (SW2).

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 695px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **sensor carrier connector** on the A2 input of the **MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor_on_carrier.JPG
	   :width: 412px
	   :align: center
	   :height: 689px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the **ORP sensor**. connect on OPR electronic board the white side of the wire. The otherside will be connected later.

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor.JPG
	   :width: 638px
	   :align: center
	   :height: 352px
	   :alt: alternate text
	   :figclass: align-center

#.	Take picot/grove wire you see bellow. Connect the yellow part on the blue output of the ORP electronic board wire.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Take two picot/picot wire (one black and one red wire). Connect one side of each of them on the red ouptut of the **ORP board** wire for the red one, and on the black output of the ORP board wire for the black one.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotch this 3 wire together.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the black wire from the sensor board to the left input of the **first Relay board**, third relay (SW3). Then connect the black wire from the carrier connector to the middle input of the **first Relay bord**, third relay (SW3).

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_on_relay1.JPG
	   :width: 900px
	   :align: center
	   :height: 660px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the red wire from the sensor board to the left input of the **second Relay board**, third relay (SW3). Then connect the red wire from the carrier connector to the middle input of the **second Relay bord**, third relay (SW3).

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 741px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **sensor carrier connector** on the **A1** input of the **MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_on_carrier.JPG
	   :width: 410px
	   :align: center
	   :height: 758px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the pH sensor **SEN0169_V2**. Connect on electronic board the BNC wire and the white side of the wire.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor.JPG
	   :width: 900px
	   :align: center
	   :height: 462px
	   :alt: alternate text
	   :figclass: align-center

#.	Take picot/grove wire you see bellow. Connect the yellow part on the blue output of the **SEN0169_V2 board wire**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Take two picot/picot wire (one black and one red wire). Connect one side off each of them on the red ouptut of the **SEN0169_V2 board wire** for the red one, and on the black output of the **SEN0169_V2 board wire** for the black one.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotch this 3 wire together.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the black wire from the sensor board to the left input of the **first Relay board**, fourth relay (SW4). Then connect the black wire from the carrier connector to the middle input of the **first Relay bord**, fourth relay (SW4).

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_on_relay1.JPG
	   :width: 885px
	   :align: center
	   :height: 759px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the red wire from the sensor board to the left input of the **second Relay board**, fourth relay (SW4). Then connect the red wire from the carrier connector to the middle input of the **second Relay bord**, fourth relay (SW4).

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 757px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **sensor carrier connector** on the **A0** input of the **MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_on_carrier.JPG
	   :width: 556px
	   :align: center
	   :height: 735px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **ORP sensor board** on the PVC support ("following the physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/ORP-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 750px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the box and drill 5 holes : 3 for the Conductivity, O2 and pH sensor BNC connexion (size of the hole "**M12**"), and 2 for for the Temperature and ORP sensor output (size of the hole "**M16**"). Be careful, this hole needs to be on the side where you're gonna have your relay in the box, use wood protection inside the box.

        .. figure:: Image/Physico_Chemical/Hardware/sensor_hole.JPG
	   :width: 900px
	   :align: center
	   :height: 524px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill 2 holes (size of the hole "**M12**") on the other side of the box to set the banana connect later. Be careful, this hole needs to be on the side where you're gonna have your **Arduino boards** in the box, use wood protection inside the box.

        .. figure:: Image/Physico_Chemical/Hardware/banana_hole.JPG
	   :width: 900px
	   :align: center
	   :height: 575px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill 1 holes (size of the hole "**M14**") on the voltage divider side of the box to set the push button later.

        .. figure:: Image/Physico_Chemical/Hardware/button_hole.JPG
	   :width: 900px
	   :align: center
	   :height: 384px
	   :alt: alternate text
	   :figclass: align-center

#.	Put the PVC support inside the box.

        .. figure:: Image/Physico_Chemical/Hardware/PVC-inbox.JPG
	   :width: 900px
	   :align: center
	   :height: 551px
	   :alt: alternate text
	   :figclass: align-center

#.	Insert the different BNC connect of the sensor in the 3 holes, following this sequence : pH,  O2, Conductivity.

        .. figure:: Image/Physico_Chemical/Hardware/BNC_inbox.JPG
	   :width: 646px
	   :align: center
	   :height: 771px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix all the BNC connector with a seal and a nut.

        .. figure:: Image/Physico_Chemical/Hardware/BNC_inbox_fixing.JPG
	   :width: 629px
	   :align: center
	   :height: 758px
	   :alt: alternate text
	   :figclass: align-center

#.	Cut 12 cm of **red wire diameter 3 mm**. use a **stripped clamp** for 1 cm of both side of the wires.

        .. figure:: Image/Flowmeter/Hardware/red_wire_stripped.JPG
	   :width: 816px
	   :align: center
	   :height: 778px
	   :alt: alternate text
	   :figclass: align-center

#.	Cut 12 cm of **black wire diameter 3 mm**. use a **stripped clamp** for 1 cm of both side of the wires.

        .. figure:: Image/Flowmeter/Hardware/Black_wire_stripped.JPG
	   :width: 867px
	   :align: center
	   :height: 827px
	   :alt: alternate text
	   :figclass: align-center

#.	Take **1 crimp lug**. use a **crimping tool** to set one pod on **red wire diameter 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/red_wire_pod.JPG
	   :width: 792px
	   :align: center
	   :height: 682px
	   :alt: alternate text
	   :figclass: align-center

#.	Take **1 crimp lug**. use a **crimping tool** to set one pod on **black wire diameter 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/black_wire_pod.JPG
	   :width: 740px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Set the red banana connect inside his hole. Then, connect the red wire on the red banana connector with the pod side, then screw the nut after. Do the same for the black banana connect (Ground).

        .. figure:: Image/Physico_Chemical/Hardware/banana_plug-on-box.JPG
	   :width: 849px
	   :align: center
	   :height: 668px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the otherside of the **red wire diameter 3 mm** from the banana connect and the red wire wire from the **voltage divider** in the **Vin outlet of the MKR CARRIER**. Then connect the black wire from the banana connect and the black wire from the **voltage divider** in the **GND outlet of the MKR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/input_Wire.JPG
	   :width: 824px
	   :align: center
	   :height: 674px
	   :alt: alternate text
	   :figclass: align-center

#.	Set the "**push button**" inside his hole, and use a nut to make sure it didn't move.

        .. figure:: Image/Physico_Chemical/Hardware/push-button-inbox.JPG
	   :width: 884px
	   :align: center
	   :height: 701px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect both wire of the "** push button**" on the Grove board (one of the wire need to be connect to the VCC port and the other to the D2 port), then screw it. The wire connection place is not important.

        .. figure:: Image/Physico_Chemical/Hardware/push-button-connect.JPG
	   :width: 871px
	   :align: center
	   :height: 763px
	   :alt: alternate text
	   :figclass: align-center

#.	Set a cable gland on the both hole remaining. One is for the **ORP sensor wire**, and the other one is for the **temperature sensor**. 

        .. figure:: Image/Physico_Chemical/Hardware/stuffing-box.JPG
	   :width: 437px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Set the "**ORP sensor**" wire inside the cable gland (from outside to inside the box), then, screw them on the screw terminal block. ** caution: the blue and red cables must not touch each other as this could damage the sensor.**

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_screw.JPG
	   :width: 900px
	   :align: center
	   :height: 648px
	   :alt: alternate text
	   :figclass: align-center

#.	Set the "**Temperature sensor**" inside the cable gland (from inside to outside the box). Then, connect the other different sensor on the BNC.

        .. figure:: Image/Physico_Chemical/Hardware/sensor_plug.JPG
	   :width: 900px
	   :align: center
	   :height: 758px
	   :alt: alternate text
	   :figclass: align-center

#.	Close the box. The Physico-chemical datalogger is ready to be use. **Please be careful, the datalogger need to be powered by a 12V direct current, use an adapted power supply**.

        .. figure:: Image/Physico_Chemical/Hardware/box_close.JPG
	   :width: 900px
	   :align: center
	   :height: 520px
	   :alt: alternate text
	   :figclass: align-center


