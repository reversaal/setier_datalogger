*************************************
DataLogger SETIER : Software V0.0 FR
*************************************

Pour toutes la partie suivante, prière d'utiliser un ordinateur ou une tablette possédant comme système d'exploitation Windows 10 minimumn, Linux, ou MAC. 

Utilisation d'Arduino
**********************

Installation de l'IDE Arduino et téléchargement des librairies associées au datalogger SETIER
==================================================================================================

#.	Aller sur le lien **https://www.arduino.cc/en/software**, puis cliquer sur **Windows Win 7 and Newer** (Attention la capture d'écran ci-dessous présente une version de Arduino à un moment donnée, cette version est susceptible d'évoluer).

    .. figure:: Image/Software/Arduino/Arduino_download1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer ensuite sur **Just Download** afin de télécharger la version de l'IDE Arduino sélectionné.

    .. figure:: Image/Software/Arduino/Arduino_download2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer ensuite sur **Enregistrer le fichier** pour lancer le téléchargement de l'IDE Arduino sélectionné.

    .. figure:: Image/Software/Arduino/Arduino_download3.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Aller ensuite dans téléchargements afin d'exécuter le programme.

    .. figure:: Image/Software/Arduino/Arduino_download4.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Lorsque le téléchargement est terminé, ouvrir **ARDUINO** IDE sur votre ordinateur.

    .. figure:: Image/Software/Arduino/Arduino_open.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Copier sur internet les liens suivants, puis télécharger le fichier **zip** associé : 

- :https://github.com/arduino-libraries/ArduinoLowPower.git

- https://github.com/arduino-libraries/SD.git

- https://github.com/arduino-libraries/RTCZero.git

- https://github.com/arduino-libraries/ArduinoModbus.git

- https://github.com/arduino-libraries/ArduinoRS485.git

- https://github.com/arduino-libraries/WiFiNINA.git

- https://github.com/adafruit/RTClib.git

- https://github.com/Seeed-Studio/Multi_Channel_Relay_Arduino_Library.git

- https://github.com/cmaglie/FlashStorage.git

- https://github.com/PaulStoffregen/OneWire.git

    .. figure:: Image/Software/Arduino/Arduino_library.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

7.	Lorsque l'ensemble des fichiers **zip** ont été téléchargés, aller sur l'IDE **Arduino** > **Croquis** > **inclure une bibliothèque** > **Ajouter la bibliothèque .ZIP**.

    .. figure:: Image/Software/Arduino/Arduino_library_add.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

8.	Sélectionnez une par une les différentes librairies à ajouter.

    .. figure:: Image/Software/Arduino/Arduino_library_zip.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Téléchargement et Paramètres du code Arduino
***********************************************

Téléchargement du code
=======================

#.	Ouvrir le lien suivant afin de télécharger (zip) le code Arduino (setier_datalogger_code.ino) dans le GITLAB associées au projet SETIER : https://gitlab.irstea.fr/reversaal/setier_datalogger 

        .. figure:: Image/Software/Arduino/Arduino_Git.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Dans **téléchargements**, trouver le dossier **setier_datalogger-main.zip**, puis l'extraire sur votre ordinateur/tablette le fichier télécharger afin d'accéder au code Arduino (setier_datalogger_code.ino). 

        .. figure:: Image/Software/Arduino/Arduino_Git_zip.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Aller dans le dossier extrait précédemment, puis ouvrir avec l'IDE Arduino le code **setier_datalogger_code**.

        .. figure:: Image/Software/Arduino/Arduino_open_code.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
Fichier de configuration
==========================

#.	Aller à l'onglet **config_file.h** pour commencer la configuration de votre datalogger, selon le type de datalogger assemblé précédemment.

        .. figure:: Image/Software/Arduino/Arduino_config.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Choisir le nom du datalogger et l'écrire ici (**par exemple, nous voulons nommer notre datalogger phy_Chi_02, donc nous allons changer TOTO_01 en phy_Chi_02**). Ce nom sera celui de la connexion wifi de votre datalogger.

        .. figure:: Image/Software/Arduino/datalogger_name.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

.. sidebar:: Seulement une ligne "#define Ma_Cadence" doit être décommentée, il est important de penser à commenter les lignes non utilisées. Pour cela, ajouter "//" au début des lignes non utilisées. De plus, la cadence 1 minute n'est pas compatible avec le datalogger physico-chimique.

             .. image:: Image/Software/Arduino/Arduino_cadence_warning.JPG

#.	Pour choisir la fréquence d'acquisition des mesures de votre datalogger, décommenter la ligne associée. Pour décommenter : supprimer "//" avant la ligne "#define Ma_Cadence" souhaitée. Par défaut, la cadence est de 1 minute. Il y a 6 différentes possibilitées de cadence : 1 minute, 5 minutes, 15 minutes, 30 minutes, 1 heure et 1 jour.


        .. figure:: Image/Software/Arduino/datalogger_cadence.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

.. sidebar:: Seulement un seul groupe de lignes "#define TYPE_DATALOGGER" doit être décommenté, il est donc important de penser à commenter les autres lignes relatives aux data logger non souhaités.

             .. image:: Image/Software/Arduino/datalogger_warning.JPG
	 
#.	Décommenter tout le groupe de lignes associées au data logger assemblé (la première partie est pour le débitmètre, la seconde est pour le physico-chimique et la troisième pour l'énergie).

        .. figure:: Image/Software/Arduino/Arduino_datalogger_choice.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Paramètre centrale hauteur d'eau / débitmètre
"""""""""""""""""""""""""""""""""""""""""""""""

Le capteur de débit est un capteur ultrason qui mesure la distance au premier obstacle en face de sa zone d'émission. Pour les différents types de canaux Venturi, il est possible de convertir cette distance en débit à l'aide des équations associées.

#.	Merci de définir "Ht_Debit_NUL", qui correspond à la distance entre la zone d'émission ultrasonique du capteur et le fond du canal Venturi. Pour cela, mesurer cette distance puis l'écrire ci-contre en millimètres. Par défaut, la distance défini est 700mm.

        .. figure:: Image/Software/Arduino/config_debit_nul.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Merci de définir "Profondeur_Canal", qui correspond à la distance entre le haut et le bas du canal Venturi (en millimètres). Par défaut, la distance est de 300mm.

        .. figure:: Image/Software/Arduino/config_canal.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Paramètres centrale physico-chimique
""""""""""""""""""""""""""""""""""""""
#.	Merci de sélectionner les capteurs qui seront utilisés. Seul un capteur  pH peut être connecté sur le port A0 // Seul un capteur Redox peut être connecté sur le port A1 // Seul un capteur O2 peut être connecté sur le port A2 // Seul un capteur conductivité peut être connecté sur le port A3. Si aucun capteur n'est connecté, écrire 0 à la place de 1.

        .. figure:: Image/Software/Arduino/config_physico.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Paramètres centrale énergie
"""""""""""""""""""""""""""""
#.	Merci de définir le capteur utilisé sur chaque port (si le capteur connecté au port A0 est un capteur 5A, écrire 5 à la place de 20). Il existe 3 types de capteurs : 5 A, 10 A et 20 A. Ici, les capteurs sur les ports A0 et A1 sont des capteurs 20 A, et sur le port A2, il n'y a pas de capteur, c'est pourquoi nous écrivons 0.

        .. figure:: Image/Software/Arduino/config_energy.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Téléversement du code sur le datalogger
=========================================

#.	Brancher le cable USB entre l'ordinateur et la carte **Arduino MKR WIFI 1010** de votre datalogger.

        .. figure:: Image/Software/Arduino/nopicture.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Dans la fenètre "**outils**", aller à "**type de carte**", "**Arduino SAMD (32-bits ARM Cortex-M0+)**" et choisir "**Arduino MKR Wifi 1010**". Si "**Arduino SAMD (32-bits ARM Cortex-M0+)**" n'est pas disponible, télécharger le depuis l'onglet "**gestionnaire de carte**".

        .. figure:: Image/Software/Arduino/Arduino_board_choose.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Dans la fenètre "**Outils**", aller à  "**Port**", et choisir le port COM associé au branchement du datalogger (le port COM associé au branchement du datalogger se présente comme suit "COM X (Arduino MKR WIFI 1010)".

        .. figure:: Image/Software/Arduino/Arduino_port_choose.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "téléverser" afin de charger le code sur la carte **Arduino MKR Wifi 1010**.

        .. figure:: Image/Software/Arduino/Arduino_code_upload.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Dans les 30 secondes après le televersement (clignotement LED) aller dans la fenètre "**Outils**", puis cliquer sur "**Moniteur Série**". Si le délai de 30 secondes est dépassé, débrancher le datalogger de votre PC, puis le rebrancher, puis suivre à nouveau les étapes 4 et 5.

        .. figure:: Image/Software/Arduino/Arduino_moniteur_serie.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Attendre la fin de la première mesure. Vérifier la concordance de l'affichage des données du moniteur série avec la réalité de la station assemblée (configuration, heure ...).

        .. figure:: Image/Software/Arduino/Arduino_moniteur_serie_open.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Etalonnage des capteurs (pour le datalogger Physico-chimique seulement)
************************************************************************

Cette partie permet d'expliquer le processus de calibration des différents capteurs physico-chimique, à l'aide de 2 ou 3 solutions étalon, en utilisant un fichier excel de calibration et le Wifi du datalogger. Le fichier excel, nommé Calibration_datalogger_physico.xlm est disponible sur le répertoire SETIER GITLAB. Attention de bien utiliser de nouvelles solutions étalon pour les étalonnages des capteurs.

Etalonnage pH
==============

#.	Prendre le capteur de pH et le nettoyer avec de l'eau déminéralisée.

        .. figure:: Image/Software/Arduino/Calibration/pH_cleaning.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sécher le capteur pH avec du papier absorbant. Attention à bien sécher l'ensemble du capteur.

        .. figure:: Image/Software/Arduino/Calibration/pH_drying.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Prendre la première solution étalon, et la mettre dans un bécher propre. Placer ensuite la sonde pH dans le bécher.

        .. figure:: Image/Software/Arduino/Calibration/pH_solution.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Presser le bouton poussoir afin d'activer le wifi sur votre datalogger, puis connecter votre téléphone ou votre ordinateur sur le wifi (ici, le nom de la connexion wifi est phy_Chi_02). Sur le navigateur, entrer l'adresse IP suivante : 192.168.4.1. 
**Le Wifi n'est accessible que si la station ne mesure pas (LED éteinte).**

        .. figure:: Image/Software/Arduino/Calibration/wifi_connexion.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur la page internet, cliquer sur "**Oui**" après "**affichage pH**" afin d'ouvrir la partie pH.

        .. figure:: Image/Software/Arduino/Calibration/pH_part.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "**lancer une mesure**" pour commencer une mesure avec le capteur pH dans la première solution étalon.

        .. figure:: Image/Software/Arduino/Calibration/pH_measurement.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur votre ordinateur faire "clique droit" sur le fichier "**setier_fichier_calibration**", puis sélectionner "propriétés". 

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	En bas de l'onglet "Général", cocher la case "débloquer".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "OK".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_3.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Ouvrir le fichier excel "**setier_fichier_calibration**". 

        .. figure:: Image/Software/Arduino/Calibration/excel_folder_pH.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Lorsque la mesure est terminée, reporter la tension mesurée dans le tableau excel (colonne "**valeur de la tension mesurée (mV)**") ainsi que la valeur théorique de la solution étalon (colonne "**valeur solution étalon**"). Ici, la solution étalon utilisée a un pH de 4.

        .. figure:: Image/Software/Arduino/Calibration/excel_measurement_pH1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Selon si vous effectuez une calibration en 2 ou 3 points, répeter les étapes 1 à 8 avec les autres solutions étalon. La mesure de tension ainsi que la valeur théorique de la solution étalon doivent alors être inscrites dans le tableau, sur les lignes point 2 et point 3. Ici les 2 autres solutions étalons ont un pH de 7 et 10.

#.	Lorsque l'ensemble des points de calibration ont été renseignés dans le tableau excel, cliquer sur "**calculer pH**" afin de calculer les coefficients A et B.

        .. figure:: Image/Software/Arduino/Calibration/pH_calcul.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Une courbe de calibration apparaît, et les coefficients A (A=Pente) et B (B=offset) sont affichés.

        .. figure:: Image/Software/Arduino/Calibration/pH_excel_coefficent.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Reporter les coefficients A et B individuellement dans la page internet du datalogger, puis cliquer sur "**envoyer**". Une seule saisie de coefficient par envoi.

        .. figure:: Image/Software/Arduino/Calibration/pH_end.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	La calibration du pH est terminée. Vous pouvez lancer une mesure dans une des solutions étalons afin de vérifier que l'étalonnage est réussi.

     
Calibration du Redox
=====================

#.	Prendre le capteur redox et le nettoyer avec de l'eau déminéralisée.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_cleaning.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sécher le capteur redox avec du papier absorbant. Attention à bien sécher l'ensemble du capteur.

        .. figure:: Image/Software/Arduino/Calibration/Redox_drying.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Prendre la première solution étalon, et la mettre dans un bécher propre. Placer ensuite la sonde redox dans le bécher.

        .. figure:: Image/Software/Arduino/Calibration/Redox_solution.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Presser le bouton poussoir afin d'activer le wifi sur votre datalogger, puis connecter votre téléphone ou votre ordinateur sur le wifi (ici, le nom de la connexion wifi est phy_Chi_02). Sur le navigateur, entrer l'adresse IP suivante : 192.168.4.1. 
**Le Wifi n'est accessible que si la station ne mesure pas (LED éteinte).** 

        .. figure:: Image/Software/Arduino/Calibration/wifi_connexion.JPG
	   :width: 900px
	   :align: center
	   :alt: alternate text

#.	Sur la page internet, cliquer sur "**Oui**" après "**affichage Redox**" afin d'ouvrir la partie redox.

        .. figure:: Image/Software/Arduino/Calibration/Redox_part.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "**lancer une mesure**" pour commencer une mesure avec le capteur redox dans la première solution étalon.

        .. figure:: Image/Software/Arduino/Calibration/Redox_measurement.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur votre ordinateur faire "clique droit" sur le fichier "**setier_fichier_calibration**", puis sélectionner "propriétés". 

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	En bas de l'onglet "Général", cocher la case "débloquer".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "OK".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_3.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Ouvrir le fichier excel "**setier_fichier_calibration**". Puis ouvrir la fenêtre Redox.

        .. figure:: Image/Software/Arduino/Calibration/excel_folder_Redox.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Lorsque la mesure est terminée, reporter la tension mesurée dans le tableau excel (colonne "**valeur de la tension mesurée (mV)**") ainsi que la valeur théorique de la solution étalon (colonne "**valeur solution étalon**"). Ici, la solution étalon utilisée a un potentiel redox de 220mV.

        .. figure:: Image/Software/Arduino/Calibration/excel_measurement_Redox1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Selon si vous effectuez une calibration en 2 ou 3 points, répeter les étapes 1 à 8 avec les autres solutions étalon. La mesure de tension ainsi que la valeur théorique de la solution étalon doivent alors être inscrites dans le tableau, sur les lignes point 2 et point 3. Ici les 2 autres solutions étalons ont un potentiel redox de 440mV et 660mV.

#.	Lorsque l'ensemble des points de calibration ont été renseignés dans le tableau excel, cliquer sur "**calculer pH**" afin de calculer les coefficients A et B.

        .. figure:: Image/Software/Arduino/Calibration/Redox_calcul.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Une courbe de calibration apparaît, et les coefficients A (A=Pente) et B (B=offset) sont affichés. Le coefficient B doit être proche de Zéro.

        .. figure:: Image/Software/Arduino/Calibration/Redox_excel_coefficent.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Reporter les coefficients A et B individuellement dans la page internet du datalogger, puis cliquer sur "**envoyer**". Une seule saisie de coefficient par envoi.

        .. figure:: Image/Software/Arduino/Calibration/Redox_end.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	La calibration du capteur Redox est terminée. Vous pouvez lancer une mesure dans une des solutions étalons afin de vérifier que l'étalonnage est réussi.


Calibration Conductivité
=========================

#.	Prendre le capteur de conductivité et le nettoyer avec de l'eau déminéralisée. Faire la même chose pour le capteur de température.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_cleaning.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sécher les deux capteurs avec du papier absorbant. **Attention, pour le capteur de conductivité, ne pas toucher à l'électrode de platine**.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_drying.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la première solution étalon, et la mettre dans un bécher propre. Placer ensuite la sonde conductivité et température dans le bécher.

        .. figure:: Image/Software/Arduino/Calibration/conductivity_solution.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Presser le bouton poussoir afin d'activer le wifi sur votre datalogger, puis connecter votre téléphone ou votre ordinateur sur le wifi (ici, le nom de la connexion wifi est phy_Chi_02). Sur le navigateur, entrer l'adresse IP suivante : 192.168.4.1. 
**Le Wifi n'est accessible que si la station ne mesure pas (LED éteinte).** 

        .. figure:: Image/Software/Arduino/Calibration/wifi_connexion.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur la page internet, cliquer sur "**Oui**" après "**affichage Conduc**" afin d'ouvrir la partie Conductivité.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_part.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "**lancer une mesure EC**" pour commencer une mesure avec le capteur conductivité et température dans la première solution étalon.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_measurement.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur votre ordinateur faire "clique droit" sur le fichier "**setier_fichier_calibration**", puis sélectionner "propriétés". 

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	En bas de l'onglet "Général", cocher la case "débloquer".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "OK".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_3.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Ouvrir le fichier excel "**setier_fichier_calibration**". Puis ouvrir la fenêtre Conductivité.

        .. figure:: Image/Software/Arduino/Calibration/excel_folder_Conductivity.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Lorsque la mesure est terminée, reporter la tension mesurée dans le tableau excel (colonne "**valeur de la tension mesurée (mV)**") la temperature mesurée par le capteur de température ainsi que la valeur théorique de la solution étalon  (colonne "**valeur solution étalon**"). Ici, la solution étalon est à 84 µS/cm.

        .. figure:: Image/Software/Arduino/Calibration/excel_measurement_Conductivity1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Selon si vous effectuez une calibration en 2 ou 3 points, répeter les étapes 1 à 8 avec les autres solutions étalon. La mesure de tension, de température ainsi que la valeur théorique de la solution étalon doivent être inscrites dans le tableau, sur les lignes point 2 et point 3. Ici les 2 autres solutions étalons sont à 200µS/cm et 1413µS/cm.

#.	Lorsque l'ensemble des points de calibration ont été renseignés dans le tableau excel, cliquer sur **calculer** afin de calculer les coefficients A et B.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_calcul.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Une courbe de calibration apparaît, et les coefficients A (A=Pente) et B (B=offset) sont affichés.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_excel_coefficent.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Reporter les coefficients A et B individuellement dans la page internet du datalogger, puis cliquer sur "**envoyer**". Une seule saisie de coefficient par envoi.

        .. figure:: Image/Software/Arduino/Calibration/Conductivity_end.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	La calibration du capteur conductivité est terminée. Vous pouvez lancer une mesure dans une des solutions étalons afin de vérifier que l'étalonnage est réussi.


Calibration O2
=================

#.	Prendre le capteur O2, dévisser sa membrane et la remplir d'une solution à 0.5mol/L de soude (NaOH), en utilisant une pipette. La membrane doit être pleine de soude. **Attention, la soude est un produit chimique dangeureux, porter gants, lunettes et blouse**.

      .. figure:: Image/Software/Arduino/Calibration/O2-filing.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Revisser la membrane sur le capteur O2. Attention, la soude pourrait déborder.

        .. figure:: Image/Software/Arduino/Calibration/O2-screw.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Nettoyer ensuite le capteur 02 avec de l'eau déminéralisée. Faire de même avec le capteur de température.

        .. figure:: Image/Software/Arduino/Calibration/O2_cleaning.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sécher les deux capteurs avec du papier absorbant. 

        .. figure:: Image/Software/Arduino/Calibration/O2_drying.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Pour la première étape d'étalonnage, les capteurs doivent être mis dans l'air ambiant sans lumière.

        .. figure:: Image/Software/Arduino/Calibration/O2_solution.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Presser le bouton poussoir afin d'activer le wifi sur votre datalogger, puis connecter votre téléphone ou votre ordinateur sur le wifi (ici, le nom de la connexion wifi est phy_Chi_02). Sur le navigateur, entrer l'adresse IP suivante : 192.168.4.1. Le Wifi n'est accessible que si la station ne mesure pas (LED éteinte). 

        .. figure:: Image/Software/Arduino/Calibration/wifi_connexion.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur la page internet, cliquer sur "**Oui**" après "**affichage O2**" afin d'ouvrir la partie O2.

        .. figure:: Image/Software/Arduino/Calibration/O2_part.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "**lancer une mesure**" pour commencer une mesure avec le capteur O2 dans la première solution étalon.

        .. figure:: Image/Software/Arduino/Calibration/O2_measurement.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Sur votre ordinateur faire "clique droit" sur le fichier "**setier_fichier_calibration**", puis sélectionner "propriétés". 

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	En bas de l'onglet "Général", cocher la case "débloquer".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "OK".

        .. figure:: Image/Software/Arduino/Calibration/Opening_calib_3.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Ouvrir le fichier excel "**setier_fichier_calibration**". Puis ouvrir la fenêtre O2.

        .. figure:: Image/Software/Arduino/Calibration/excel_folder_O2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
	   
#.	Lorsque la mesure est terminée, reporter la tension mesurée dans le tableau excel (colonne "**valeur de la tension mesurée (mV)**") l'altitude à laquelle la mesure est effectuée ainsi que la température. Ici, la mesure est effectuée à 170m au dessus du niveau de la mer, et la température est de 24°C. Avec ces informations, le tableau excel calculera automatiquement le taux d'oxygène en mg/L.

        .. figure:: Image/Software/Arduino/Calibration/excel_measurement_O2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Pour le second point, dissoudre 0.15 gr de sulfite dans 1L d'eau, puis attendre 20 minutes. Le sulfite va désoxygéner l'eau. Placer le capteur dans la solution d'eau désoxygénée puis lancer une mesure. Reporter la valeur de tension mesurée sur la ligne 0 mg/L. 

        .. figure:: Image/Software/Arduino/Calibration/O2_solution2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Cliquer sur "**calculer**" afin de calculer les coefficients A et B.

        .. figure:: Image/Software/Arduino/Calibration/O2_calcul.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Une courbe de calibration apparaît, et les coefficients A (A=Pente) et B (B=offset) sont connus.

        .. figure:: Image/Software/Arduino/Calibration/O2_excel_coefficent.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Reporter les coefficients A et B individuellement dans la page internet du datalogger, puis cliquer sur "**envoyer**". Une seule saisie de coefficient par envoi.

        .. figure:: Image/Software/Arduino/Calibration/O2_end.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	La calibration du capteur O2 est terminée. Vous pouvez lancer une mesure dans une des solutions étalons afin de vérifier que l'étalonnage est réussi.

Utilisation du Wifi
********************

Le datalogger est composé d'une LED permettant d'appréhender le système de fonctionnement de la centrale en temps réel. Lorsque la LED est éteinte, le système est en veille, on peut donc faire appel au Wifi via le bouton poussoir. Lorsque le système est en cours de mesure, la LED est allumée, le fait d'appuyer sur le bouton poussoir allumera donc le wifi à l'issue du cycle de mesure. Après allumage du Wifi, la LED est allumée en continue pendant 15 minutes, soit la durée pendant laquelle le Wifi restera allumé.

#.	Appuyer sur le bouton poussoir afin d'activer le wifi. Chercher sur son téléphone, ordinateur ou tablette le nom du wifi (ici phy_Chi_02) et connectez vous. Le Wifi n'est accessible que si la station ne mesure pas (LED éteinte). Attention à bien penser à désactiver les données mobiles sur votre téléphone.


#.	Entrer le lien suivant sur un navigateur **192.168.4.1**. La page ci-contre apparait.

        .. figure:: Image/Software/Wifi/Wifi_research.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Selon votre type de datalogger, vous avez différentes pages qui apparaissent. Pour tous les dataloggers, vous pouvez télécharger les données enregistrées ainsi que l'historique des connexions wifi. Vous pouvez également supprimer ces derniers fichiers. Il est également possible de faire une mesure instantanée, qui ne sera par contre pas enregistrée. Pour le datalogger physico-chimique, vous pouvez effectuez la calibration de vos capteurs comme expliqué précédemment.



Utilisation application VisuSetier
************************************

L'application VisuSetier permet, après avoir récupéré les données sur votre datalogger, de visionner celle-ci sous forme de graphique (mesure du datalogger en fonction du temps). 


VisuSetier datalogger Débitmètrique
====================================

L'application VisuSetier permet, dans ce cas de figure, de traçer en direct la mesure de hauteur d'eau ainsi que de débit (en fonction du canal Venturi sur lequel est présent le capteur).

#.	Lancer l'application Visusetier et ouvrir la partir "hauteur d'eau".

        .. figure:: Image/Software/Visusetier/Débit/Debit-0.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Ouvrir le fichier "Data.txt" sur l'application.

        .. figure:: Image/Software/Visusetier/Débit/Debit-1.JPG
	   :align: center
	   :height: 500px
	   :alt: alternate text

#.	Dans les cellules associées, indiquer la **profondeur du canal** et la **distance capteur / fond du canal**.

        .. figure:: Image/Software/Visusetier/Débit/Debit-2.JPG
	   :align: center
	   :height: 500px
	   :alt: alternate text

#.	Dans la cellule associée, indiquer le **type de canal Venturi** sur lequel est placé le canal (ici canal de type 2).

        .. figure:: Image/Software/Visusetier/Débit/Debit-3.JPG
	   :align: center
	   :height: 500px
	   :alt: alternate text

#.	L'onglet suivant apparait. Il représente le tracé de la hauteur d'eau et de débit.

        .. figure:: Image/Software/Visusetier/Débit/Debit-4.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

#.	Un onglet permettant de voir les données abberantes est disponibles. lorsque la valeur est supérieur à la profondeur du canal par exemple.

        .. figure:: Image/Software/Visusetier/Débit/Debit-5.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

VisuSetier datalogger Physico-chimique
========================================

L'application VisuSetier permet, dans ce cas de figure, de traçer en direct la mesure des différents capteur physico-chimique.

#.	Lancer l'application Visusetier et ouvrir la partir "physico-chimique".

        .. figure:: Image/Software/Visusetier/physico/physico-0.JPG
	   :align: center
	   :height: 500px
	   :alt: alternate text

#.	Choisir le fichier texte contenant les données des capteurs physico-chimique. Il est possible de choisir un fichier étalon afin de faire apparaitre les dates d'étalonnages sur les graphiques.

        .. figure:: Image/Software/Visusetier/physico/physico-1.JPG
	   :align: center
	   :height: 500px
	   :alt: alternate text
#.	Afficher les graphiques associées aux différents capteurs.

        .. figure:: Image/Software/Visusetier/physico/physico-2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Exemple de mise en place du datalogger_débit sur site
*********************************************************
Vous trouverez ci-joint un exemple de mise en place d'un datalogger débit sur le terrain. Le boitier a été fixé sur une planche PVC préalablement percée afin de laisser passer le capteur. Les contraintes associées à ce datalogger sont essentiellement dues au surelevement du capteur. En effet, la distance de fonctionnement du capteur étant comprise entre 10 cm et 150 cm, la zone émettrice du capteur doit être surélevé de 10 cm, ce qui donne dans le cas de figure ci-contre la mise en place d'un marche-pied de 15 cm.

Le capteur est alimenté par une batterie de voiture 12V-50A. Celle-ci, chargée à 100%, permet l'acquisition de données pendant 1 mois environ, avec une cadence de une mesure par minute.

        .. figure:: Image/Flowmeter/Setup/Setup1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Exemple de mise en place du datalogger_Physico-chimique sur site
******************************************************************
Vous trouverez ci-joint un exemple de mise en place d'un datalogger physico-chimique sur le terrain. Les contraintes associées à celui-ci sont souvent dues à la longueur des fils (moins de 2m pour les capteurs de conductivité et d'Oxygène dissous). Il est également important de ne pas immerger la totalité des capteurs sous l'eau (seulement leurs membranes).

Ici, chaque capteur a été fixé à une tiges métalliques, fixées elles même sur les caibotis du canal de sortie de la station. L'alimentation du datalogger se fait à l'aide d'une batterie de voiture 12V-40A branchée à l'aide de fiche banane. 

        .. figure:: Image/Physico_Chemical/Setup/Setup1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Ici, on voit les 4 capteurs en position de mesures. La centrale a été recouverte par une boîte afin d'éviter tout problème dues à d'éventuelles pluies. 

        .. figure:: Image/Physico_Chemical/Setup/Setup2.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text

Exemple de mise en place du datalogger_Energie sur site
**********************************************************

Vous trouverez ci-joint un exemple de mise en place d'un datalogger Energie sur le terrain. Les contraintes associées à celui-ci sont souvent dues à la longueur des fils d'alimentation et des capteur. S'agissant de mesures de courant, les capteurs ne sont pas étanches, et doivent donc être placées à l'abri.

Ici, Le boitier à été placé au sein d'une armoire électrique dans une station d'épuration de 90000 EH, afin de faire des mesures sur différents types de pompes, centrifugeuses... L'alimentation du capteur se fait à l'aide d'une prise branchée sur secteur, sur laquelle deux fiches bananes males ont été fixées. 

        .. figure:: Image/Energy/Setup/Energy_box.JPG
	   :align: center
	   :height: 500px
	   :alt: alternate text

Les pinces ont été placées sur 3 appareils différents, dont le courant est censé rester dans la gamme mesure des capteurs associées. L'objectif est notamment de voir la puissance de certaines pompes qui n'entre en fonctionnement que quelques minutes par heure. 

        .. figure:: Image/Energy/Setup/Setup1.JPG
	   :width: 600px
	   :align: center
	   :alt: alternate text
