*******************************************
DataLogger SETIER ENERGIE V0.0 Hardware FR
*******************************************

     .. figure:: Image/Energy/Hardware/final.JPG
	   :width: 900px
	   :align: center
	   :height: 595px
	   :alt: alternate text
	   :figclass: align-center

.. attention::
	Nous vous invitons à vous referer à ce document pour assembler le datalogger-Energie V1.02 associé au projet SETIER.
	
.. attention::
	**SETIER est un projet participatif ouvert à tous, cependant l'assemblage des dataloggers associés requiere le respect des règles de sécurités associées à l'utilisation de cartes électroniques notamment. Les dataloggers SETIER doivent être assemblés dans un contexte professionel par des personnes ayant des connaissances en électronique. L'équipe SETIER ne peut en aucun cas être responsable de tout dommage matériels ou humain qui pourrait subvenir lors de l'assemblage ou de l'utilisation d'un datalogger SETIER. De plus, l'équipe SETIER ne pourra être portée responsable si le datalogger ne fonctionne pas à la fin de l'assemblage.You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.**

Données Techniques
*******************

+-------------------------------+--------------------+-----------+
| **Paramètres**                | **Specifications** | Unités    |
+-------------------------------+--------------------+-----------+
|Tension d'entrée (VCC)         |3.3 à 5.5           |V          |
+-------------------------------+--------------------+-----------+
|Intervalle de mesure           |0 à 5, 10 ou 20     |A          |
+-------------------------------+--------------------+-----------+
|Précision de mesure            |+-1%                |%          |             
+-------------------------------+--------------------+-----------+
|Non-linearité                  |+-0.2               |%          |
+-------------------------------+--------------------+-----------+
|Longueur de cable              |1                   |m          |
+-------------------------------+--------------------+-----------+
|Température d'utilisation      |-25 à 70            |°C         |
+-------------------------------+--------------------+-----------+
|Taille d'ouverture (Lxl)       |13x13               |mm         |
+-------------------------------+--------------------+-----------+
|Stockage des données           |µ carte SD          |           |
+-------------------------------+--------------------+-----------+

Assemblage du datalogger
**************************

Etape 1 : Assemblage de la partie électronique
===============================================

La première partie consiste à assembler les différentes cartes électronique composant le datalogger.


Liste du matériel de l'étape 1
-------------------------------

+-------------------------------+--------------------+----------------------------------------------------+
|Nom                            |Nombre              |Image                                               | 
+-------------------------------+--------------------+----------------------------------------------------+
|MKR MEM SHIELD                 |1                   |.. image:: Image/General/Electronic/MKR_SD.JPG      |
+-------------------------------+--------------------+----------------------------------------------------+
|MKR Connector Carrier          |1                   |.. image:: Image/General/Electronic/MKR_CARRIER.JPG | 
+-------------------------------+--------------------+----------------------------------------------------+
|MKR WIFI 1010	                |1                   |.. image:: Image/General/Electronic/MKR_WIFI.JPG    | 
+-------------------------------+--------------------+----------------------------------------------------+
|Carte µSD                      |1                   |.. image:: Image/General/Electronic/SD.JPG          | 
+-------------------------------+--------------------+----------------------------------------------------+
|Module RTC                     |1                   |.. image:: Image/General/Electronic/RTC.JPG         | 
+-------------------------------+--------------------+----------------------------------------------------+
|Diviseur de tension            |1                   |.. image:: Image/General/Electronic/VOLTAGE.JPG     | 
+-------------------------------+--------------------+----------------------------------------------------+
|Double cable Grove             |1                   |.. image:: Image/General/Electronic/cable-double.JPG|
+-------------------------------+--------------------+----------------------------------------------------+
|Pile CR1220 lithium            |1                   |.. image:: Image/General/Electronic/Battery.JPG     | 
+-------------------------------+--------------------+----------------------------------------------------+

.. sidebar::  Les cartes electroniques doivent être branchées dans le bon sens (Pin 5V sur Pin 5V par exemple).

             .. image:: Image/General/Electronic/WARNING_1.JPG

1.	Brancher la carte **MKR MEM Shield** sur la carte **MKR Connector Carrier**.

     .. figure:: Image/General/Electronic/MKR_MEM_On_CARRIER.JPG
	   :width: 1037px
	   :align: center
	   :height: 577px
	   :alt: alternate text
	   :figclass: align-center

2.	Connecter la carte **MKR WIFI 1010** sur la carte **MKR MEM Shield**. 

	 .. figure:: Image/General/Electronic/MKR_WIFI_On_CARRIER.JPG
	   :width: 996px
	   :align: center
	   :height: 502px
	   :alt: alternate text
	   :figclass: align-center

3.	Inserer la carte **µSD** dans la carte **MKR MEM SHIELD**.

	.. figure:: Image/General/Electronic/SD_On_CARRIER.JPG
	   :width: 925px
	   :align: center
	   :height: 461px
	   :alt: alternate text
	   :figclass: align-center

.. sidebar::  Le câble doit être branché dans le bon sens.

             .. image:: Image/General/Electronic/WARNING_2.JPG

4.	Brancher la partie simple du **double cable Grove** sur le port **TWI** de la carte **ARDUINO CONNECTOR CARRIER**.

        .. figure:: Image/Energy/Hardware/Doble_wire_on_CARRIER.JPG
	   :width: 831px
	   :align: center
	   :height: 340px
	   :alt: alternate text
	   :figclass: align-center

5.	Prendre la carte **RTC** et brancher une des extrémité du double cable sur celle-ci. L'autre extémité sera branché ultérieurement.

        .. figure:: Image/Energy/Hardware/Doble_wire_on_RTC.JPG
	   :width: 789px
	   :align: center
	   :height: 610px
	   :alt: alternate text
	   :figclass: align-center

6.	Prendre la pile CR1220 et la brancher sur la carte RTC.

	.. figure:: Image/Physico_Chemical/Hardware/RTC_battery.JPG
	   :width: 900px
	   :align: center
	   :height: 707px
	   :alt: alternate text
	   :figclass: align-center

7.	Prendre la carte **Diviseur de tension** et brancher son cable.

        .. figure:: Image/Energy/Hardware/voltage_divider_wire.JPG
	   :width: 900px
	   :align: center
	   :height: 562px
	   :alt: alternate text
	   :figclass: align-center

8.	Brancher l'autre extrémité du cable  **Diviseur de tension** dans le port A5-A6 de la carte **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Energy/Hardware/voltage_divider_connexion.JPG
	   :width: 828px
	   :align: center
	   :height: 638px
	   :alt: alternate text
	   :figclass: align-center

Etape 2 : Assemblage mécanique
===============================

Cette seconde étape consiste à l'assemblage mécanique des différentes parties du datalogger, et de leurs montage dans un boitier.

Liste du matériel
------------------

+-----------------------------------------------+--------------------+----------------------------------------------------+
|Nom                                      	|Nombre              |Image                                               |   
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Carte 4 relais                           	|2                   |.. image:: Image/General/Electronic/relai.JPG       |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Pince ampéromètrique SEN0211 (20A Sensor)	|1                   |.. image:: Image/General/Electronic/Sen0211.JPG     |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Pince ampéromètrique SEN0288 (10A Sensor)	|1                   |.. image:: Image/General/Electronic/Sen0211.JPG     |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Pince ampéromètrique SEN0287 (5A Sensor) 	|1                   |.. image:: Image/General/Electronic/Sen0211.JPG     |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Support PVC                              	|1                   |.. image:: Image/General/Electronic/PVC_pc.JPG      |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Boîte 344 x 289 x 117mm                  	|1                   |.. image:: Image/General/Electronic/pc_Box.JPG      |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Entretoise (M2 et M3) 20 mm de long      	|+/-10 de chaque     |.. image:: Image/General/Electronic/entretoise.JPG  |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Vis (M2 et M3)                           	|+/-10 de chaque     |.. image:: Image/General/Electronic/vis.JPG         |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Ecrou (M2 et M3)                         	|+/-10 de chaque     |.. image:: Image/General/Electronic/ecrou.JPG       |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|12 cm de Cable rouge/noir diamètre 1,6 mm	|1 de chaque couleur |.. image:: Image/General/Electronic/cable-fin.JPG   |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|12 cm de Cable rouge/noir diamètre 3 mm  	|1 de chaque couleur |.. image:: Image/General/Electronic/cable-épais.JPG |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Cosse à sertir                           	|2                   |.. image:: Image/General/Electronic/Pod.JPG         |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Fiche banane rouge (diamètre extérieur 12 mm)	|1                   |.. image:: Image/General/Electronic/Red_Banana.JPG  |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Fiche banane noire (diamètre extérieur 12 mm)	|1                   |.. image:: Image/General/Electronic/Black_Banana.JPG|
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Presse-étoupe (diamètre extérieur 18 mm)     	|3                   |.. image:: Image/General/Electronic/Cable_Gl.JPG    |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Cables (Grove/Picot)                     	|3                   |.. image:: Image/General/Electronic/Grove_picot.JPG |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Cable Picot/Picot (rouge & noir)         	|10                  |.. image:: Image/General/Electronic/cable-picot.JPG |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Alimentation voltcraft                   	|1                   |.. image:: Image/General/Electronic/Alimentation.JPG|
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Cable µ USB mâle/ USB-A mâle             	|1                   |.. image:: Image/General/Electronic/USB-Cable.JPG   |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Bouton poussoir (diamètre extérieur 14 mm)   	|1                   |.. image:: Image/General/Electronic/Bouton.JPG      |
+-----------------------------------------------+--------------------+----------------------------------------------------+
|Module bornier à vis                     	|1                   |.. image:: Image/General/Electronic/Screw_Gr.JPG    |
+-----------------------------------------------+--------------------+----------------------------------------------------+

#.	Décoller le scotch marron des différentes parties de la boite de protection présente dans le carton du **Grove Channel 4 relay**.

        .. figure:: Image/General/Electronic/Relay_box_unstick.JPG
	   :width: 900px
	   :align: center
	   :height: 484px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la carte électronique *4 relais** et la fixer sur le support fourni à l'aide des vis.

        .. figure:: Image/General/Electronic/Relay_box_1.JPG
	   :width: 909px
	   :align: center
	   :height: 721px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher la second extremité du cable double sur la carte électronique  **4 Relais** (En passant à travers le trou de la boite de protection de la carte électronique). Attention, il est nécessaire de forcer un peu pour faire passer le cable.

        .. figure:: Image/Energy/Hardware/Doble_wire_on_Relay.JPG
	   :width: 900px
	   :align: center
	   :height: 391px
	   :alt: alternate text
	   :figclass: align-center

5.	Prendre les 4 parties verticales de la boite de protection et les insérer dans le support. Ensuite, prendre la partie supérieure et la fixer à l'aide des entretoises et des vis fournies avec la carte électronique.
        
.. figure:: Image/General/Electronic/Relay_box_2.JPG
	   :width: 813px
	   :align: center
	   :height: 697px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre un morceau de bois, PVC, ou tout élément non conducteur, et le couper suivant le fichier "**Energy_datalogger_hole.pdf**" afin de réaliser la plaque fixation des différents éléments du datalogger. Percer les trous afin de placer les différentes entretoises, que vous fixerez.

        .. figure:: Image/Energy/Hardware/box_bottom.JPG
	   :width: 939px
	   :align: center
	   :height: 522px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la **boite** contenant la carte **4 Relais** sur le PVC.

        .. figure:: Image/Energy/Hardware/box_relay.JPG
	   :width: 836px
	   :align: center
	   :height: 656px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **Arduino MKR CARRIER** sur les entretoises associées.

        .. figure:: Image/Energy/Hardware/box_MKR.JPG
	   :width: 822px
	   :align: center
	   :height: 672px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **RTC** sur les entretoises associées.

        .. figure:: Image/Energy/Hardware/box_RTC.JPG
	   :width: 934px
	   :align: center
	   :height: 672px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **Diviseur de tension** sur les entretoises associées.

        .. figure:: Image/Energy/Hardware/box_Divider.JPG
	   :width: 834px
	   :align: center
	   :height: 671px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la **carte électronique** des capteurs et brancher le cable fourni avec les capteurs sur cette carte.

        .. figure:: Image/Energy/Hardware/sensor_board.JPG
	   :width: 748px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher le cable Grove à picot sur la carte électronique **MKR CARRIER**. Sur cette carte électronique, les sorties dédiées au branchement des capteurs de courant sont situées sur le port A0, A1, A2 et A3 (içi, nous n'utilisons que les ports A0, A1 et A2 pour la connexion de 3 capteurs).

        .. figure:: Image/Energy/Hardware/wire_on_A0port.JPG
	   :width: 800px
	   :align: center
	   :height: 670px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le fil noir de ce cable et le brancher sur le connecteur triple du cable lié à la carte du capteur (noir sur noir). 

        .. figure:: Image/Energy/Hardware/black_wire.JPG
	   :width: 768px
	   :align: center
	   :height: 609px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le cable jaune et le brancher sur le connecteur triple du cable lié à la carte du capteur (jaune sur bleu).  

        .. figure:: Image/Energy/Hardware/yellow_wire.JPG
	   :width: 824px
	   :align: center
	   :height: 671px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre un cable rouge indépendant et le brancher sur le branchement rouge du cable lié à la carte du capteur. 

        .. figure:: Image/Energy/Hardware/red_wire.JPG
	   :width: 856px
	   :align: center
	   :height: 664px
	   :alt: alternate text
	   :figclass: align-center

.. sidebar::  Si vous voulez brancher plus de capteurs, merci de suivre l'étape 5 à 10  pour le port A0, A1, A2 et A3.
       
	    :subtitle: Attention, le capteur sur le port A0 doit être branché sur le relais SW4, celui sur le port A1 sur le relais SW3...

11.	A l'aide de scotch, coller ensemble les cables branchées précédement afin d'éviter tout débranchement malencontreux. 
        
        .. figure:: Image/Energy/Hardware/wire_scotch.JPG
	   :width: 818px
	   :align: center
	   :height: 624px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher le cable rouge provenant de la carte du capteur sur la partie gauche du relais SW4.
        
        .. figure:: Image/Energy/Hardware/wire_on_relay.JPG
	   :width: 815px
	   :align: center
	   :height: 662px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher le cable rouge provenant du port A0 sur la partie centrale du relais SW4. Faire les étapes 16 et 17 pour tous les capteurs branchées sur ce Datalogger (par exemple, ici, les relais SW4, SW3 et SW2 seront utilisées).
        
        .. figure:: Image/Energy/Hardware/wire_on_relay2.JPG
	   :width: 869px
	   :align: center
	   :height: 674px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **module bornier à vis** sur les entretoises associées.

        .. figure:: Image/Energy/Hardware/box_grove.JPG
	   :width: 824px
	   :align: center
	   :height: 670px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter la carte **module bornier à vis** sur la carte arduino MKR Carrier.

        .. figure:: Image/Energy/Hardware/grove_connexion.JPG
	   :width: 843px
	   :align: center
	   :height: 662px
	   :alt: alternate text
	   :figclass: align-center

#.	Couper deux fois 12cm de **cable rouge diamètre 3 mm**. Utiliser une **pince à dénuder** afin de dénuder 1 cm à chaque extrémité du cable.

        .. figure:: Image/Flowmeter/Hardware/red_wire_stripped.JPG
	   :width: 816px
	   :align: center
	   :height: 778px
	   :alt: alternate text
	   :figclass: align-center

#.	Couper deux fois 12cm de **cable noir diamètre 3 mm**. Utiliser une **pince à dénuder** afin de dénuder 1 cm à chaque extrémité du cable.

        .. figure:: Image/Flowmeter/Hardware/Black_wire_stripped.JPG
	   :width: 867px
	   :align: center
	   :height: 827px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre une **cosse à sertir**. Utiliser une **pince à sertir** afin de le fixer sur une seule des extrémités des 2 **cables rouges diamètre 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/red_wire_pod.JPG
	   :width: 792px
	   :align: center
	   :height: 682px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre une **cosse à sertir**. Utiliser une **pince à sertir** afin de le fixer sur une seule des extrémités des 2 **cables noirs diamètre 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/black_wire_pod.JPG
	   :width: 740px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre un des **cable noir diamètre 3 mm**, brancher 1 extrémité du cable noir dans le port **GRD** de la carte **diviseur de tension**. Vissez-le.

        .. figure:: Image/Energy/Hardware/divider_wire.JPG
	   :width: 892px
	   :align: center
	   :height: 667px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre un des "cable rouge diamètre 3 mm", brancher 1 extrémité du cable rouge dans le port **VOL** de la carte **diviseur de tension**. Vissez-les.

        .. figure:: Image/Energy/Hardware/divider_wire2.JPG
	   :width: 831px
	   :align: center
	   :height: 668px
	   :alt: alternate text
	   :figclass: align-center

#.	Percer un trou par capteur dans la boîte au diamétre M18.

        .. figure:: Image/Energy/Hardware/box_cable_gland.JPG
	   :width: 894px
	   :align: center
	   :height: 523px
	   :alt: alternate text
	   :figclass: align-center

#.	Poser et fixer aux endroits prévues à cet effet la plaque de PVC dans la boite. 

        .. figure:: Image/Energy/Hardware/box_PVC.JPG
	   :width: 899px
	   :align: center
	   :height: 673px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer les 3 presse-étoupes de part et autres de ceux-ci.

        .. figure:: Image/Energy/Hardware/cable_gland.JPG
	   :width: 900px
	   :align: center
	   :height: 615px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre les **capteurs de courant** (SEN0287, SEN0288 or SEN0211) et insérer leurs cables à l'intérieur des presses étoupes. 

        .. figure:: Image/Energy/Hardware/current_sensor_cable_gland.JPG
	   :width: 900px
	   :align: center
	   :height: 631px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le capteur jack des capteurs sur leurs cartes electronique.

        .. figure:: Image/Energy/Hardware/current_sensor_connected.JPG
	   :width: 900px
	   :align: center
	   :height: 644px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer leurs cartes electroniques sur une plaque PVC prépercée en utilisant des colliers.

        .. figure:: Image/Energy/Hardware/current_sensor_fixation.JPG
	   :width: 900px
	   :align: center
	   :height: 639px
	   :alt: alternate text
	   :figclass: align-center

#.	Mettre en place le **bouton poussoir** dans son trou (trou de diamètre 14 mm).

        .. figure:: Image/Energy/Hardware/push_button_box.JPG
	   :width: 900px
	   :align: center
	   :height: 601px
	   :alt: alternate text
	   :figclass: align-center

#.	fixer le **bouton poussoir** dans son trou à l'aide d'un écrou (compris avec le bouton poussoir).

        .. figure:: Image/Energy/Hardware/push_button_fix.JPG
	   :width: 767px
	   :align: center
	   :height: 668px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter les deux cables du **bouton poussoir** sur le **module bornier à vis** (un des cables doit être branché sur le port VCC et l'autre sur le port D2), puis les visser.

        .. figure:: Image/Energy/Hardware/push_button_connection.JPG
	   :width: 787px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Inserez la fiche banane rouge dans un des trous de la boîte prévues à cet effet (trou de diamètre 12 mm). vissez-la afin de la fixer.

        .. figure:: Image/Energy/Hardware/banana_plug1.JPG
	   :width: 839px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Inserez la fiche banane noire dans l'autre trou de la boîte prévue à cet effet (trou de diamètre 12 mm). vissez-la afin de la fixer.

        .. figure:: Image/Energy/Hardware/banana_plug2.JPG
	   :width: 824px
	   :align: center
	   :height: 663px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher une extrémité du second **cable rouge diamètre 3 mm** sur la borne VIN de la carte **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/CARRIER_VIN.JPG
	   :width: 850px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher une extrémité du second **cable noir diamètre 3 mm** sur la borne GND de la carte **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/CARRIER_GND.JPG
	   :width: 847px
	   :align: center
	   :height: 669px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher sur la fiche banane rouge les extrémités des deux **cables rouges diamètre 3 mm** sertis provenant du **diviseur de tension** et de la carte **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/banana_connect_red.JPG
	   :width: 846px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher sur la fiche banane noire les extrémités des deux **cables noires diamètre 3 mm** sertis provenant du **diviseur de tension** et de la carte **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/banana_connect_black.JPG
	   :width: 848px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Fermer la boite. Le datalogger Energie est prêt à être utilisé. **Soyez attentif, le datalogger nécessite une alimentation en 12V continu, utilisez donc une source de courant adaptée**.

        .. figure:: Image/Energy/Hardware/final.JPG
	   :width: 900px
	   :align: center
	   :height: 595px
	   :alt: alternate text
	   :figclass: align-center
