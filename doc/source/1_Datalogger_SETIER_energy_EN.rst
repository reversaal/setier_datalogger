*******************************************
DataLogger SETIER ENERGY V0.0 Hardware EN
*******************************************

     .. figure:: Image/Energy/Hardware/final.JPG
	   :width: 900px
	   :align: center
	   :height: 595px
	   :alt: alternate text
	   :figclass: align-center

.. attention::
	We invite you to refer to this document to assemble Datalogger SETIER V0.00 FLOWMETER Hardware Part.
	
.. attention::
	**SETIER is a participative project open to all, it requires skills in electronics and to respect the safety rules. SETIER must be assembled in a professional context and by people competent in electronics. The SETIER team cannot be held responsible for any material or human damage which would be associated with the use or the assembly of Datalogger SETIER. The SETIER team cannot be held responsible if the equipment does not work after assembly. You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.**

Technical date
*******************

+-------------------------------+--------------------+-----------+
| **Parameter**                 | **Specifications** | Units     |
+-------------------------------+--------------------+-----------+
|Input voltage (VCC)            |3.3 to 5.5          |V          |
+-------------------------------+--------------------+-----------+
|Effective measurement range    |0 to 5, 10 or 20    |A          |
+-------------------------------+--------------------+-----------+
|Measurement accuracy           |+/-1%               |%          |             
+-------------------------------+--------------------+-----------+
|Non-linearity                  |+/-0.2              |%          |
+-------------------------------+--------------------+-----------+
|Cable lenght                   |1                   |m          |
+-------------------------------+--------------------+-----------+
|Operation temperature          |-25 to 70           |°C         |
+-------------------------------+--------------------+-----------+
|Opening size (Lxl)             |13x13               |mm         |
+-------------------------------+--------------------+-----------+
|Data storage                   |µ SD card           |           |
+-------------------------------+--------------------+-----------+

Assembly of the datalogger
**************************

Step 1 : Electronic board assembly
===============================================

The first part consists of assembling the different electronic board together.

Material List
-------------------------------

+-------------------------------+--------------------+----------------------------------------------------+
|Name                           |How Many            |Picture						  | 
+-------------------------------+--------------------+----------------------------------------------------+
|MKR MEM SHIELD                 |1                   |.. image:: Image/General/Electronic/MKR_SD.JPG      |
+-------------------------------+--------------------+----------------------------------------------------+
|MKR Connector Carrier          |1                   |.. image:: Image/General/Electronic/MKR_CARRIER.JPG | 
+-------------------------------+--------------------+----------------------------------------------------+
|MKR WIFI 1010	                |1                   |.. image:: Image/General/Electronic/MKR_WIFI.JPG    | 
+-------------------------------+--------------------+----------------------------------------------------+
|µ SD card                      |1                   |.. image:: Image/General/Electronic/SD.JPG          | 
+-------------------------------+--------------------+----------------------------------------------------+
|RTC module                     |1                   |.. image:: Image/General/Electronic/RTC.JPG         | 
+-------------------------------+--------------------+----------------------------------------------------+
|Voltage divider board          |1                   |.. image:: Image/General/Electronic/VOLTAGE.JPG     | 
+-------------------------------+--------------------+----------------------------------------------------+
|Double wire grove              |1                   |.. image:: Image/General/Electronic/cable-double.JPG|
+-------------------------------+--------------------+----------------------------------------------------+
|CR1220 lithium battery         |1                   |.. image:: Image/General/Electronic/Battery.JPG     | 
+-------------------------------+--------------------+----------------------------------------------------+

.. sidebar::  Be careful, the electronic boards needs to be connect on the right side (GND Pin on GND Pin for example).

             .. image:: Image/General/Electronic/WARNING_1.JPG

1.	Connect the **MKR MEM Shield** on the **MKR Connector Carrier**.

     .. figure:: Image/General/Electronic/MKR_MEM_On_CARRIER.JPG
	   :width: 1037px
	   :align: center
	   :height: 577px
	   :alt: alternate text
	   :figclass: align-center

2.	Connect the **MKR WIFI 1010** on the **MKR MEM Shield**. 

	 .. figure:: Image/General/Electronic/MKR_WIFI_On_CARRIER.JPG
	   :width: 996px
	   :align: center
	   :height: 502px
	   :alt: alternate text
	   :figclass: align-center

3.	Insert the **µSD** card inside the **MKR MEM SHIELD**.

	.. figure:: Image/General/Electronic/SD_On_CARRIER.JPG
	   :width: 925px
	   :align: center
	   :height: 461px
	   :alt: alternate text
	   :figclass: align-center

.. sidebar::  The wire need to be connect in the right side.

             .. image:: Image/General/Electronic/WARNING_2.JPG

4.	Connect the simple part of the **double wire Grove** sur le port **TWI** de la carte **ARDUINO CONNECTOR CARRIER**.

        .. figure:: Image/Energy/Hardware/Doble_wire_on_CARRIER.JPG
	   :width: 831px
	   :align: center
	   :height: 340px
	   :alt: alternate text
	   :figclass: align-center

5.	Take the **RTC module** and connect on of doble wire extremity on it. The other side will be connected later.

        .. figure:: Image/Energy/Hardware/Doble_wire_on_RTC.JPG
	   :width: 789px
	   :align: center
	   :height: 610px
	   :alt: alternate text
	   :figclass: align-center

6.	Take the **CR1220 lithium battery** and insert it on the **RTC module**.

	.. figure:: Image/Physico_Chemical/Hardware/RTC_battery.JPG
	   :width: 900px
	   :align: center
	   :height: 707px
	   :alt: alternate text
	   :figclass: align-center

7.	Take the **voltage divider board** and plug its wire on.

        .. figure:: Image/Energy/Hardware/voltage_divider_wire.JPG
	   :width: 900px
	   :align: center
	   :height: 562px
	   :alt: alternate text
	   :figclass: align-center

8.	Connect the other side of the wire from the **voltage divider board** to the **A5-A6** of the **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Energy/Hardware/voltage_divider_connexion.JPG
	   :width: 828px
	   :align: center
	   :height: 638px
	   :alt: alternate text
	   :figclass: align-center

Step 2 : Mechanical assembly
===============================

That second step consist of assembling the of the different part of the datalogger, and to fix it inside a box.

Material list
------------------

+--------------------------------------------------+--------------------+----------------------------------------------------+
|Name                                              |How many            |Picture                                             |   
+--------------------------------------------------+--------------------+----------------------------------------------------+
|4 relay board                                     |2                   |.. image:: Image/General/Electronic/relai.JPG       |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Current clamp SEN0211 (20A Sensor)                |1                   |.. image:: Image/General/Electronic/Sen0211.JPG     |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Current clamp SEN0288 (10A Sensor)                |1                   |.. image:: Image/General/Electronic/Sen0211.JPG     |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Current clamp SEN0287 (5A Sensor)                 |1                   |.. image:: Image/General/Electronic/Sen0211.JPG     |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|PVC plate                                         |1                   |.. image:: Image/General/Electronic/PVC_pc.JPG      |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Box 344 x 289 x 117mm                             |1                   |.. image:: Image/General/Electronic/pc_Box.JPG      |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Spacer (M2 and M3) 20 mm long                     |+/-10 of each       |.. image:: Image/General/Electronic/entretoise.JPG  |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Screw (M2 and M3)                                 |+/-10 of each       |.. image:: Image/General/Electronic/vis.JPG         |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Nuts (M2 and M3)                                  |+/-10 of each       |.. image:: Image/General/Electronic/ecrou.JPG       |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|12 cm red/black multi-strand cable diameter 1,6 mm|1 of each color     |.. image:: Image/General/Electronic/cable-fin.JPG   |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|12 cm red/black multi-strand cable diameter 3 mm  |1 of each color     |.. image:: Image/General/Electronic/cable-épais.JPG |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Crimp lug                                         |2                   |.. image:: Image/General/Electronic/Pod.JPG         |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Red banana plug (external diameter 12 mm)         |1                   |.. image:: Image/General/Electronic/Red_Banana.JPG  |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Black banana plug (external diameter 12 mm)       |1                   |.. image:: Image/General/Electronic/Black_Banana.JPG|
+--------------------------------------------------+--------------------+----------------------------------------------------+
|cable gland (external diameter 18 mm)             |3                   |.. image:: Image/General/Electronic/Cable_Gl.JPG    |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Grove/Picot wire                                  |3                   |.. image:: Image/General/Electronic/Grove_picot.JPG |
+--------------------------------------------------+--------------------+----------------------------------------------------+
| Picot/Picot wire(red and black)                  |10                  |.. image:: Image/General/Electronic/cable-picot.JPG |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Voltcraft power supply                            |1                   |.. image:: Image/General/Electronic/Alimentation.JPG|
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Male µ USB/male USB-A wire                        |1                   |.. image:: Image/General/Electronic/USB-Cable.JPG   |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Push button (external diameter 14 mm)             |1                   |.. image:: Image/General/Electronic/Bouton.JPG      |
+--------------------------------------------------+--------------------+----------------------------------------------------+
|Grove screw terminal                              |1                   |.. image:: Image/General/Electronic/Screw_Gr.JPG    |
+--------------------------------------------------+--------------------+----------------------------------------------------+

#.	Unstick the brown scotch of the different side of protection box of the **4 relay board**.

        .. figure:: Image/General/Electronic/Relay_box_unstick.JPG
	   :width: 900px
	   :align: center
	   :height: 484px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the electronic board of the **4 relay board** and fix it on the support provided with, using screw.

        .. figure:: Image/General/Electronic/Relay_box_1.JPG
	   :width: 909px
	   :align: center
	   :height: 721px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the second extremity of the **double wire grove** on the **4 Relay board** (passing through the hole of the protection box). Be careful, it's necessary to force a little.

        .. figure:: Image/Energy/Hardware/Doble_wire_on_Relay.JPG
	   :width: 900px
	   :align: center
	   :height: 391px
	   :alt: alternate text
	   :figclass: align-center

5.	Take the 4 vertical side of the protection box, and insert it on the **4 relay board** support. Then, take the top part and fix it using spacer provided with the **4 relay board**.       

	.. figure:: Image/General/Electronic/Relay_box_2.JPG
	   :width: 813px
	   :align: center
	   :height: 697px
	   :alt: alternate text
	   :figclass: align-center

#.	Take a piece of wood, PVC, or some other non-conductive element, and cut it following the folder "**Energy_datalogger_hole.pdf**" to do the fixing plate of the different datalogger board. Drill hole to put on the spacer inside it, then fix it.

        .. figure:: Image/Energy/Hardware/box_bottom.JPG
	   :width: 939px
	   :align: center
	   :height: 522px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the protection box of the **4 Relay board** on the PVC plate.

        .. figure:: Image/Energy/Hardware/box_relay.JPG
	   :width: 836px
	   :align: center
	   :height: 656px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **Arduino MKR Carrier** on the spacer provided for that purpose, with some screw (following the folder ""**Energy_datalogger_hole.pdf**"). 

  .. figure:: Image/Energy/Hardware/box_MKR.JPG
	   :width: 822px
	   :align: center
	   :height: 672px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **RTC module** on the spacer provided for that purpose with some screw (following the folder "**Energy_datalogger_hole.pdf**").

        .. figure:: Image/Energy/Hardware/box_RTC.JPG
	   :width: 934px
	   :align: center
	   :height: 672px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **voltage divider board** on the spacer provided for that purpose with some screw (follwing the folder "**Energy_datalogger_hole.pdf**").

        .. figure:: Image/Energy/Hardware/box_Divider.JPG
	   :width: 834px
	   :align: center
	   :height: 671px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the **electronic board** of the sensor and connect its wire (provided with the sensor) on the board.

        .. figure:: Image/Energy/Hardware/sensor_board.JPG
	   :width: 748px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the Grove/picot wire on the **MKR CARRIER**. On that board, the output associate to the current sensor connexion are on the  **A0, A1, A2 and A3** port (Here, we only use the **A0, A1 and A2** port for 3 sensor connexion).

        .. figure:: Image/Energy/Hardware/wire_on_A0port.JPG
	   :width: 800px
	   :align: center
	   :height: 670px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the black wire of this Grove/picot wire, and connect it on the triple connector of the wire coming from the sensor board (black on black). 

        .. figure:: Image/Energy/Hardware/black_wire.JPG
	   :width: 768px
	   :align: center
	   :height: 609px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the yellow wire of this Grove/picot wire, and connect it on the triple connector of the wire coming from the sensor board ( yellow on blue).  

        .. figure:: Image/Energy/Hardware/yellow_wire.JPG
	   :width: 824px
	   :align: center
	   :height: 671px
	   :alt: alternate text
	   :figclass: align-center

#.	Take an independant red wire and connect one side on the triple connector of the wire coming from the sensor (red on red). 

        .. figure:: Image/Energy/Hardware/red_wire.JPG
	   :width: 856px
	   :align: center
	   :height: 664px
	   :alt: alternate text
	   :figclass: align-center

.. sidebar::  If you want to connect more sensor, please follow the step 5 to 10 for the **A0, A1, A2 and A3** port.

        :subtitle: Be carefull, the sensor on the **A0** must be fixed on the **SW4 relay**, the sensor on the **A1** port on the **SW3 relay**...

11.	With scotch, stick together all the wire connecter to the triple wire coming from the sensor board.
        
        .. figure:: Image/Energy/Hardware/wire_scotch.JPG
	   :width: 818px
	   :align: center
	   :height: 624px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the red wire coming from the triple connector to the left side of the **SW4 relay**.
        
        .. figure:: Image/Energy/Hardware/wire_on_relay.JPG
	   :width: 815px
	   :align: center
	   :height: 662px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the red wire coming from the **A0 port** on the middle part of the **SW4 relay**. Do step 16 and 17  for each connected sensor on that datalogger (for example, here, the SW4, SW3 et SW2 relay will be use).
        
        .. figure:: Image/Energy/Hardware/wire_on_relay2.JPG
	   :width: 869px
	   :align: center
	   :height: 674px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the **Grove screw terminal** on the spacer provided for that purpose with some screw (following the folder "**Energy_datalogger_hole.pdf**").

        .. figure:: Image/Energy/Hardware/box_grove.JPG
	   :width: 824px
	   :align: center
	   :height: 670px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the **Grove screw terminal** on the **MKR Carrier**.

        .. figure:: Image/Energy/Hardware/grove_connexion.JPG
	   :width: 843px
	   :align: center
	   :height: 662px
	   :alt: alternate text
	   :figclass: align-center

#.	Cut 12cm of **red wire diameter 3 mm**. Use a **wire stripper** to strip 1 cm on both side of the wire.

        .. figure:: Image/Flowmeter/Hardware/red_wire_stripped.JPG
	   :width: 816px
	   :align: center
	   :height: 778px
	   :alt: alternate text
	   :figclass: align-center

#.	Cut 12cm of **black wire diameter 3 mm**. Use a **wire stripper** to strip 1 cm on both side of the wire.

        .. figure:: Image/Flowmeter/Hardware/Black_wire_stripped.JPG
	   :width: 867px
	   :align: center
	   :height: 827px
	   :alt: alternate text
	   :figclass: align-center

#.	Take a **crimp lug**. Use a **crimping tool** to fix the **crimp lug** on one side  of the **red wire diameter 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/red_wire_pod.JPG
	   :width: 792px
	   :align: center
	   :height: 682px
	   :alt: alternate text
	   :figclass: align-center

#.	Take a **crimp lug**. Use a **crimping tool** to fix the **crimp lug** on one side of the **black wire diameter 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/black_wire_pod.JPG
	   :width: 740px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Take one of the **red wire diameter 3 mm**, connect one side of the wire on the **GRD** port of the **voltage divider board**. Screw it.

        .. figure:: Image/Energy/Hardware/divider_wire.JPG
	   :width: 892px
	   :align: center
	   :height: 667px
	   :alt: alternate text
	   :figclass: align-center

#.	Take one of the "red wire diameter 3 mm", connect one side of the wire on the **VOL** port of the **voltage divider board**. Screw it.

        .. figure:: Image/Energy/Hardware/divider_wire2.JPG
	   :width: 831px
	   :align: center
	   :height: 668px
	   :alt: alternate text
	   :figclass: align-center

#.	Drill a M18 hole for each sensor on one side of the box.

        .. figure:: Image/Energy/Hardware/box_cable_gland.JPG
	   :width: 894px
	   :align: center
	   :height: 523px
	   :alt: alternate text
	   :figclass: align-center

#.	Put and fix the **PVC plate** inside the box. 

        .. figure:: Image/Energy/Hardware/box_PVC.JPG
	   :width: 899px
	   :align: center
	   :height: 673px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix the cable gland on the hole that were drill before.

        .. figure:: Image/Energy/Hardware/cable_gland.JPG
	   :width: 900px
	   :align: center
	   :height: 615px
	   :alt: alternate text
	   :figclass: align-center

#.	Take the **current sensor** (SEN0287, SEN0288 or SEN0211) and plug there jack wire inside the cable gland. 

        .. figure:: Image/Energy/Hardware/current_sensor_cable_gland.JPG
	   :width: 900px
	   :align: center
	   :height: 631px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect the jack wire of the sensor on their electronic board.

        .. figure:: Image/Energy/Hardware/current_sensor_connected.JPG
	   :width: 900px
	   :align: center
	   :height: 644px
	   :alt: alternate text
	   :figclass: align-center

#.	Fix their electronic board on a PVC plate using collar.

        .. figure:: Image/Energy/Hardware/current_sensor_fixation.JPG
	   :width: 900px
	   :align: center
	   :height: 639px
	   :alt: alternate text
	   :figclass: align-center

#.	Put the **push button** inside its hole (hole diameter is 14 mm).

        .. figure:: Image/Energy/Hardware/push_button_box.JPG
	   :width: 900px
	   :align: center
	   :height: 601px
	   :alt: alternate text
	   :figclass: align-center

#.	fix the **push button** inside his hole using a nut.

        .. figure:: Image/Energy/Hardware/push_button_fix.JPG
	   :width: 767px
	   :align: center
	   :height: 668px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect both wire from the **push button** on the **Grove screw terminal** (one of the wire must be connect on the **VCC** port, and the other on the **D2** port), then, screw it.
        
        .. figure:: Image/Energy/Hardware/push_button_connection.JPG
	   :width: 787px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Take one **red banana plug**. Insert it inside there hole (hole diameter 12 mm), then fix it (a nuts is provided with the banana plug).

	.. figure:: Image/Energy/Hardware/banana_plug1.JPG
	   :width: 839px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Take one **black banana plug**. Insert it inside there hole ((hole diameter 12 mm), then fix it (a nuts is provided with the banana plug).

        .. figure:: Image/Energy/Hardware/banana_plug2.JPG
	   :width: 824px
	   :align: center
	   :height: 663px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect one side of the second **red wire diameter 3 mm** on the **VIN** port of the **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/CARRIER_VIN.JPG
	   :width: 850px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect one side of the second **black wire diameter 3 mm** on the **GND** port of the **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/CARRIER_GND.JPG
	   :width: 847px
	   :align: center
	   :height: 669px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect on the **red banana plug** both **red wire diameter 3 mm** with the **crimp lug** coming from the **voltage divider** and the **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/banana_connect_red.JPG
	   :width: 846px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Connect on the **black banana plug** both **black wire diameter 3 mm** with the **crimp lug** coming from the **voltage divider** and the **MKR CARRIER**.

        .. figure:: Image/Energy/Hardware/banana_connect_black.JPG
	   :width: 848px
	   :align: center
	   :height: 666px
	   :alt: alternate text
	   :figclass: align-center

#.	Close the box. The energy datalogger is ready to be use. **Be careful, the datalogger need an 12V DC power supply, use an adapted power supply**.

        .. figure:: Image/Energy/Hardware/final.JPG
	   :width: 900px
	   :align: center
	   :height: 595px
	   :alt: alternate text
	   :figclass: align-center
