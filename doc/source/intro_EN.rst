# SETIER : Suivi et Evaluation des capTeurs low-cost pour les Installations de traitement des Eaux usées : qualité des Eaux et énErgie


En partenariat avec l'Agence de l'eau Rhône-Méditérranée-Corse, l’INRAE et l'IRD ont lancé le projet SETIER en 2019. Coordonnées par INRAE ( Stéphanie Prost-Boucle et Rémi Clément), le projet Setier, s'appuie sur le développement mondial de l'électronique open source et des capteurs bon marché, miniaturisés et portatifs. Cette tendance rend les mesures autour de la qualité de l’eau (conductivité, pH, turbidité, DCO, température, redox, O2 dissous, énergie) de plus en plus accessibles. Leur développement ouvre de nouvelles perspectives en matière de suivi et de pilotage des installations de traitement et valorisation des eaux résiduaires grâce à une caractérisation spatio-temporelle plus détaillée (mètre, minute). Ce type d’approche, dite « SMART Project » se développe considérablement dans d’autres domaines d’applications environnementales, par exemple en pollution de l’air, en hydrologie. Le gros avantage de ces outils open source est qu’ils offrent une flexibilité très forte (choix des mesures, nombre de capteurs, fréquence de mesure), mais également s'incluent dans une démarche plus respectueuse de l'environnement avec la possibilité de mieux connaître les outils utilisés et de les réparer en ne remplaçant que les composants défectueux, ou en réutilisant certains composants dans d'autres applications.

Sur le Gitlab, vous trouverez trois types de centrales de mesure qui ont été développées dans nos laboratoires, avec une philosophie dite "lego". Cette philosophie s'appuie sur:

- des capteurs qui ont été évalués en laboratoire pour s'assurer de leur performance et qui sont en adéquation avec les besoins identifiés pour les eaux usées;
- une stratégie d'assemblage simple sans soudure, sur la base de modules commerciaux disponibles en France chez les revendeurs spécialistes de l’électronique;
- sur une notice bien documentée répertoriée dans les dossiers ici.


**La centrale n°1**, est la centrale à mesure de débit ou de hauteur d'eau, elle inclut un capteur ultrason de précision avec une plage de mesure comprise entre 10 et 150 cm.

**La centrale n°2** est la centrale physico-chimique, elle inclut, une sonde de température, une sonde de conductivité électrique, une sonde redox, une sonde pH, et une sonde d’oxygène.

**La centrale n°3**, est la centrale d’énergie, elle inclut jusqu'à 4 pinces ampérométriques

# Documentation

Vous trouverez en ligne des documents nécessaire à l'assemblage et à l'utilisation de la centrale SETIER. 
Ces documents sont les suivants :
- flowmeter_datalogger_hole.pdf, Modèle de percage et de fixation des différents composants du datalogger débitmètre.

- physico_chemical_datalogger_hole.pdf, Modèle de percage et de fixation des différents composants du datalogger physico-chimique.

- energy_datalogger_hole.pdf, Modèle de percage et de fixation des différents composants du datalogger énergie.

- flowmeter_datalogger_connexion.pdf, Modèle de connexion des différents composants du datalogger débitmètre.

- physico_chemical_datalogger_connexion.pdf, Modèle de connexion des différents composants du datalogger physico-chimique.

- energy_datalogger_connexion.pdf, Modèle de connexion des différents composants du datalogger énergie.

- logigramme_soft_setier.pdf, logigramme décisionnel d'aide à l'utilisation du software arduino sur les différents datalogger.

- setier-fichier-calibration.xlsm, fichier excel avec Macro, à utiliser avec le datalogger physico-chimique. Ce fichier permet de procéder à la calibration des 4 capteurs physico-chimiques (redox, O2, pH et conductivité), en utilisant en parallèle le wifi du datalogger. 

- setier_datalogger_code, dossier dans lequel se trouve l'ensemble du code permettant le paramètrage et l'utilisation des différents datalogger. Ce dossier est à télécharger intégralement.

[Documentation en PDF](https://reversaal.gitlab.irstea.page/setier_datalogger/output.pdf)
[Documentation en PDF](https://reversaal.gitlab.irstea.page/setier_datalogger/output.pdf)

# Références


Articles
========
Low-cost sensors and datalogger open hardware for wastewaters: Setier project, S. Prost-Boucle, H. Kamgang Nzeufo, T. Bardo, S. Moreau, H. Guyard, C. Duwig, B. Kim, V. Dubois, S. Gillot, R. Clément

Communications
==============

Prost-Boucle S., Sudre J., Imig A-F., Clément R. (2022) Le projet SETIER : capteurs bon marché pour l’analyse des eaux usées - Des outils complémentaires à la gestion des stations. Réseau des exploitants de station d’épuration - GRAIE, Aix-les-Bains, FRA

Prost-Boucle S., Clément R. (2022) Le projet SETIER : capteurs bon marché pour l’analyse des eaux usées - Des outils complémentaires à la gestion des stations. Inovations digitales - RESEED, Villeurbanne, FRA

Prost-Boucle S., Clément R. (2021) Intérêt des capteurs bon marché pour l’analyse des eaux usées : un outil complémentaire à la gestion des stations ? 100ème congrès de l’ASTEE, Paris, FRA



