#include <multi_channel_relay.h>
#include <OneWire.h>      // librairie pour temp DS18B20

#define Temp_PIN 0 
 
Multi_Channel_Relay relay;
OneWire  Temp_Sensor(Temp_PIN);


void Init_Relay (void)
{
  // Set I2C address and start relay
  relay.begin(0x11); 
}

void Get_Temp(float *Temp)
 {
    byte data[9]; // data[] : Données lues depuis le scratchpad
    byte addr[8]; // addr[] : Adresse du module 1-Wire détecté
      
    /* Reset le bus 1-Wire si nécessaire (requis pour la lecture du premier capteur) */
    Temp_Sensor.reset_search();
       
    /* Recherche le prochain capteur 1-Wire disponible */
    if (!Temp_Sensor.search(addr))
    {     
      Serial.println(F("NO_Temp_SENSOR_FOUND")); // Pas de capteur
      *Temp = 999;
    }
  
    /* Vérifie que l'adresse a été correctement reçue */
    if (OneWire::crc8(addr, 7) != addr[7])
    {     
      Serial.println(F("INVALID_ADDRESS for Temp_Sensor")); // Adresse invalide
      *Temp = 999;
    }
  
    /* Vérifie qu'il s'agit bien d'un DS18B20 */
    if (addr[0] != 0x28)
    {     
      Serial.println(F("INVALID_ADDRESS for Temp_Sensor")); // Mauvais type de capteur
      *Temp = 999;
    }
  
    /* Reset le bus 1-Wire et sélectionne le capteur */
    if (addr[0] == 0x28)
    {
      Temp_Sensor.reset();
      Temp_Sensor.select(addr);
  
      /* Lance une prise de mesure de température et attend la fin de la mesure */
      Temp_Sensor.write(0x44, 1);
      delay(800);
  
      /* Reset le bus 1-Wire, sélectionne le capteur et envoie une demande de lecture du scratchpad */
      Temp_Sensor.reset();
      Temp_Sensor.select(addr);
      Temp_Sensor.write(0xBE);
 
     /* Lecture du scratchpad */
      for (byte i = 0; i < 9; i++) 
          data[i] = Temp_Sensor.read();
         
      /* Calcul de la température en degré Celsius */
      *Temp = (int16_t) ((data[1] << 8) | data[0]) * 0.0625; 
    }  
 }

void Get_VPhy(float *VofPhysensor, byte A_number, byte ON_tempo)
{
    long V_Value=0;
    long  sum=0;
        
    if (A_number==0)
    {
      relay.turn_on_channel(4);     
      for (byte i=0;i<ON_tempo;i++)
        delay(1000);
      analogReadResolution(12);  
      for(int i=0;i<20;i++)
      {
          V_Value=analogRead(A0);
          sum=V_Value+sum;
          delay(500);
      }
      relay.turn_off_channel(4);     
    }
    if (A_number==1)
    {
      relay.turn_on_channel(3);
      for (byte i=0;i<ON_tempo;i++)
        delay(1000);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          V_Value=analogRead(A1);
          sum=V_Value+sum;
          delay(500);
      }
      relay.turn_off_channel(3);
    }
    if (A_number==2)
    {
      relay.turn_on_channel(2);
      for (byte i=0;i<ON_tempo;i++)
        delay(1000);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          V_Value=analogRead(A2);
          sum=V_Value+sum;
          delay(500);
      }
      relay.turn_off_channel(2);
    }
    if (A_number==3)
    {
      relay.turn_on_channel(1);
      for (byte i=0;i<ON_tempo;i++)
        delay(1000);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          V_Value=analogRead(A3);
          sum=V_Value+sum;
          delay(500);
      }
      relay.turn_off_channel(1);
    }
    
    sum=sum/20;
    
    *VofPhysensor= sum* 3300; 
    *VofPhysensor = *VofPhysensor/4095; // resolution 12 bits (4095 = 2puissance12 - 1)
    *VofPhysensor = *VofPhysensor * 1.52; // prise en compte du pont diviseur en entree analogique de carte connecteur
        
    return;  
}

void Write_Data_SD_Phy(float*Vofbatt, float *P1_Temp,float*P1, float*V_P1, float *P2_Temp,float*P2, float*V_P2, float *P3_Temp,float*P3, float*V_P3, float *P4_Temp,float*P4, float*V_P4)
{
    DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307
    SD_File=SD.open("Data_PC.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       SD_File=SD.open("Data_PC.txt", FILE_WRITE); 
       SD_File.print(F("Nom du datalogger : ")); 
       SD_File.print(F ("ID_NAME "));
       SD_File.println(STATION_NAME);   
       SD_File.println(F("Mesure PhysicoChimique"));
       SD_File.println(F("Version programme v0 / Helene Guyard / IGE-IRD /partie Wifi / Valerie Quatela / IGE-CNRS"));
       SD_File.println(F("Hardware: Licensed under CERN-OHL-S v2 or any later version"));
       SD_File.println(F("Software: Licensed under the GNU General Public License v3.0"));
       SD_File.println(F("Projet Setier / Mesures:")); 
       SD_File.println(F("Date et Heure; Vbatt (V) ; Capteur_PH [Temp(°C); PH; Tension_Sonde (mV)]; Capteur_Redox [Temp(°C); Redox (mV); Tension_Sonde (mV)]; Capteur_O2 [Temp(°C); O2; Tension_Sonde (mV)]); Capteur_Conductivimetre [Temp(°C); Conducti (µs/cm ; Tension_Sonde (mV)]; Cadence;  "));
       SD_File.println(F("START_DATA"));              
    }
    SD_File.close();     
    SD_File=SD.open("Data_PC.txt", FILE_WRITE);
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.print(':');
    if (DS1307_Time.second()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.second(), DEC);
    SD_File.print(" ; ");
    SD_File.print(*Vofbatt);
    SD_File.print(" ; ");
    SD_File.print(*P1_Temp);
    SD_File.print(" ; ");
    SD_File.print(*P1);
    SD_File.print(" ; ");   
    SD_File.print(*V_P1);
    SD_File.print(" ; ");
    SD_File.print(*P2_Temp);
    SD_File.print(" ; ");
    SD_File.print(*P2);
    SD_File.print(" ; ");   
    SD_File.print(*V_P2);
    SD_File.print(" ; ");
    SD_File.print(*P3_Temp);
    SD_File.print(" ; ");
    SD_File.print(*P3);
    SD_File.print(" ; ");   
    SD_File.print(*V_P3);
    SD_File.print(" ; ");
    SD_File.print(*P4_Temp);
    SD_File.print(" ; ");
    SD_File.print(*P4);
    SD_File.print(" ; ");   
    SD_File.print(*V_P4);
    SD_File.print(" ; ");
    SD_File.print(Ma_Cadence);
    SD_File.print(" ; ");
    SD_File.println();
    SD_File.close();
}

void Affiche_Data_Phy (float*Vofbatt, float *P1_Temp,float*P1, float*V_P1, float *P2_Temp,float*P2, float*V_P2, float *P3_Temp,float*P3, float*V_P3, float *P4_Temp,float*P4, float*V_P4)
 {
      
   Serial.print(F("Nom du datalogger : ")); 
   Serial.println(STATION_NAME);
   DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307    
   Serial.print(DS1307_Time.year(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.month(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.day(), DEC);
   Serial.print("  ");
   Serial.print(DS1307_Time.hour(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.minute(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.second(), DEC);
   Serial.print(" ; ");
   Serial.print(F(" Tension Vbatt : "));
   Serial.print ( *Vofbatt);
   Serial.print (F( " V "));
   Serial.print(F(" Temp_PH "));
   Serial.print(*P1_Temp);               //Print PH
   Serial.print(F(" °C "));
   Serial.print(F(" PH "));
   Serial.print(*P1);
   //Serial.print(F(" A "));
   Serial.print(F(" Tension PH "));
   Serial.print(*V_P1);
   Serial.print(F(" mV "));
   Serial.print(F(" Temp_Redox "));
   Serial.print(*P2_Temp);               //Print Redox
   Serial.print(F(" °C "));
   Serial.print(F(" Redox "));
   Serial.print(*P2);
   Serial.print(F(" mV "));
   Serial.print(F(" Tension Redox "));
   Serial.print(*V_P2);
   Serial.print(F(" mV "));
   Serial.print(F(" Temp_O2 "));
   Serial.print(*P3_Temp);               //Print O2
   Serial.print(F(" °C "));
   Serial.print(F(" O2 "));
   Serial.print(*P3);
   //Serial.print(F(" A "));
   Serial.print(F(" Tension O2 "));
   Serial.print(*V_P3);
   Serial.print(F(" mV "));
    Serial.print(F(" Temp_Conducti "));
   Serial.print(*P4_Temp);               //Print conducti
   Serial.print(F(" °C "));
   Serial.print(F(" Conductivité "));
   Serial.print(*P4);
   Serial.print(F(" µs/cm "));
   Serial.print(F(" Tension Conducti "));
   Serial.print(*V_P4);
   Serial.print(F(" mV "));   
   Serial.print(F("Frequence de mesure: "));
   Serial.print(Ma_Cadence);
   Serial.println();      
  }

 void Write_Coeff_SD(float*Coeff0_A, float*Coeff0_B,float*Coeff1_A, float*Coeff1_B,float*Coeff2_A, float*Coeff2_B,float*Coeff3_A, float*Coeff3_B)
 {
   DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307     
    SD_File=SD.open("Etalon.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       SD_File=SD.open("Etalon.txt", FILE_WRITE);                
       SD_File.println(F("Fichier des coefficients d'etalonnage des sondes"));
       SD_File.println(F("Date et Heure; Capteur_PH [CoeffA ; CoeffB]; Capteur_Redox [CoeffA ; CoeffB]; Capteur_O2 [CoeffA ; CoeffB]; Capteur_Conductivimetre [CoeffA ; CoeffB]; "));
       SD_File.println(F("START_DATA"));           
    } 
    SD_File.close();
    SD_File=SD.open("Etalon.txt", FILE_WRITE);   
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.print(" ; ");
    SD_File.print(*Coeff0_A);
    SD_File.print(" ; ");
    SD_File.print(*Coeff0_B);
    SD_File.print(" ; ");
    SD_File.print(*Coeff1_A);
    SD_File.print(" ; ");
    SD_File.print(*Coeff1_B);
    SD_File.print(" ; ");
    SD_File.print(*Coeff2_A);
    SD_File.print(" ; ");
    SD_File.print(*Coeff2_B);
    SD_File.print(" ; ");
    SD_File.print(*Coeff3_A);
    SD_File.print(" ; ");
    SD_File.print(*Coeff3_B);
    SD_File.print(" ; ");
    SD_File.println();
    SD_File.close();
 }
