void Awake_Wifi()
{
  int NbrFic;                         // Nombre de fichiers sur carte SD (pour Wifi)
  float EspTotal;                     // Capacité carte SD (pour Wifi)
  float EspLibre;                     // Capacité libre carte SD (pour Wifi)
  float EspOcc;                       // Capacité occupé carte SD (pour Wifi)
  String Cwifi;                       // date et heure derniere connexion wifi
  digitalWrite(STATE_LED_Pin, HIGH);
  Zero_RTC.detachInterrupt(); 
  WIFI_RTC_STATE = true ; 
  Cwifi= Get_Cwifi(); // recuperation de la date de derniere connexion wifi
   // Mise à jour des données de la carte µSD
  EspTotal= etatSD();   // capacité CARTE SD en Go
  EspOcc = espOccSD();  // espace occupé CARTE SD en Go
  NbrFic= inspectSD(); //nombre de fichiers dans SD 
  String NomFicSD[NbrFic]; //NbrFIcle nombre de fichier dans la sd est obtenu à la config du wifi
  float SizeFicSD[NbrFic];
  File entry;            
  SD_File=SD.open("/");
  byte i =0;
  // variable pour datalogger physico chimique
  #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE)
    boolean PH_Aff = false;
    boolean PH_Mes = false;
    boolean Redox_Aff = false;
    boolean Redox_Mes = false;
    boolean O2_Aff = false;
    boolean O2_Mes = false;
    boolean EC_Aff = false;
    boolean EC_Mes = false;
    boolean Modif_Coeff = false;
    String Coeff;
  #endif
  
  while (true) // creation du tableau des noms de fichier de la carte SD et de la taille
  {
      File entry = SD_File.openNextFile();
      if (! entry) { // si tous les noms de fichier ont été lu sortir de la boucle while
          break;
        }
      if ( ! entry.isDirectory()) {// si ce n'est pas un répertoire
         NomFicSD[i]= entry.name();
         SizeFicSD[i]=(float)entry.size()/1024;
         i++;
         
       }
      entry.close();                  // fermeture du fichier
  }
  SD_File.close();  
  //*** FIN creation du tableau des noms de fichier de la carte SD et de la taille         
  Config_WIFI();  
  Alarm_Zero_RTC_Wifi();
  WIFI_STATE = true;
  
  while(WIFI_STATE && WIFI_RTC_STATE) 
  {
    // comparer status précédent avec status courant
    if (status != WiFi.status())
    {   // si different, mettre a jour status
        status = WiFi.status();
        if (status == WL_AP_CONNECTED)
        { // connexion d'un périphérique
           // Serial.println("Périphérique connecté au point d'accès");
        } 
        else
        { // péripherique déconnecté du point d'accès, retour en mode écoute
           //Serial.println("Périphérique déconnecté du point d'accès");
        }
    } 
 
    WiFiClient client = server.available();   // écoute si nouveaux clients

    if (client) 
    {                             // si client,
      //Serial.println("nouveau client");       // affiche un message sur port série    
      String currentLine = "";                // création d'une String de réception des data du client
      String header;                         // Variable pour sauvegarde requette HTTP
      while (client.connected()) 
      {            // boucle while "tant que le client est connecté"
        if (client.available())
        {             // si le client envoie des byte,
          char c = client.read();             // lire le byte, et 
          //Serial.write(c);                  // l'afficher sur le port série si besoin pour debug
          header += c;                        // construction du header = sauvegarde requête http du client si besoin ultérieur de traitement
   
         
        if (c == '\n') // // si le byte est un caractère de nouvelle ligne
        {                    
          if (currentLine.length() == 0)
          {  // si la ligne actuelle est vide, on a alors deux caractères de nouvelle ligne d'affilée: 
            // c'est la fin de la requête HTTP du client, alors envoyer une réponse:                    
            client.println("HTTP/1.1 200 OK"); // le header HTTP commence toujours par un code de réponse (HTTP/1.1 200 OK)
            client.println("Content-type:text/html"); // et un "Content-type" pour préciser au client le type de donnée qu'il va recevoir 
            client.println(); // et enfin une ligne vide

            // ********** Fabrication de la page web envoyé au client *****************

            // ****** Entête page Web ******            
            client.print("<body style='background-color: #66bac7';><h1> DATALOG : ");
            client.print (STATION_NAME);
            client.println ("</h1><hr>");
            client.print("<hr>");         // ligne de séparation horizontale
            
            // ****** Module état datalogger ******
            client.print("<h2> ETAT DATALOGGER </h2>");                        
            client.print("Date et heure Station : ");
            Get_RTC();
            for (byte i = 0; i < 6; i++) //ecriture de date heure
            {
              client.print(RTC_Tab[i]);
              client.print(RTC_Txt[i]);
            }
            client.print("<br>");  
                       
            client.print("Niveau Batterie : ");
            Get_Vbatt (&Vbatt); // recuperation de la tension batterie
            client.print(Vbatt);
            client.print(" Volt <br>");
            
            if (Ma_Cadence == 24)
              client.print("Une mesure par jour <br>");
            else
            {
              client.print("Une mesure toutes les ");
              client.print(Ma_Cadence);
              client.print(" min <br>");
            }
            
            #if defined(TYPE_DATALOGGER_HAUTEUR_EAU)
            
              client.print("Distance : ");
              Get_Ht_Eau(&Usound_Dist,&Usound_Temp,&Usound_Noise);// recuperation des données du capteur
              client.print(Usound_Dist);
              client.print(" mm <br>");
  
              client.print("Temperature : ");            
              client.print(Usound_Temp);
              client.print(" C <br>");
  
              client.print("Bruit : ");             
              client.print(Usound_Noise);
              client.print(" <br>");

              client.print("Hauteur a debit nul: "); 
              client.print(Ht_Debit_NUL);
              client.print(" mm <br>");

              client.print("Hauteur d'eau: "); 
              client.print(Ht_Debit_NUL - Usound_Dist);
              client.print(" mm <br>");

           
            #elif defined(TYPE_DATALOGGER_ENERGIE)
              if (Capteur_Energie_A0)
              {          
                Get_VEnergie(&IEnergie_0, 0, Capteur_Energie_A0);
                client.print("Courant capteur_A0 ");            
                client.print(IEnergie_0);
                client.print(" A / ( Imax = ");
                client.print(Capteur_Energie_A0);
                client.print(" A ) <br>");               
              }
              if (Capteur_Energie_A1) 
              {
                Get_VEnergie(&IEnergie_1, 1, Capteur_Energie_A1);
                client.print("Courant capteur_A1 ");            
                client.print(IEnergie_1);
                client.print(" A / ( Imax = ");
                client.print(Capteur_Energie_A1);
                client.print(" A ) <br>");           
              }             
              if (Capteur_Energie_A2)
              {
                 Get_VEnergie(&IEnergie_2, 2, Capteur_Energie_A2);
                 client.print("Courant capteur_2 ");            
                 client.print(IEnergie_2);
                 client.print(" A / ( Imax = ");
                 client.print(Capteur_Energie_A2);                 
                 client.print(" A ) <br>");         
              }            
              if (Capteur_Energie_A3)
              {
                Get_VEnergie(&IEnergie_3, 3, Capteur_Energie_A3); 
                client.print("Courant capteur_3 ");            
                client.print(IEnergie_3);
                client.print(" A / ( Imax = ");
                client.print(Capteur_Energie_A3);
                client.print(" A ) <br>");          
              }
            
            #elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE)
            
              client.print("Distance : ");
              Get_Ht_Boue(&Ht_Boue_Dist,&Ht_Boue_Temp); // recuperation des données du capteur
              client.print(Ht_Boue_Dist);
              client.print(" cm <br>");
  
              client.print("Temperature : ");            
              client.print(Ht_Boue_Temp);
              client.print(" C <br>");              

              client.print("Hauteur cuve à vide: "); 
              client.print(Ht_a_Vide);
              client.print(" cm <br>");

              client.print("Hauteur de boue: "); 
              client.print(Ht_a_Vide - Ht_Boue_Dist);
              client.print(" cm <br>");

            #endif

            client.print("Derniere connexion Wifi : ");            
            client.print(Cwifi);            
            client.print("<br><br>");

            // bouton Actualiser pour rafraichir la page
            client.print("<button onclick='document.location.reload(false)'> Actualiser </button>");
            client.print(" <br> ");
            client.print("<hr>");         // ligne de séparation horizontale
            client.print("<hr>");         // ligne de séparation horizontale
            //******* Module etalonnage pour datalogger physico chimique *****
          #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) //Si c'est le datalogger physico chimique qui est utilisé      

            client.print("<h2> MESURE / ETALONNAGE </h2>");
           
            client.println("<ul>");
            if (Capteur_PH_A0) // si on a une sonde PH
            {
              client.print("<label> Affichage PH : </label>");
              client.print("<a href=\"/A\">OUI</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/B\">NON</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }
            if (PH_Aff) // si on demande l'affichage de la mesure PH
              {
                client.print("<a href=\"/I\">Lancer une mesure PH</a>");   // bouton  ON
                client.print(" <br> ");
                if (PH_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  Redox_Aff = false;
                  O2_Aff = false;
                  EC_Aff = false;
                  client.print("Mesure PH en cours");
                  client.print(" <br> ");
                  Get_Temp(&PH_Temp); // mesure temperature
                  Sum_Temp = PH_Temp;
                  Get_VPhy(&V_PH_A0, 0, 60); // mesure PH entrée A0, temps de mise sous tension 60s
                  Get_Temp(&PH_Temp); // mesure temperature
                  Sum_Temp = PH_Temp + Sum_Temp ; 
                  PH_Temp= Sum_Temp / 2 ;
                  PH_Mes=false;
                } 
                client.print("PH : Temp ");            
                client.print(PH_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");                
                if (C_PH_A == 0.00) // Pas de calcul besoin d'un etalonnage
                  client.print("<br> Cette sonde doit etre etalonnee <br>");                    
                else
                {
                  PH_A0= V_PH_A0-C_PH_B; // calcul du PH 
                  PH_A0= PH_A0 / C_PH_A; // calcul du PH
                  client.print("Valeur PH ");
                  client.print(PH_A0);
                  client.print(" <br> ");
                } 
                client.print("( PH : Tension ");
                client.print(V_PH_A0);
                client.print(" mV )"); 
                client.print(" <br> ");
                client.print(" <br> ");
                  
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("PH Coeff_A =  ");
                client.print(C_PH_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"phCoeff_A\" id=\"phCoeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("PH Coeff_B =  ");
                client.print(C_PH_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"phCoeff_B\" id=\"phCoeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // ligne de séparation horizontale  
            
            if (Capteur_Redox_A1) // si on a une sonde Redox
            {
              client.print("<label> Affichage Redox : </label>");
              client.print("<a href=\"/Z\">OUI</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/D\">NON</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }
             if (Redox_Aff) //Si on demande l'affichage de la sonde redox
              {
                client.print("<a href=\"/J\">Lancer une mesure Redox</a>");   // bouton  ON
                client.print(" <br> ");
                if (Redox_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  PH_Aff = false;
                  O2_Aff = false;
                  EC_Aff = false;
                  client.print("Mesure Redox en cours");
                  client.print(" <br> ");
                  Get_Temp(&Redox_Temp); // mesure temperature
                  Sum_Temp = Redox_Temp;
                  Get_VPhy(&V_Redox_A1, 1, 60); //mesure Redox entrée A1, temps de mise sous tension 60s
                  Get_Temp(&Redox_Temp); // mesure temperature
                  Sum_Temp = Redox_Temp + Sum_Temp ;
                  Redox_Temp= Sum_Temp / 2 ;
                  Redox_Mes=false;
                }
                client.print("Redox : Temp  ");            
                client.print(Redox_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");                
                //calcul Redox
                if (Redox_A == 0) // Pas de calcul besoin d'un etalonnage
                  client.print("<br> Cette sonde doit etre etalonnee <br>"); 
                else
                {
                  Redox_A1= V_Redox_A1-Redox_B; // calcul du Redox
                  Redox_A1= Redox_A1 / Redox_A; // calcul du Redox
                  client.print(" Valeur Redox ");
                  client.print(Redox_A1);
                  client.print(" mV  <br> ");
                }
                client.print("( Redox Tension ");
                client.print(V_Redox_A1);
                client.print(" mV ) "); 
                client.print(" <br> ");
                client.print(" <br> ");
                           
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Redox Coeff_A =  ");
                client.print(Redox_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Redox_Coeff_A\" id=\"Redox_Coeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Redox Coeff_B =  ");
                client.print(Redox_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Redox_Coeff_B\" id=\"Redox_Coeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // ligne de séparation horizontale
            if (Capteur_O2_A2) // si on a une sonde Redox
            {
              client.print("<label> Affichage O2 : </label>");
              client.print("<a href=\"/L\">OUI</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/M\">NON</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }
            if (O2_Aff) //Si on demande l'affichage de la sonde redox
              {
                client.print("<a href=\"/N\">Lancer une mesure O2</a>");   // bouton  ON
                client.print(" <br> ");
                if (O2_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  Redox_Aff = false;
                  PH_Aff = false;
                  EC_Aff = false;
                  client.print("Mesure O2 en cours");
                  client.print(" <br> ");
                  Get_Temp(&O2_Temp); // mesure temperature
                  Sum_Temp = O2_Temp;
                  Get_VPhy(&V_O2_A2, 2, 60); //mesure Redox entrée A1, temps de mise sous tension 60s
                  Get_Temp(&O2_Temp); // mesure temperature
                  Sum_Temp = O2_Temp + Sum_Temp ;
                  O2_Temp= Sum_Temp / 2 ;
                  O2_Mes=false;
                }
                client.print("O2 : Temp  ");            
                client.print(O2_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");                 
                //calcul O2
                if (O2_A == 0.00) // Pas de calcul besoin d'un etalonnage
                  client.print("<br> Cette sonde doit etre etalonnee <br>"); 
                else
                {
                  O2_A2= V_O2_A2-O2_B; // calcul O2
                  O2_A2= O2_A2 / O2_A; // calcul O2
                  client.print("Valeur O2 ");
                  client.print(O2_A2);
                  client.print(" mg/L  <br> ");
                }
                client.print("( O2 Tension ");
                client.print(V_O2_A2);
                client.print(" mV ) "); 
                client.print(" <br> ");
                client.print(" <br> ");
                           
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("O2 Coeff_A =  ");
                client.print(O2_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"O2_Coeff_A\" id=\"O2_Coeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("O2 Coeff_B =  ");
                client.print(O2_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"O2_Coeff_B\" id=\"O2_Coeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // ligne de séparation horizontale  
            if (Capteur_Conducti_A3)// si on a une sonde Conducti
            {
              client.print("<label> Affichage Conductivite : </label>");
              client.print("<a href=\"/G\">OUI</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/H\">NON</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }                                 
                        
              if (EC_Aff) //si on demande l'affichage de la sonde Conducti
              {
                client.print("<a href=\"/K\">Lancer une mesure EC</a>");   // bouton  ON
                client.print(" <br> ");
                if (EC_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  Redox_Aff = false;
                  O2_Aff = false;
                  PH_Aff = false;
                  client.println("Mesure EC en cours");
                  client.print(" <br> ");
                  Get_Temp(&Conducti_Temp); // mesure temperature
                  Sum_Temp = Conducti_Temp;
                  Get_VPhy(&V_Conducti_A3, 3, 60); //mesure conducti entrée A3, temps de mise sous tension 60s
                  Get_Temp(&Conducti_Temp); // mesure temperature
                  Sum_Temp = Conducti_Temp + Sum_Temp ;
                  Conducti_Temp= Sum_Temp / 2 ;
                  EC_Mes=false;
                }
                client.print("Conductivite : Temp  ");            
                client.print(Conducti_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");               
               
                // calcul Conducti
                if (Conducti_A == 0) // Pas de calcul besoin d'un etalonnage
                  client.print("<br> Cette sonde doit etre etalonnee <br>"); 
                else
                {
                  Conducti_A3= V_Conducti_A3-Conducti_B; // calcul du Conducti / EC
                  Conducti_A3= Conducti_A3 / Conducti_A; // calcul du Conducti / EC
                  Sum_Temp= 0.020*(Conducti_Temp-25)+1; // calcul correction en temperature de EC
                  Conducti_A3 = Conducti_A3 / Sum_Temp ; // calcul correction en temperature de EC
                  client.print(" Valeur conductivite");
                  client.print(Conducti_A3);
                  client.print(" µS/cm  <br> ");
                } 
                client.print("( Conductivite Tension ");
                client.print(V_Conducti_A3);
                client.print(" mV ) "); 
                client.print(" <br> ");
                client.print(" <br> ");
                
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Conducti Coeff_A =  ");
                client.print(Conducti_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Conducti_Coeff_A\" id=\"Conducti_Coeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Conducti Coeff_B =  ");
                client.print(Conducti_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Conducti_Coeff_B\" id=\"Conducti_Coeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // ligne de séparation horizontale
            client.print("<hr>");         // ligne de séparation horizontale
          #endif 
            // ****** Module téléchargement ******

            client.print("<h2> TELECHARGEMENTS </h2>");
  
            // création des liens de téléchargement portant le nom des fichiers trouvés    
            for (i=0; i<NbrFic ; i++)
            {
                  client.println("<ul>");
                  client.print("<li><a href=\"");
                  client.print(NomFicSD[i]);
                  client.print("\" >");        
                  client.print(NomFicSD[i]);
                  client.print("</a>");
                  client.print(" ( ");                
                  client.print(SizeFicSD[i]);
                  client.print(" Ko ");
                  client.print(") ");
                  client.print(" <br> <br>");
               
               //*********** début formulaire pour traitemant suppression ************************** NOUVEAU **********//
               
                 client.print("<form action=\"/S\" method=\"get\">");
                 client.print("<input name=\"mdp\" id=\"motdepasse\" type=\"text\" inputmode=\"numeric\" size=\"4\" required=\"true\">");
                 client.print("<input name=\"fic\" id=\"fichier\" type=\"hidden\" value=\"");
                 client.print(NomFicSD[i]);
                 client.print("\" >");
                 client.print("<input type=\"submit\" value=\"Supprimer\"> </a><br>");
                 client.print("<label>taper code 5379 pour supprimer le fichier ci-dessus</label><br>");
                 client.print("  ");
                 client.print("</form>");    
                       
            
                  client.println("</li>");
                  client.println("</ul>");                
           }           
           client.print(" <br> ");
           client.print("<hr>");         // ligne de séparation horizontale
           client.print("<hr>");         // ligne de séparation horizontale
                
           // ****** Module état carte SD ******

            client.print("<h2> CARTE SD </h2>");

            client.print("Espace total : ");
            client.print(EspTotal);
            client.print(" (Go)<br>");
           
            client.print("Espace fichiers : ");
            client.print(EspOcc);
            client.print(" (Go) <br>");

            client.print("Espace libre : ");
            EspLibre = EspTotal - EspOcc ;      // calcul de l'espace libre sur la carte SD
            client.print(EspLibre);
            client.print(" (Go) <br>");
            
            client.print(" <br> ");
            client.print("<hr>");         // ligne de séparation horizontale                    
            client.print("<hr>");       // ligne de séparation horizontale

            // ***********************************
            
            client.println(); // La réponse HTTP se termine avec une nouvelle ligne vide
            break;            // sortir de la boucle while "tant que le client est connecté"

            // ********** La réponse HTTP est terminé , fin de la page web envoyé au client *****************
            
          }          
          else
          {      // si la nouvelle ligne est vide (mais la ligne courante n'est pas vide), 
            currentLine = ""; // alors mettre à jour "currentLine" pour la mettre à vide :
          }
        }        
        else if (c != '\r') 
        {    // si le byte est different d'une nouvelle ligne et aussi d'un caractère retour ,
          currentLine += c;      // l'ajouter à la fin de "currentLine"
        }

        // Si le client demande un telechargement
        String test="GET /";        
          for (int i=0; i < NbrFic; i++)
          {            
            test +=NomFicSD[i];          
            if (currentLine.startsWith(test))
            {
               currentLine = "";
               Serial.print(" demande de telechargement : ");
               Serial.println(NomFicSD[i]);
               client.println("HTTP/1.1 200 OK"); //send new page
               client.println("Content-Disposition: attachment; filename=");
               client.println(NomFicSD[i]);
               client.println("Connection: close");
               client.println(); 
               
               char file_buffer[16];
               int avail;
               SD_File = SD.open(NomFicSD[i], FILE_READ); // ouvrir fichier en lecture
              while (avail = SD_File.available())
              {
                int to_read = min(avail, 16);
                if (to_read != SD_File.read(file_buffer, to_read))
                {
                  break;
                }
                // uncomment the serial to debug (slow!)
                //Serial.write((char)c);
                client.write(file_buffer, to_read);
              }// fin de l envoie des données
              SD_File.close();            
            }            
            test="GET /";
         }
         //si le client demande une supression de fichier
         String test2 ="GET /S?mdp=5379&fic=";
         if (currentLine.startsWith(test2))
         {            
           boolean RemoveSDFile;
           RemoveSDFile = false;
           for (int i=0; i < NbrFic; i++)
           {
              test2 +=NomFicSD[i];     
              if (currentLine.startsWith(test2))
              {            
                 SD.remove(NomFicSD[i]);
                 test2="GET /S?mdp=5379&fic=";                 
                 for(int j=i; j<NbrFic-1; j++)
                 {
                    NomFicSD[j]=NomFicSD[j+1];
                    SizeFicSD[j]=SizeFicSD[j+1];
                 }
                 i=NbrFic;
                 RemoveSDFile=true;                 
              }
              test2="GET /S?mdp=5379&fic=";
           }
           if( RemoveSDFile)
           {
              NbrFic=NbrFic-1;
              EspTotal= etatSD();   // capacité CARTE SD en Go
              EspOcc = espOccSD();  // espace occupé CARTE SD en Go              
           }                         
          } //fin de demande de suppression de fichier 

          //debut traitement des changement de coeff d etalonnage
        #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE)
          String Coeff_etalonnage ="GET /C?phCoeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1             
              C_PH_A=Coeff.toFloat();
              flash_PH_A.write(C_PH_A);
              Modif_Coeff = true ;                            
           }
           Coeff_etalonnage ="GET /C?phCoeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1           
              C_PH_B= Coeff.toFloat();
              flash_PH_B.write(C_PH_B);
              Modif_Coeff = true ;             
           }
           Coeff_etalonnage ="GET /C?Redox_Coeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1             
              Redox_A=Coeff.toFloat();
              flash_Redox_A.write(Redox_A);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?Redox_Coeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1             
              Redox_B=Coeff.toFloat();
              flash_Redox_B.write(Redox_B);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?O2_Coeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1             
              O2_A=Coeff.toFloat();
              flash_O2_A.write(O2_A);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?O2_Coeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1             
              O2_B=Coeff.toFloat();
              flash_O2_B.write(O2_B);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?Conducti_Coeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1
              Conducti_A=Coeff.toFloat();
              flash_Conducti_A.write(Conducti_A);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?Conducti_Coeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1
              Conducti_B=Coeff.toFloat();
              flash_Conducti_B.write(Conducti_B);//ecriture en flash
              Modif_Coeff = true ;
           }

           // affichage Mesure Sonde 
           if (currentLine.startsWith("GET /A")) 
                {  PH_Aff = true;}
           if (currentLine.startsWith("GET /B"))                        
                {  PH_Aff = false;}
           if (currentLine.startsWith("GET /Z")) 
                {  Redox_Aff = true;}
           if (currentLine.startsWith("GET /D"))                        
                {  Redox_Aff = false;}
           if (currentLine.startsWith("GET /L")) 
                {  O2_Aff = true;}
           if (currentLine.startsWith("GET /M"))                        
                {  O2_Aff = false;}     
           if (currentLine.startsWith("GET /G")) 
                {  EC_Aff = true;}
           if (currentLine.startsWith("GET /H"))                        
                {  EC_Aff = false;} 
           if (currentLine.startsWith("GET /I")) 
                {  PH_Mes = true;} 
           if (currentLine.startsWith("GET /J")) 
                {  Redox_Mes = true;} 
           if (currentLine.startsWith("GET /N")) 
                {  O2_Mes = true;}      
           if (currentLine.startsWith("GET /K")) 
                {  EC_Mes = true;}    
           
        #endif
      
        }
    }
    // fin de la boucle while, le client est déconnecté, fermer la connexion:
    client.stop();
    //Serial.println("client déconnecté");
  }

  // pas de client, retour au début de While  
 } 
  WiFi.disconnect(); //Arret du module wifi
  WiFi.end();
  digitalWrite(STATE_LED_Pin, LOW);
  WIFI_STATE = false;
  Write_WIFI_SD();
  #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE)
    if(Modif_Coeff)    
      Write_Coeff_SD(&C_PH_A,&C_PH_B,&Redox_A,&Redox_B,&O2_A,&O2_B,&Conducti_A,&Conducti_B);      //ecriture des nouveaux coeff sur fichier SD 
    
  #endif
  Zero_RTC.detachInterrupt(); 
}

void Config_WIFI()
{
  // Communication WiFi
  //Serial.println(" Serveur Web / Point d'acces Wifi ");
  
  // Test module WiFi :
  if (WiFi.status() == WL_NO_MODULE)
  {
    //Serial.println(" Echec Communication module WiFi !");
    // stop
    while (true);
  }

  //Test version firmware wifi
  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION)
  {
    //Serial.println("Mettre a jour firmware ");
  }

  // Par defaut adresse IP local = 192.168.4.1, modification avec : 
  // WiFi.config(IPAddress(10, 0, 0, 1));

  // Affiche nom reseau (SSID);
 // Serial.print("Creation point d'acces : ");
 // Serial.println(ssid);

  // Creation d'un reseau ouvert, si besoin d'un reseau protégé par mot de passe WEP :
   //status = WiFi.beginAP(ssid, pass);
  status = WiFi.beginAP(ssid);
  if (status != WL_AP_LISTENING) 
  {
    //Serial.println("Echec création point d'accès");
    // stop
    while (true);
  }

  // attendre 10 seconds pour connexion:
  delay(10000);

  // Demarrer serveur web sur port 80
  server.begin();

  // connection établie, afficher status
  //printWiFiStatus();  
}

// *********** Fonction printWiFiStatus *********** 
//void printWiFiStatus() {
//  // afficher SSID du réseau :
//  Serial.print("SSID: ");
//  Serial.println(WiFi.SSID());
//
//  // afficher adresse IP du réseau point d'accès Wifi :
//  IPAddress ip = WiFi.localIP();
//  Serial.print("Adresse IP : ");
//  Serial.println(ip);
//
//  // affichage sur navigateur web:
//  Serial.print("Connection sur cette page à l'adresse http://");
//  Serial.println(ip);
//
//}



// *********** Fonction inspection carte SD ************************************************

int inspectSD(){

  //Serial.println("Inspection carte SD ... ");
  int NbrFichier=0;
  
  // lecture du nombre de fichiers présents sur la carte SD 
            // par incrément sur le dossier racine "/"
            
            File SD_File1=SD.open("/");
            while (true) {
                File entry = SD_File1.openNextFile();
                if (! entry) { // si tous les noms de fichier ont été lu sortir de la boucle while
                    break;
                  }
                if ( ! entry.isDirectory()) {// si ce n'est pas un répertoire
                   NbrFichier +=1;
                   }
                entry.close();                  // fermeture du fichier
              }
              SD_File1.close();
    
    return NbrFichier;
}

// *********** Fonction Etat carte SD ************************************************

float etatSD(){

  Sd2Card carte;                      // Carte DS
  SdVolume volume;                    //Volume carte SD
  SdFile root;
  uint32_t volumesize;
  
  //Serial.println("Module Web Carte SD ... ");
  
  if (!carte.init(SPI_HALF_SPEED, SD_Pin)) {
  //Serial.println("erreur carte SD");
  while (1);
  } else {
  //Serial.println("carte SD ok");
  }
  
  return (float)carte.cardSize()/2097152; // espace total en Gb

}

// *********** Fonction espace occupé carte SD ************************************************

float espOccSD(){

  int taille = 0;
  
  // lecture de la tailles des fichiers présents sur la carte SD 
            // par incrément sur le dossier racine "/"
            
            File SD_File=SD.open("/");
            while (true) {
                File entry = SD_File.openNextFile();
                if (! entry) { // si tous les noms de fichier ont été lu sortir de la boucle while
                    break;
                  }
                if ( ! entry.isDirectory()) {// si ce n'est pas un répertoire
                   taille += entry.size();
                                      
                 }
                entry.close();                  // fermeture du fichier
              }
              SD_File.close();
    
    return (float)taille/2097152; // espace occupé en Gb;
}
//*************************************************************

//*****************Fonction Get_Cwifi*****************************************

String Get_Cwifi()
{
  String DateHeure ;      // affichage sur page web
  char DateHeure_Tab[18]; // format = 00/00/0000 00:00  --> 16 caracteres
//  int nbrchar = 0;        // nombre caractère dans fichier
//  int nbrLigne = 0;       // nombre de ligne dans fichier
  File ficWifi;
  unsigned long Size;
  ficWifi = SD.open("LogWifi.txt"); // Ouverture fichier
  
  if (ficWifi)// ********* Exploration du fichier
  
  {    
    Size = ficWifi.size();    
    Size=Size-18;
    ficWifi.seek(Size);
    for (int i = 0; i < 18; i++) // lecture de date heure sur la derniere ligne
    {
        DateHeure_Tab[i] = ficWifi.read();
    }
    
    for (int i = 0; i < 16; i++) // creation du string dateheure
    { //ecriture de date heure dans string DateHeure
      DateHeure = DateHeure + DateHeure_Tab[i];
    }
    ficWifi.close();   // Fermeture fichier
  } 
  else // erreur si le fichier ne s'ouvre pas    
    DateHeure = " premiere connexion ";  

  return DateHeure;
}
//****************Fonction Get_RTC*******************************************************************

void Get_RTC() //construction du tableau date heure
{
   DS1307_Time = DS1307_RTC.now();
   RTC_Tab[2]=DS1307_Time.day();
   RTC_Tab[1]=DS1307_Time.month();
   RTC_Tab[0]=DS1307_Time.year();
   RTC_Tab[3]=DS1307_Time.hour();
   RTC_Tab[4]=DS1307_Time.minute();
   RTC_Tab[5]=DS1307_Time.second();

}
