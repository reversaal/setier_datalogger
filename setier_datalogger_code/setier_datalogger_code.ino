// Programmme pour Datalogger Setier 
// Programme realisé par Helene Guyard contact helene.guyard@ird.fr
// Programme partie Wifi en colaboration avec Valerie Quatela 
// Sept 2021
//
// Software: Licensed under the GNU General Public License v3.0
// Hardware: Licensed under CERN-OHL-S v2 or any later version
//
// Cartes utilisées pour le montage 
// Carte µC: MKR Wifi 1010 ref: ASX00023
// Carte µSD: MKR MEM Shield ref: ASX00008 CS_SD D4 CS_Flash D5
// Carte Fond de panier: MKR Connector Carrier ref ASX00007
// Carte RTC : Module GROVE RTC DS1307 ref 101020013 (connecteur TWI)
// Carte lecture tension batterie: Module GROVE diviseur de tension ref 104020000 (connecteur A5/A6)
//
// Toutes les informations sur: https://reversaal.gitlab.irstea.page/setier_datalogger/



//Librairies et fichier .h necessaire pour tout type de datalogger Base

#include <ArduinoLowPower.h> //Fichier contenant les fonctions pour le mode veille du SAMD21
#include <Wire.h>   //Librairie pour l'I2C
#include "RTClib.h" //Librairie pour utiliser le module Grove DS1307
#include <RTCZero.h> //Librairie pour utiliser la RTC du SAMD21
#include <SD.h> // attention avec cette librairie il faut la librairie SPI.h à inclure si n 'existe pas dans le programme principal
#include <SPI.h>
#include <WiFiNINA.h> //Librairie pour la gestion du module wifi
#include "Config_File.h" // fichier de configuration du datalogger 
#include <FlashStorage.h> // utilisation de la memoire Flash pour stocker des données sensibles

//Definition broche et variable pour datalogger de base
#define STATE_LED_Pin 3  //emplacement led d'etat datalogger

#define Vbatt_Pin A5  // emplacement module mesure tension batterie
float Vbatt;   //valeur tension batterie

FlashStorage(New_Compil, boolean); // Boolean qui permet de detecter une nouvelle compilation du systeme.

/* Creation des variables pour l'utilisation des RTC */
RTC_DS1307 DS1307_RTC; //Création de l'objet pour utiliser les fonctions associées à la RTC DS1307
DateTime DS1307_Time; // Creation de l'objet pour stocker la date et l'heure de la DS1307
RTCZero Zero_RTC; //Création de l'objet pour utiliser les fonctions associées à la RTC du SAMD21

/* Définition des pin CS des cartes SD, utilisés avec les différentes cartes */
#define SD_Pin 4  //definition de la broche CS du module SD
File SD_File; //Création de l'objet SD_File 
boolean SD_OK; // Variable pour savoir si la carte SD a bien ete initialise


// RTC modif
int RTC_Tab[6]; // Deux table pour affichage facile de l'heure
char RTC_Txt[] = {'/', '/', ' ', ':', ':', ' '};                // modif format


/* Definitions des differents parametres pour le wifi */


# define WIFI_INTERRUPT_Pin 6

volatile boolean WIFI_STATE ; // Flag pour reveil par wifi
volatile boolean WIFI_RTC_STATE; // Flag tempo duree wifi

byte Wifi_length; //temps maximum d'utilisation de la wifi

char ssid[] = STATION_NAME;         // SSID reseau (nom)
int keyIndex = 0;                   // clé réseau "Index number" (seulement en WEP)
int status = WL_IDLE_STATUS;

WiFiServer server(80);



/* Définition du data logger utilisée, en fonction du mot contenue dans le fichier config */

#if defined(TYPE_DATALOGGER_HAUTEUR_EAU) //Si c'est le datalogger pour la hauteur d'eau qui est utilisé  
  #include "Ht_Eau.h"  // rajout de l'onglet concerné pour appel des sous programmes
  //definition des variables globales utilisées
    word Usound_Dist;
    float Usound_Temp;
    byte Usound_Noise;
    uint16_t Usound_Type;
    uint16_t Usound_Version;
    
#elif defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) //Si c'est le datalogger physico chimique qui est utilisé
  #include "Physico_Chimique.h" // rajout de l'onglet concerné pour appel des sous programmes
   //definition des variables globales utilisées
    // Variables pour capteur PH connecté en A0
    FlashStorage(flash_PH_A, float); // sauvegarde du coef etalonnage A PH
    FlashStorage(flash_PH_B, float);// sauvegarde du coef etalonnage B PH
    float PH_Temp; // Temperature associée au PH 
    float PH_A0; // valeur calculées du PH
    float V_PH_A0; // valeur en volt du PH
    float C_PH_A ; // coeff d'etalonnage A pour PH
    float C_PH_B ; // coeff d'etalonnage B pour PH

    // Variables pour capteur Redox en A1
    FlashStorage(flash_Redox_A, float); // sauvegarde du coef etalonnage A Redox
    FlashStorage(flash_Redox_B, float); // sauvegarde du coef etalonnage B Redox
    float Redox_Temp; // Temperature associée au Redox 
    float Redox_A1; // valeur calculées du Redox
    float V_Redox_A1; // valeur en volt du Redox
    float Redox_A ; // coeff d'etalonnage A pour Redox
    float Redox_B ; // coeff d'etalonnage B pour Redox

    // Variables pour capteur O2 en A2
    FlashStorage(flash_O2_A, float); // sauvegarde du coef etalonnage A O2
    FlashStorage(flash_O2_B, float); // sauvegarde du coef etalonnage B O2
    float O2_Temp; // Temperature associée au O2
    float O2_A2;  // valeur calculées du O2
    float V_O2_A2;  // valeur en volt du O2
    float O2_A ; // coeff d'etalonnage A pour O2
    float O2_B ; // coeff d'etalonnage B pour O2
    
    // Variables pour capteur Conducti en A3
    FlashStorage(flash_Conducti_A, float); // sauvegarde du coef etalonnage A Conducti
    FlashStorage(flash_Conducti_B, float); // sauvegarde du coef etalonnage B Conducti
    float Conducti_Temp; // Temperature associée au conducti    
    float Conducti_A3;   // valeur calculées du conducti
    float V_Conducti_A3;  // valeur en volt du conducti
    float Conducti_A ; // coeff d'etalonnage A pour Conducti
    float Conducti_B ; // coeff d'etalonnage B pour Conducti
 
    float Sum_Temp; // moyen des mesures temperatures
    
#elif defined(TYPE_DATALOGGER_ENERGIE) // si c'est le datalogger energie qui est utilisé 
  #include "Energie.h"  // rajout de l'onglet concerné pour appel des sous programmes 
  //definition des variables courant pour chaque capteur energie
    float IEnergie_0;  
    float IEnergie_1; 
    float IEnergie_2; 
    float IEnergie_3;

#elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE) // si c'est le datalogger hauteur boue qui est utilisé 
  #include "Ht_Boue.h"  // rajout de l'onglet concerné pour appel des sous programmes 
  //definition des variables globales utilisées   
    float  Ht_Boue_Dist;
    float  Ht_Boue_Temp;
  
#endif

  

void setup()
{
  
  Serial.begin(115200); //Début de la liaison série pour moniteur serie

  pinMode(STATE_LED_Pin, OUTPUT); 
  digitalWrite(STATE_LED_Pin, HIGH); //la led d'etat est allumée
  Wifi_length=15; // le temps maxi d'utilisation de la wifi est de 15 min
    
//    while(!Serial) //Attente de l'ouverture du terminal série
//    {
//      /* On fait clignoter la LED */
//      digitalWrite(STATE_LED_Pin, !digitalRead(STATE_LED_Pin));
//      delay(100);
//    }
//    digitalWrite(STATE_LED_Pin, LOW);


 boolean LedState = true; 
 for (byte i=0;i<90;i++)  // attente de 90s pour eventuel televersement d'un nouveau programme
 {
    LedState = !LedState;
    digitalWrite(STATE_LED_Pin, LedState); //la led d'etat clignote
    delay(1000);
 }
 digitalWrite(STATE_LED_Pin, HIGH); //la led d'etat est allumee
 
//Initialisation pour tous les Datalogger
  Init_SD_Card(); //onglet base SD test de presence carte SD
  Init_DS1307_RTC(); //mise à l'heure en cas de compilation cf onglet Base_RTC
  Init_Zero_RTC (); // init de la RTC du SAMD21
  New_Compil.write(true); // boolean de compilation passe à 1

  // inscription sur le moniteur serie de la configuration du datalogger suite à la lecture de Config_File  
  Serial.println(F("Ce datalogger va etre configuré selon le fichier config joint"));
  Serial.print(F("Nom du datalogger : ")); 
  Serial.println(STATION_NAME);
  if(Ma_Cadence == 24)
  Serial.println(F("Ce datalogger effectura une mesure par jour "));
  else
  {
    Serial.print(F("Ce datalogger effectura une mesure toutes les "));
    Serial.print(Ma_Cadence);
    Serial.println(F(" minutes "));
  }
  
  #if defined(TYPE_DATALOGGER_HAUTEUR_EAU)  //Code compile si le datalogger est un hauteur d'eau
  
    Serial.println(F("Ce datalogger est de type hauteur d'eau "));
    Serial.print(F("La distance mesurée en millimetre à debit nul est de :  "));
    Serial.println(Ht_Debit_NUL);
    Serial.print(F("La profondeur du canal en millimetre est de :  "));
    Serial.println(Profondeur_Canal);
    
    Init_Ht_Eau(&Usound_Type,&Usound_Version); //Initialisation du capteur (onglet Ht_Eau.h
    Init_SD_Ht_Eau(&Usound_Type,&Usound_Version);
  
    
 #elif defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) //Code compile si le datalogger est un physico-chimique
  
    Serial.println(F("Ce datalogger est de type physicochimique "));
    if (Ma_Cadence==1)
    {
      Serial.println(F("ATTENTION la cadence de mesure minimum doit etre de 5 min jusqu'a 3 sondes 15 min à partir de 4 sondes"));      
    }  
    if (Ma_Cadence==5)
    {
      Serial.println(F("ATTENTION à partir de 4 sondes la cadence minimum est de 15min "));      
    }  
    if (Capteur_PH_A0) 
    {   
      Serial.println(F("Le capteur PH doit etre connecté sur A0")); 
      C_PH_A = flash_PH_A.read();
      C_PH_B= flash_PH_B.read();
      if ( C_PH_A == 0.00)
        Serial.println(F("Attention coeff d'etalonnage A = 0 il faut etalonner la sonde"));
      if ( C_PH_B == 0.00)
        Serial.println(F("Attention coeff d'etalonnage B = 0 il faut etalonner la sonde"));
    }  
    else 
      Serial.println(F("Il n'y a pas de capteur PH "));
      
    if (Capteur_Redox_A1) 
    {   
      Serial.println(F("Le capteur Redox doit etre connecté sur A1"));
      Redox_A = flash_Redox_A.read();
      Redox_B= flash_Redox_B.read();
      if ( Redox_A == 0.00)
        Serial.println(F("Attention coeff d'etalonnage A = 0 il faut etalonner la sonde"));
      if ( Redox_B == 0.00)
        Serial.println(F("Attention coeff d'etalonnage B = 0 il faut etalonner la sonde"));
    }      
    else 
      Serial.println(F("Il n'y a pas de capteur Redox "));
      
    if (Capteur_O2_A2)
    {    
      Serial.println(F("Le capteur Oxygene doit etre connecté sur A2"));
      O2_A = fflash_O2_A.read();
      O2_B= flash_O2_B.read();
      if ( O2_A == 0.00)
        Serial.println(F("Attention coeff d'etalonnage A = 0 il faut etalonner la sonde"));
      if ( O2_B == 0.00)
        Serial.println(F("Attention coeff d'etalonnage B = 0 il faut etalonner la sonde"));      
    }
    else 
      Serial.println(F("Il n'y a pas de capteur Oxygene "));
      
    if (Capteur_Conducti_A3)
    {
      Serial.println(F("Le capteur Conductivimetre doit etre connecté sur A3")); 
      Conducti_A = flash_Conducti_A.read();
      Conducti_B= flash_Conducti_B.read();
      if ( Conducti_A == 0.00)
        Serial.println(F("Attention coeff d'etalonnage A = 0 il faut etalonner la sonde"));
      if ( Conducti_B == 0.00)
        Serial.println(F("Attention coeff d'etalonnage B = 0 il faut etalonner la sonde"));
    }
    else 
      Serial.println(F("Il n'y a pas de capteur Conductivimetre "));

    V_PH_A0 = V_Redox_A1 = V_O2_A2 = V_Conducti_A3 = 999; // initialisation des variables tension des capteurs 
    Init_Relay();  // initialisation de la carte relay pour la mise sous tension des capteurs   
    Write_Coeff_SD(&C_PH_A,&C_PH_B,&Redox_A,&Redox_B,&O2_A,&O2_B,&Conducti_A,&Conducti_B); //ecriture des  coeff sur fichier SD
    
    
 #elif defined(TYPE_DATALOGGER_ENERGIE) //Code compile si le datalogger est un energie
  
    Serial.println(F("Ce datalogger est de type energie "));
    if (Capteur_Energie_A0)
    {
      Serial.print(F("Le capteur_A0 est de type courant max (en A) "));
      Serial.println(Capteur_Energie_A0);      
    }
    else 
      Serial.println(F("Il n'y a pas de capteur_A0 "));
    if (Capteur_Energie_A1)
    {
      Serial.print(F("Le capteur_A1 est de type courant max (en A) "));
      Serial.println(Capteur_Energie_A1);      
    }
    else 
      Serial.println(F("Il n'y a pas de capteur_A1 "));
    if (Capteur_Energie_A2)
    {
      Serial.print(F("Le capteur_A2 est de type courant max (en A) "));
      Serial.println(Capteur_Energie_A2);      
    }
    else 
      Serial.println(F("Il n'y a pas de capteur_A2 "));
    if (Capteur_Energie_A3)
    {
      Serial.print(F("Le capteur_A3 est de type courant max (en A) "));
      Serial.println(Capteur_Energie_A3);      
    }
    else 
      Serial.println(F("Il n'y a pas de capteur_A3 "));
      
    IEnergie_0 = IEnergie_1 = IEnergie_2 = IEnergie_3 = 999; // initialisation des variables tension des capteurs
    Init_Relay(); // initialisation de la carte relay pour la mise sous tension des capteurs

 #elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE) //Code compile si le datalogger est un hauteur de boue
  
    Serial.println(F("Ce datalogger est de type hauteur de boue "));
    Serial.print(F("La distance mesurée en centimetre à reservoir vide est de :  "));
    Serial.println(Ht_a_Vide);
    Serial1.begin(19200); //Début de la liaison série capteur Ht Boue    
    
 #endif 

  // config broche d'interruption wifi
  pinMode(WIFI_INTERRUPT_Pin, INPUT);
  WIFI_STATE = false ;
  LowPower.attachInterruptWakeup(WIFI_INTERRUPT_Pin, WIFI_INTERRUPT_Pg, FALLING);
  
  Serial.print(F("Wifi "));
  Serial.print(STATION_NAME); 
  Serial.println(F(" operationnel ")); 
  digitalWrite(STATE_LED_Pin, LOW);

  // Le datalogger est pret à mesurer
}

void loop() 
{
    if (WIFI_STATE == true)//le systeme a ete reveille par le bouton wifi
      Awake_Wifi();//appel à un sous programme de l'onglet Base_Wifi
      
    else // Reveil sequentiel debut d'un nouveau cycle de mesure
    {   
        digitalWrite(STATE_LED_Pin, HIGH); // La led d'etat est allumée
        
        #if defined(TYPE_DATALOGGER_HAUTEUR_EAU) //Code compile si le datalogger est un hauteur d'eau
        
          Get_Vbatt (&Vbatt); //Acquisition de la tension batterie (onglet Base_Vbatt)

          // les lignes suivantes font appel à des sous programmes de l'onglet Ht_Eau
          Get_Ht_Eau(&Usound_Dist,&Usound_Temp,&Usound_Noise); // acquisition des données Ht_Eau          
          if(Serial) //Affichage des données sur le moniteur serie
            Affiche_Data_Ht_Eau (&Vbatt,&Usound_Dist,&Usound_Temp,&Usound_Noise );
          if (SD_OK == true) //Ecriture des données sur la carte SD
            Write_Data_SD_Ht_Eau (&Usound_Type,&Usound_Version,&Vbatt,&Usound_Dist, &Usound_Temp,&Usound_Noise);
        
        #elif defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) //Code compile si le datalogger est un physico_chimique
        
          Get_Vbatt (&Vbatt); //Acquisition de la tension batterie (onglet Base_Vbatt)
          
          // les lignes suivantes font appel à des sous programmes de l'onglet Physico_chimique
          if (Capteur_PH_A0)//acquisition des données pH si capteur present
          {
            Get_Temp(&PH_Temp); // mesure temperature
            Sum_Temp = PH_Temp;
            Get_VPhy(&V_PH_A0, 0, 30); // mesure PH, entrée A0, temps de mise sous tension 30s
            Get_Temp(&PH_Temp); // mesure temperature
            Sum_Temp = PH_Temp + Sum_Temp ; 
            PH_Temp= Sum_Temp / 2 ;
            // calcul du PH 
            if (C_PH_A == 0.00) // Pas de calcul besoin d'un etalonnage
              PH_A0 = 0;
            else
            {
              PH_A0= V_PH_A0-C_PH_B; // calcul du PH 
              PH_A0= PH_A0 / C_PH_A; // calcul du PH
            }            
          } 
             
          if (Capteur_Redox_A1) //acquisition des données redox si capteur present
          { 
            Get_Temp(&Redox_Temp); // mesure temperature
            Sum_Temp = Redox_Temp;
            Get_VPhy(&V_Redox_A1, 1, 30); //mesure Redox, entrée A1, temps de mise sous tension 30s
            Get_Temp(&Redox_Temp); // mesure temperature
            Sum_Temp = Redox_Temp + Sum_Temp ;
            Redox_Temp= Sum_Temp / 2 ;
            //calcul Redox
            if (Redox_A == 0.00) // Pas de calcul besoin d'un etalonnage
              Redox_A1 = 0;
            else
            {
              Redox_A1= V_Redox_A1-Redox_B; // calcul du Redox
              Redox_A1= Redox_A1 / Redox_A; // calcul du Redox
            }            
          }
                       
          if (Capteur_O2_A2) //acquisition des données oxygene si capteur present
          {
            Get_Temp(&O2_Temp); // mesure temperature
            Sum_Temp = O2_Temp;
            Get_VPhy(&V_O2_A2, 2, 30); //mesure O2 entrée A2, temps de mise sous tension 30s
            Get_Temp(&O2_Temp); // mesure temperature
            Sum_Temp = O2_Temp + Sum_Temp ;
            O2_Temp= Sum_Temp / 2 ;
            //calcul O2
            if (O2_A == 0.00) // Pas de calcul besoin d'un etalonnage
              O2_A2 = 0;
            else
            {
              O2_A2= V_O2_A2-O2_B; // calcul du O2
              O2_A2= O2_A2 / O2_A; // calcul du O2
            }            
          }
            
          if (Capteur_Conducti_A3) //acquisition des données conductivité si capteur present
          { 
            Get_Temp(&Conducti_Temp); // mesure temperature
            Sum_Temp = Conducti_Temp;
            Get_VPhy(&V_Conducti_A3, 3, 30); //mesure conducti entrée A3, temps de mise sous tension 30s
            Get_Temp(&Conducti_Temp); // mesure temperature
            Sum_Temp = Conducti_Temp + Sum_Temp ;
            Conducti_Temp= Sum_Temp / 2 ; 
            // calcul Conducti
            if (Conducti_A == 0.00) // Pas de calcul besoin d'un etalonnage
              Conducti_A3 = 0;
            else
            {
              Conducti_A3= V_Conducti_A3-Conducti_B; // calcul du Conducti / EC
              Conducti_A3= Conducti_A3 / Conducti_A; // calcul du Conducti / EC
              Sum_Temp= 0.020*(Conducti_Temp-25)+1; // calcul correction en temperature de EC
              Conducti_A3 = Conducti_A3 / Sum_Temp ; // calcul correction en temperature de EC
            }       
          }          
          if(Serial) //Affichage des données sur le moniteur serie
            Affiche_Data_Phy (&Vbatt,&PH_Temp,&PH_A0,&V_PH_A0,&Redox_Temp,&Redox_A1,&V_Redox_A1,&O2_Temp,&O2_A2,&V_O2_A2,&Conducti_Temp,&Conducti_A3,&V_Conducti_A3);
          if (SD_OK == true) //Ecriture des données sur la carte SD
            Write_Data_SD_Phy (&Vbatt,&PH_Temp,&PH_A0,&V_PH_A0,&Redox_Temp,&Redox_A1,&V_Redox_A1,&O2_Temp,&O2_A2,&V_O2_A2,&Conducti_Temp,&Conducti_A3,&V_Conducti_A3);
        
        #elif defined(TYPE_DATALOGGER_ENERGIE) //Code compile si le datalogger est un energie
        
          Get_Vbatt (&Vbatt); //Acquisition de la tension batterie (onglet Base_Vbatt)

          // les lignes suivantes font appel à des sous programmes de l'onglet Energie
          if (Capteur_Energie_A0)          
            Get_VEnergie(&IEnergie_0, 0, Capteur_Energie_A0); // acquisition des données Energie_0 si present
          if (Capteur_Energie_A1)          
            Get_VEnergie(&IEnergie_1, 1, Capteur_Energie_A1); // acquisition des données Energie_1 si present 
          if (Capteur_Energie_A2)          
            Get_VEnergie(&IEnergie_2, 2, Capteur_Energie_A2); // acquisition des données Energie_2 si present 
          if (Capteur_Energie_A3)          
            Get_VEnergie(&IEnergie_3, 3, Capteur_Energie_A3); // acquisition des données Energie_3 si present           
          if(Serial) //Affichage des données sur le moniteur serie
            Affiche_Data_Energie (&Vbatt,&IEnergie_0,&IEnergie_1,&IEnergie_2,&IEnergie_3);
          if (SD_OK == true) //Ecriture des données sur la carte SD
            Write_Data_SD_Energie (&Vbatt,&IEnergie_0,&IEnergie_1,&IEnergie_2,&IEnergie_3);


        #elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE) //Code compile si le datalogger est un hauteur de Boue
        
          Get_Vbatt (&Vbatt); //Acquisition de la tension batterie (onglet Base_Vbatt)
          
          // les lignes suivantes font appel à des sous programmes de l'onglet Ht_Boue
          Get_Ht_Boue(&Ht_Boue_Dist,&Ht_Boue_Temp); // acquisition des données Ht_Boue          
          if(Serial) //Affichage des données sur le moniteur serie
            Affiche_Data_Ht_Boue (&Vbatt,&Ht_Boue_Dist,&Ht_Boue_Temp );
          if (SD_OK == true) //Ecriture des données sur la carte SD
            Write_Data_SD_Ht_Boue (&Vbatt,&Ht_Boue_Dist, &Ht_Boue_Temp);
                
        #endif 
        
        // Ajouter les autres dataloggers ici
        digitalWrite(STATE_LED_Pin, LOW); //la led d'etat est eteinte
    }    
    delay (1000);
    Alarm_Zero_RTC_Config(); //l'alarme est de nouveau configuree (fonction dans onglet Base_RTC) 
    LowPower.sleep();        // le systeme est mis en veille 
    Zero_RTC.disableAlarm(); //Le systeme se reveille l'alarme est desactivée
}

void WIFI_INTERRUPT_Pg() //programme interruption quand reveil par le bouton wifi
{
   WIFI_STATE = !WIFI_STATE ;
}
void Zero_RTC_Interrupt_Pg()//programme interruption quand reveil sequentiel pour mesure
{
  WIFI_RTC_STATE = false ;
}
