// Utilisation du capteur DF Robot SEN0358
// Dist max =1500mm 
// Dist min= 100mm
// 
// A rajouter module GROVE RS485 ref : 103020193
// A connecter sur SERIAL
// niveau capteur fil blanc RS485A fil bleu RS485B fil noir GND fil orange alim entre 7 et 15 V
//
// Toutes les informations sur: https://reversaal.gitlab.irstea.page/setier_datalogger/


#include <ArduinoModbus.h>
#include <ArduinoRS485.h>

#define   SLAVE_ADDR                ((uint16_t)0x0C)

#define   TEMP_CPT_SEL_BIT          ((uint16_t)0x01)
#define   TEMP_CPT_ENABLE_BIT       ((uint16_t)0x01 << 1)
#define   MEASURE_MODE_BIT          ((uint16_t)0x01 << 2)
#define   MEASURE_TRIG_BIT          ((uint16_t)0x01 << 3)


typedef enum{ 
  ePid,
  eVid,
  eAddr,
  eComBaudrate,
  eComParityStop,
  eDistance,
  eInternalTempreture,
  eExternTempreture,
  eControl,
  eNoise
}eRegIndex_t;//Sensor register index


uint16_t cr ;

/*
 *@brief Read data from holding register of client
 *
 *@param addr ： Address of Client
 *@param reg: Reg index
 *@return data if execute successfully, false oxffff.
 */
uint16_t readData(uint16_t addr, eRegIndex_t reg)
{
  uint16_t data;
  if (!ModbusRTUClient.requestFrom(addr, HOLDING_REGISTERS, reg, 1)){
    Serial.println(F(" Probleme communication capteur, est il alimenté ? "));
    Serial.println(ModbusRTUClient.lastError());
    data = 0xffff;
  }else{
    data =  ModbusRTUClient.read();
  }
  return data;
}
/*
 *@brief write data to holding register of client 
 *
 *@param addr ： Address of Client
 *@param reg: Reg index
 *@param data: The data to be written
 *@return 1 if execute successfully, false 0.
 */
uint16_t writeData(uint16_t addr, eRegIndex_t reg, uint16_t data)
{
  if (!ModbusRTUClient.holdingRegisterWrite(addr, reg, data)){
    Serial.println(F(" Probleme communication capteur, est il alimenté ? "));
    Serial.println(ModbusRTUClient.lastError());
    return 0;
  }else
    return 1;
}


void Init_Ht_Eau(uint16_t*type, uint16_t *serie) //Initialisation du capteur
{
  RS485.setPins(PIN_SERIAL1_TX,-1,-1);// initie les pins utilisé pour la liaison RS485
  ModbusRTUClient.begin(19200);
  cr = 0;
  cr |= MEASURE_MODE_BIT;//Set bit2=1 , Set to trigger mode
  cr &= ~(uint16_t)TEMP_CPT_SEL_BIT;//Select internal temperature compensation
  cr &= ~(uint16_t)TEMP_CPT_ENABLE_BIT;//enable temperature compensation
  writeData(SLAVE_ADDR, eControl, cr); //Writes the setting value to the control register
  delay(100);
  *type = readData(SLAVE_ADDR, ePid);
  *serie = readData(SLAVE_ADDR, eVid);
  return;
}

void Get_Ht_Eau(word *dist, float *Utemp, byte *noise)
{
   cr |= MEASURE_TRIG_BIT;//Set trig bit
  writeData(SLAVE_ADDR, eControl, cr); //Write the value to the control register and trigger a ranging
  delay(300);//Delay of 300ms(minimum delay should be greater than 30ms) is to wait for the completion of ranging
  *dist = readData(SLAVE_ADDR, eDistance) / 10; //Read distance register, one LSB is 0.1mm
  
  *Utemp =  (float)readData(SLAVE_ADDR, eInternalTempreture) / 10.0;//Read the temperature register, one LSB is 0.1℃
  
  *noise =  (byte)readData(SLAVE_ADDR, eNoise);
  
  delay(500);
  return;

}

void Init_SD_Ht_Eau( uint16_t *Type_Capteur, uint16_t *Version_Capteur ) //Fonction d'initialisation de la carte SD
{
  
    SD_File=SD.open("Data_HE.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       Serial.println(F("Fichier absent. Création de ce dernier."));
       SD_File=SD.open("Data_HE.txt", FILE_WRITE);       
       Serial.println(F("Fichier crée, ecriture entete"));
       SD_File.print(F("Nom du datalogger : "));
       SD_File.print(F ("ID_NAME ")); 
       SD_File.println(STATION_NAME);
       SD_File.println(F("Mesure de la hauteur d'eau avec capteur SEN0358"));
       SD_File.print(F("Type de capteur (Hexa) : "));
       SD_File.println(*Type_Capteur,HEX );
       SD_File.print(F("Version du capteur (Hexa) : "));
       SD_File.println(*Version_Capteur,HEX );
       SD_File.println(F("Version programme v0 / Helene Guyard / IGE-IRD /partie Wifi / Valerie Quatela / IGE-CNRS"));
       SD_File.println(F("Hardware: Licensed under CERN-OHL-S v2 or any later version"));
       SD_File.println(F("Software: Licensed under the GNU General Public License v3.0"));
       SD_File.println(F("Projet Setier / Mesures:")); 
       SD_File.println(F("Date et Heure; Vbatt (V); Distance capteur / eau (mm); Temperature capteur distance (°C); Facteur qualité mesure; Hauteur à debit nul; Hauteur d'eau; Profondeur du canal; cadence; "));
       SD_File.println(F("START_DATA"));       
    }
    else //Sinon
    {
        Serial.println(F("Fichier présent. Aucune modification apportée."));
    }
    SD_File.close(); //Fermeture du fichier   
  
}

void Write_Data_SD_Ht_Eau (uint16_t *Type_Capteur, uint16_t *Version_Capteur,float*Vofbatt, word*param1,float*param2, byte*param3)
{
    DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307
    SD_File=SD.open("Data_HE.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       SD_File=SD.open("Data_HE.txt", FILE_WRITE); 
       SD_File.print(F("Nom du datalogger : ")); 
       SD_File.print(F ("ID_NAME "));
       SD_File.println(STATION_NAME);   
       SD_File.println(F("Mesure de la hauteur d'eau avec capteur SEN0358"));
       SD_File.print(F("Type de capteur (Hexa) : "));
       SD_File.println(*Type_Capteur,HEX );
       SD_File.print(F("Version du capteur (Hexa) : "));
       SD_File.println(*Version_Capteur,HEX );       
       SD_File.println(F("Version programme v0 / Helene Guyard / IGE-IRD /partie Wifi / Valerie Quatela / IGE-CNRS"));
       SD_File.println(F("Hardware: Licensed under CERN-OHL-S v2 or any later version"));
       SD_File.println(F("Software: Licensed under the GNU General Public License v3.0"));
       SD_File.println(F("Projet Setier / Mesures:")); 
       SD_File.println(F("Date et Heure; Vbatt (V) ; Distance capteur / eau (mm); Temperature capteur distance (°C); Facteur qualité mesure; Hauteur à debit nul; Hauteur d'eau; Profondeur du canal; Cadence;  "));
       SD_File.println(F("START_DATA"));              
    }
    SD_File.close();     
    SD_File=SD.open("Data_HE.txt", FILE_WRITE);
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.print(':');
    if (DS1307_Time.second()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.second(), DEC);
    SD_File.print(" ; ");
    SD_File.print(*Vofbatt);
    SD_File.print(" ; ");
    SD_File.print(*param1);
    SD_File.print(" ; ");
    SD_File.print(*param2);
    SD_File.print(" ; ");   
    SD_File.print(*param3);
    SD_File.print(" ; ");
    SD_File.print(Ht_Debit_NUL);
    SD_File.print(" ; ");
    SD_File.print(Ht_Debit_NUL-*param1);
    SD_File.print(" ; ");
    SD_File.print(Profondeur_Canal);
    SD_File.print(" ; ");
    SD_File.print(Ma_Cadence);
    SD_File.print(" ; ");
    SD_File.println();
    SD_File.close();
}
void Affiche_Data_Ht_Eau (float *Vofbatt, word *param1,float *param2,byte *param3 )
 {
      
   Serial.print(F("Nom du datalogger : ")); 
   Serial.println(STATION_NAME);
   DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307    
   Serial.print(DS1307_Time.year(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.month(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.day(), DEC);
   Serial.print("  ");
   Serial.print(DS1307_Time.hour(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.minute(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.second(), DEC);
   Serial.print(" ; ");
   Serial.print(F(" Tension Vbatt : "));
   Serial.print ( *Vofbatt);
   Serial.print (F( " V "));
   Serial.print(F(" Distance capteur Uson "));
   Serial.print(*param1);               //Print distance
   Serial.print(F(" mm "));
   Serial.print(F(" Temperature capteur Uson "));
   Serial.print(*param2);
   Serial.print(F(" °C "));
   Serial.print(F(" Facteur qualité capteur Uson "));
   Serial.print(*param3);
   Serial.print(F(" Hauteur a debit nul: ")); 
   Serial.print(Ht_Debit_NUL);
   Serial.print(F(" mm"));
   Serial.print(F(" Hauteur d'eau: ")); 
   Serial.print(Ht_Debit_NUL - *param1);
   Serial.print(F(" mm "));
   Serial.print(F("Profondeur du canal"));
   Serial.print(Profondeur_Canal);
   Serial.print(F(" mm "));
   Serial.print(F("Frequence de mesure: "));
   Serial.print(Ma_Cadence);
   Serial.println(); 
     
  }
