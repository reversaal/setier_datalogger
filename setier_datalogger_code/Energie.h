// Utilisation de capteur DF Robot au choix selon valeur max voulue
// 20A max : SEN0211
// 10A max : SEN0288
// 5A  max : SEN0287
// 
// A rajouter 1 module GROVE 4 relais ref : 103020133
// A connecter sur TWI en parallele du module RTC
// Cordon Grove double ref : 110990092
// Toutes les informations sur: https://reversaal.gitlab.irstea.page/setier_datalogger/



#include <multi_channel_relay.h>
 
Multi_Channel_Relay relay;
void Init_Relay (void)
{
  // Set I2C address and start relay
  relay.begin(0x11); 
}

void Get_VEnergie(float *Vofenergie, byte energie_number, byte energie_type)
{
    long Venerg_Value=0;
    long  sum=0;
    if (energie_number==0)
    {
      relay.turn_on_channel(4);
      delay (250);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          Venerg_Value=analogRead(A0);
          sum=Venerg_Value+sum;
          delay(20);
      }
      relay.turn_off_channel(4);
    }
    if (energie_number==1)
    {
      relay.turn_on_channel(3);
      delay (250);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          Venerg_Value=analogRead(A1);
          sum=Venerg_Value+sum;
          delay(20);
      }
      relay.turn_off_channel(3);
    }
    if (energie_number==2)
    {
      relay.turn_on_channel(2);
      delay (250);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          Venerg_Value=analogRead(A2);
          sum=Venerg_Value+sum;
          delay(20);
      }
      relay.turn_off_channel(2);
    }
    if (energie_number==3)
    {
      relay.turn_on_channel(1);
      delay (250);
      analogReadResolution(12);
      for(int i=0;i<20;i++)
      {
          Venerg_Value=analogRead(A3);
          sum=Venerg_Value+sum;
          delay(20);
      }
      relay.turn_off_channel(1);
    }
    
    sum=sum/20;
    *Vofenergie= sum* 2.333; //(0.707 * 3.3 = 2.333   O.707= racine de 2 umax  au lieu de U eff)
    *Vofenergie = *Vofenergie/8190; // 4095 *2 (4095=2puissance12-1)
    *Vofenergie = *Vofenergie * 1.52; // prise en compte du pont diviseur en entree analogique de carte connecteur 
    *Vofenergie = *Vofenergie * energie_type; // (energie type = 5, 10 ou 20A )
    
    return;  
}

void Write_Data_SD_Energie (float*Vofbatt, float *param1,float*param2, float*param3, float*param4 )
{
    DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307
    SD_File=SD.open("Data_E.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       SD_File=SD.open("Data_E.txt", FILE_WRITE); 
       SD_File.print(F("Nom du datalogger : ")); 
       SD_File.print(F ("ID_NAME "));
       SD_File.println(STATION_NAME);   
       SD_File.println(F("Mesure Energie"));
       SD_File.println(F("Version programme v0 / Helene Guyard / IGE-IRD /partie Wifi / Valerie Quatela / IGE-CNRS"));
       SD_File.println(F("Hardware: Licensed under CERN-OHL-S v2 or any later version"));
       SD_File.println(F("Software: Licensed under the GNU General Public License v3.0"));
       SD_File.println(F("Projet Setier / Mesures:")); 
       SD_File.println(F("Date et Heure; Vbatt (V) ; Capteur_AO (A); Capteur_A1 (A); Capteur_A2 (A); Capteur_A3 (A); Cadence;  "));
       SD_File.println(F("START_DATA"));              
    }
    SD_File.close();     
    SD_File=SD.open("Data_E.txt", FILE_WRITE);
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.print(':');
    if (DS1307_Time.second()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.second(), DEC);
    SD_File.print(" ; ");
    SD_File.print(*Vofbatt);
    SD_File.print(" ; ");
    SD_File.print(*param1);
    SD_File.print(" ; ");
    SD_File.print(*param2);
    SD_File.print(" ; ");   
    SD_File.print(*param3);
    SD_File.print(" ; ");
    SD_File.print(*param4);
    SD_File.print(" ; ");
    SD_File.print(Ma_Cadence);
    SD_File.print(" ; ");
    SD_File.println();
    SD_File.close();
}

void Affiche_Data_Energie (float *Vofbatt, float *param1,float *param2,float *param3, float *param4 )
 {
      
   Serial.print(F("Nom du datalogger : ")); 
   Serial.println(STATION_NAME);
   DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307    
   Serial.print(DS1307_Time.year(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.month(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.day(), DEC);
   Serial.print("  ");
   Serial.print(DS1307_Time.hour(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.minute(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.second(), DEC);
   Serial.print(" ; ");
   Serial.print(F(" Tension Vbatt : "));
   Serial.print ( *Vofbatt);
   Serial.print (F( " V "));
   Serial.print(F(" Courant capteur A0 "));
   Serial.print(*param1);               //Print distance
   Serial.print(F(" A "));
   Serial.print(F(" Courant capteur A1 "));
   Serial.print(*param2);
   Serial.print(F(" A "));
   Serial.print(F(" Courant capteur A2 "));
   Serial.print(*param3);
   Serial.print(F(" A "));
   Serial.print(F(" Courant capteur A3 "));
   Serial.print(*param4);
   Serial.print(F(" A "));   
   Serial.print(F("Frequence de mesure: "));
   Serial.print(Ma_Cadence);
   Serial.println(); 
     
  }
