// Utilisation du capteur DF Robot SEN0246
// Dist max =550cm 
// Dist min= 35cm
// 
// A rajouter module GROVE RS485 ref : 103020193
// A connecter sur SERIAL
// niveau capteur fil jaune RS485A fil vert RS485B fil noir GND fil rouge alim entre 6 et 12 V
//
// Toutes les informations sur: https://reversaal.gitlab.irstea.page/setier_datalogger/



#define header_H    0x55 //Frame header
#define header_L    0xAA //Frame header
#define device_Addr 0x11 //Module address
#define data_Length 0x00 //Data length
#define get_Dis_CMD 0x02 //Distance measurement commands
#define Dis_checksum   (header_H+header_L+device_Addr+data_Length+get_Dis_CMD) //Checksum
#define get_Temp_CMD 0x03 //A temperature measurement command
#define Temp_checksum     (header_H+header_L+device_Addr+data_Length+get_Temp_CMD) //Checksum




void Get_Ht_Boue(float *Dist,float *Temp)// SP acquisition des données 
{
 unsigned char CMD[6];
 unsigned char Rx_DATA[8];
 unsigned int  SEN0246_Dist;
 int i ;
 SEN0246_Dist=0;
 
 // mesure de la distance
 
 CMD [0]= {header_H}; //Distance measurement command packet
 CMD [1]= {header_L};
 CMD [2]= {device_Addr};
 CMD [3]= {data_Length};
 CMD [4]= {get_Dis_CMD};
 CMD [5]= {Dis_checksum};
  
 for(i=0;i<6;i++)
 {
    Serial1.write(CMD[i]);
 }
 delay(150);  //Wait for the end of distance measurement
 i=0;
 while (Serial1.available())
 {  //Read return data package (NOTE: Demo is just for your reference, the data package haven't be calibrated yet)
    Rx_DATA[i++]=(Serial1.read());
 }
 SEN0246_Dist=((Rx_DATA[5]<<8)|Rx_DATA[6]); //Get the distance data
 *Dist=(float)SEN0246_Dist;

 // mesure de la temperature
 CMD [4]= {get_Temp_CMD};
 CMD [5]= {Temp_checksum};
 for(i=0;i<6;i++)
 {
    Serial1.write(CMD[i]);
 }
 delay(50);  //Wait data to return
 i=0;
 while (Serial1.available())
 {  //Read return data package (NOTE: Demo is just for your reference, the data package haven't be calibrated yet)
    Rx_DATA[i++]=(Serial1.read());
 }
 *Temp=((Rx_DATA[5]<<8)|Rx_DATA[6]);  //Get temperature(10 times temperature value here)
 *Temp = *Temp/10;             //Print temperature
}


void Write_Data_SD_Ht_Boue (float*Vofbatt, float*param1,float*param2) //SP ecriture des données sur carte SD
{
    DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307
    SD_File=SD.open("Data_HB.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       SD_File=SD.open("Data_HB.txt", FILE_WRITE); 
       SD_File.print(F("Nom du datalogger : ")); 
       SD_File.print(F ("ID_NAME "));
       SD_File.println(STATION_NAME);   
       SD_File.println(F("Mesure de la hauteur de boue avec capteur SEN0246"));
       SD_File.println(F("Version programme v0 / Helene Guyard / IGE-IRD /partie Wifi / Valerie Quatela / IGE-CNRS"));
       SD_File.println(F("Hardware: Licensed under CERN-OHL-S v2 or any later version"));
       SD_File.println(F("Software: Licensed under the GNU General Public License v3.0"));
       SD_File.println(F("Toutes les informations sur: https://reversaal.gitlab.irstea.page/setier_datalogger/"));
       SD_File.println();
       SD_File.println(F("Projet Setier / Mesures:")); 
       SD_File.println(F("Date et Heure; Vbatt (V) ; Distance capteur / boue (cm); Temperature capteur (°C); Hauteur à vide; Hauteur de boue; Cadence;  "));
       SD_File.println(F("START_DATA"));              
    }
    SD_File.close();     
    SD_File=SD.open("Data_HB.txt", FILE_WRITE);
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.print(':');
    if (DS1307_Time.second()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.second(), DEC);
    SD_File.print(" ; ");
    SD_File.print(*Vofbatt);
    SD_File.print(" ; ");
    SD_File.print(*param1);
    SD_File.print(" ; ");
    SD_File.print(*param2);
    SD_File.print(" ; ");   
    SD_File.print(Ht_a_Vide);
    SD_File.print(" ; ");
    SD_File.print(Ht_a_Vide-*param1);
    SD_File.print(" ; ");    
    SD_File.print(Ma_Cadence);
    SD_File.print(" ; ");
    SD_File.println();
    SD_File.close();
}


void Affiche_Data_Ht_Boue (float *Vofbatt, float *param1,float *param2 )//Affichage sur moniteur serie des donnees
{      
   Serial.print(F("Nom du datalogger : ")); 
   Serial.println(STATION_NAME);
   DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307    
   Serial.print(DS1307_Time.year(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.month(), DEC);
   Serial.print('/');
   Serial.print(DS1307_Time.day(), DEC);
   Serial.print("  ");
   Serial.print(DS1307_Time.hour(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.minute(), DEC);
   Serial.print(':');
   Serial.print(DS1307_Time.second(), DEC);
   Serial.print(" ; ");
   Serial.print(F(" Tension Vbatt : "));
   Serial.print ( *Vofbatt);
   Serial.print (F( " V "));
   Serial.print(F(" Distance capteur : "));
   Serial.print(*param1);               //Print distance
   Serial.print(F(" cm "));
   Serial.print(F(" Temperature capteur "));
   Serial.print(*param2);
   Serial.print(F(" °C "));   
   Serial.print(F(" Hauteur a vide : ")); 
   Serial.print(Ht_a_Vide);
   Serial.print(F(" cm"));
   Serial.print(F(" Hauteur de boue: ")); 
   Serial.print(Ht_a_Vide - *param1);
   Serial.print(F(" cm "));
   Serial.print(F("Frequence de mesure: "));
   Serial.print(Ma_Cadence);
   Serial.println(); 
     
  }
