/**************************************************************************************************/
/*************** FICHIER DE CONFIGURATION DU DATALOGGER  ******************************************/
/**************************************************************************************************/

/************* CHOISISSEZ LE NOM DU DATALOGGER   **************************************************/
/********* remplacer le nom entre guillemets par le votre attention pas plus de 8 caracteres ******/
/********* conservez les guillemets ***************************************************************/
/********* Le reseau wifi cree portera le nom du datalogger **************************************/

#define STATION_NAME  "H_eau"  // exemple le nom de ma station est TOTO_1
/************* CHOISISSEZ LA FREQUENCE DE MESURE **************************************************/
/******** SUPPRIMER LES DEUX SLASH DE DEBUT DE LIGNE DE LA FREQUENCE CHOISI (//) ******************/
/******** Un seul choix possible ******************************************************************/

#define Ma_Cadence   1 // 1 mesure  toutes les minutes (pas adapté au datalogger physico chimique)
//#define Ma_Cadence   5 // 1 mesure  toutes 5 minutes 
//#define Ma_Cadence  15 // 1 mesure  toutes les 15 min
// #define Ma_Cadence  30 // 1 mesure   toutes les demie heure 
//#define Ma_Cadence  60 // 1 mesure  par heure
// #define Ma_Cadence  24 // 1 mesure  par jour à midi

/************* CHOIX 1 DATALOGGER DE HAUTEUR D EAU **********************************************/
/********** SUPPRIMER LES DEUX SLASH DE DEBUT DES LIGNES SUIVANTES (//)**************************/
/******* Renseignez la distance extremite du capteur fond du canal (valeur à debit nul) en mm****/
/******* Attention la distance minimum mesurable par le capteur est de   100 mm *****************/
/******* Renseignez la profondeur du canal en mm ************************************************/

#define TYPE_DATALOGGER_HAUTEUR_EAU 1
#define Ht_Debit_NUL 700 // ex ma distance est de 300 mm la ligne s 'ecrit #define Ht_Debit_NUL 300
#define Profondeur_Canal 300 //ex ma profondeur du canal venturi est de300mm 
 

/************* CHOIX 2 DATALOGGER DE PHYSICO CHIMIQUE *******************************************/
/********** SUPPRIMER LES DEUX SLASH DE DEBUT DES LIGNES SUIVANTES (//)**************************/

//#define TYPE_DATALOGGER_PHYSICO_CHIMIQUE 2
//#define Capteur_PH_A0 1  //remplacer le 0 (pas de capteur) par 1 si capteur PH present
//#define Capteur_Redox_A1 1  //remplacer le 0 (pas de capteur) par 1 si capteur Redox present
//#define Capteur_O2_A2 1  //remplacer le 0 (pas de capteur) par 1 si capteur O2 present
//#define Capteur_Conducti_A3 1  //remplacer le 0 (pas de capteur) par 1 si capteur Conductivité present


/************* CHOIX 3 DATALOGGER ENERGIE *******************************************/
/********** SUPPRIMER LES DEUX SLASH DE DEBUT DES 5 LIGNES SUIVANTES (//)**************************/

//#define TYPE_DATALOGGER_ENERGIE 3
//#define Capteur_Energie_A0 20  //remplacer le 0 (pas de capteur) par 5 si c'est un capteur 5A / 10 pour 10A / 20 pour 20A
//#define Capteur_Energie_A1 0  //remplacer le 0 par 5 si c'est un capteur 5A / 10 pour 10A / 20 pour 20A
//#define Capteur_Energie_A2 0  //remplacer le 0 par 5 si c'est un capteur 5A / 10 pour 10A / 20 pour 20A
//#define Capteur_Energie_A3 0  //remplacer le 0 par 5 si c'est un capteur 5A / 10 pour 10A / 20 pour 20A


/************* CHOIX 4 DATALOGGER DE HAUTEUR DE BOUE **********************************************/
/********** SUPPRIMER LES DEUX SLASH DE DEBUT DES LIGNES SUIVANTES (//)**************************/
/******* Renseignez la distance extremite du capteur / fond du reservoir (valeur quand reservoir vide) en cm****/
/******* Attention la distance minimum mesurable par le capteur est de   35 cm *****************/
/******* Attention la distance maximum mesurable par le capteur est de   550 cm *****************/
/***********************************************************************************************/

//#define TYPE_DATALOGGER_HAUTEUR_BOUE 4
//#define Ht_a_Vide 700 
 
