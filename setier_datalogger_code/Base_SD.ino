/*Fichier contenant les fonctions de la carte µSD: MKR MEM Shield ref: ASX00008*/

void Init_SD_Card()
{
  if(!SD.begin( SD_Pin )) //Si aucune carte SD n'est détectée
  {
    Serial.println(F("Carte SD non détectée"));
    SD_OK = false;
  }
  else
  {  
    Serial.println(F("Carte SD trouvée et initialisée")); 
    SD_OK = true;
  } 
}
void Write_WIFI_SD () // ecriture des log de connexions wifi permet de connaitre les date des tournées de maintenance
{
    DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307     
    SD_File=SD.open("LogWifi.txt", FILE_READ);
    if(!SD_File) //Ouverture du fichier Data
    {
       SD_File=SD.open("LogWifi.txt", FILE_WRITE);                
       SD_File.println(F("Fichier des horaires de connexion wifi"));
       SD_File.println(F("START_DATA"));           
    } 
    SD_File.close();
    SD_File=SD.open("LogWifi.txt", FILE_WRITE);   
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute()<10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.println();
    SD_File.close();
}
