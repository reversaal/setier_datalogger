// Carte lecture tension batterie: Module GROVE diviseur de tension ref 104020000 (connecteur A5/A6)


void Get_Vbatt(float *Vofbatt) //lecture tension batterie
{
    long Vbatt_Value=0;
    long  sum=0;
    analogReadResolution(12);
    for(int i=0;i<10;i++)
    {
        Vbatt_Value=analogRead(Vbatt_Pin);
        sum=Vbatt_Value+sum;
        delay(2);
    }
    sum=sum/10;
    *Vofbatt= sum*33;
    *Vofbatt = *Vofbatt/4095; // resolution 12 bits (4095 = 2puissance12 - 1)
    *Vofbatt = *Vofbatt * 1.52;// prise en compte du pont diviseur en entree analogique de carte connecteur 
    return;
  
}
