// Carte RTC : Module GROVE RTC DS1307 ref 101020013 (connecteur TWI)
//rtc interne du microcontroleur SAMD21 (Zero_RTC)

void Alarm_Zero_RTC(void) //Ne pas supprimer utile
 {  
 }

void Init_DS1307_RTC (void) 
{  
  if (! DS1307_RTC.begin()) 
  {
    Serial.println("pas de DS1307");
    return;
  }
  // following line sets the RTC to the date & time this sketch was compiled
  // DS1307_RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  //DS1307_RTC.adjust(DateTime(2022, 2, 23, 17, 22, 0));
  if (!New_Compil.read()) //s il y a eu compilation la RTC DS1307 est automatiquement mis à l'heure du PC
  {
    DS1307_RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
    DS1307_Time = DS1307_RTC.now(); // Mise à l'heure de la RTC DS1307  
    Serial.print("La RTC DS1307 a ete mise à l'heure de l'ordinateur : ");
    Serial.print(DS1307_Time.year(), DEC);
    Serial.print('/');
    Serial.print(DS1307_Time.month(), DEC);
    Serial.print('/');
    Serial.print(DS1307_Time.day(), DEC);
    Serial.print(" ");
    Serial.print(DS1307_Time.hour(), DEC);
    Serial.print(':');
    Serial.print(DS1307_Time.minute(), DEC);
    Serial.print(':');
    Serial.println(DS1307_Time.second(), DEC);   
  }
  if (! DS1307_RTC.isrunning()) 
  {
    Serial.println("RTC is NOT running!");    
  }  
}

void Init_Zero_RTC (void)
{
  Zero_RTC.begin();
  Alarm_Zero_RTC_Config;
}


void Alarm_Zero_RTC_Config (void) // Pour info la priorité de l'interruption de la RTC est à 0x00
{
  //Mise à l'heure de la rtc zero
  DS1307_Time = DS1307_RTC.now();  
  Zero_RTC.setTime(DS1307_Time.hour(), DS1307_Time.minute(), DS1307_Time.second());
  Zero_RTC.setDate(DS1307_Time.day(), DS1307_Time.month(), DS1307_Time.year());

  // reglage alarme
  switch (Ma_Cadence)
  {
    case 1: // 1 mesure par minute
      Zero_RTC.setAlarmSeconds(30);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_SS);
      break;
    case 5: // 1 mesure toutes les 5 minutes
      int AMin;
      int Res;
      Zero_RTC.setAlarmSeconds(0);
      AMin=Zero_RTC.getMinutes();
      do
      {
        AMin++;
        Res=AMin%5;
        
      } while (Res != 0);
      if (AMin==60)      
        Zero_RTC.setAlarmMinutes(0);
      else 
        Zero_RTC.setAlarmMinutes(AMin);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 15: // 1 mesure toutes les 15 minutes
      Zero_RTC.setAlarmSeconds(0);
      if(Zero_RTC.getMinutes() >= 0 && Zero_RTC.getMinutes() < 15)
          Zero_RTC.setAlarmMinutes(15);
      else
      {
        if (Zero_RTC.getMinutes() >= 15 && Zero_RTC.getMinutes() < 30)
          Zero_RTC.setAlarmMinutes(30);
        else
        {
          if (Zero_RTC.getMinutes() >= 30 && Zero_RTC.getMinutes() < 45)
            Zero_RTC.setAlarmMinutes(45);
          else
            Zero_RTC.setAlarmMinutes(0);
        }
      }
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 30: // 1 mesure toutes les 30 minutes à 00 et 30
      Zero_RTC.setAlarmSeconds(0);
        if(Zero_RTC.getMinutes() >= 0 && Zero_RTC.getMinutes() < 30)
          Zero_RTC.setAlarmMinutes(30);
        else
          Zero_RTC.setAlarmMinutes(0);  
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 60: // 1 mesure toutes les heures à l'heure pile
      Zero_RTC.setAlarmSeconds(00);
      Zero_RTC.setAlarmMinutes(00);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 24: // 1 mesure par jour à midi
      Zero_RTC.setAlarmSeconds(00);
      Zero_RTC.setAlarmMinutes(00);
      Zero_RTC.setAlarmHours(12);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_HHMMSS);
      break;
  }    
    Zero_RTC.attachInterrupt(Alarm_Zero_RTC);  
}


void Alarm_Zero_RTC_Wifi (void) // la tempo est reglee à Wifi_length en min
{     // Pour info la priorité de l'interruption de la RTC est à 0x00
  int WifiEnd;
  //Mise à l'heure de la rtc zero
  DS1307_Time = DS1307_RTC.now();  
  Zero_RTC.setTime(DS1307_Time.hour(), DS1307_Time.minute(), DS1307_Time.second());
  Zero_RTC.setDate(DS1307_Time.day(), DS1307_Time.month(), DS1307_Time.year());

  // reglage alarme
  
  Zero_RTC.setAlarmSeconds(0);
  WifiEnd = Zero_RTC.getMinutes() + Wifi_length;
      
  if(WifiEnd < 60)
      Zero_RTC.setAlarmMinutes(WifiEnd);
  else
  {      
      WifiEnd = WifiEnd - 60 ;
      Zero_RTC.setAlarmMinutes(WifiEnd);
  } 
  Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
     
  Zero_RTC.attachInterrupt(Zero_RTC_Interrupt_Pg);  
}

 
 
