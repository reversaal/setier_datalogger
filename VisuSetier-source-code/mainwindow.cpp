/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : mainwindow.cpp
# -------------------------------------------------------
#  30/11/2022     |   Valérie QUATELA    | Version 0.5 |
##########################################################*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "confSetierHE.h"
#include "confSetierEN.h"
#include "confSetierPC.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

void MainWindow::init()
{

        // Menus

        connect(ui->pushButtonSetierHE, SIGNAL(clicked(bool)),this,SLOT(ouvrirConfSetierHE()));
        connect(ui->pushButtonSetierPC, SIGNAL(clicked(bool)),this,SLOT(ouvrirConfSetierPC()));
        connect(ui->pushButtonSetierE, SIGNAL(clicked(bool)),this,SLOT(ouvrirConfSetierEN()));
        connect(ui->actionA_Propos, SIGNAL(triggered(bool)), this, SLOT(ouvrirApropos()));
}


void MainWindow::ouvrirConfSetierHE()
{
    ConfSetierHE *confSetierHE = new ConfSetierHE;
    confSetierHE->show();
}

void MainWindow::ouvrirConfSetierPC()
{
    ConfSetierPC *confSetierPC = new ConfSetierPC;
    confSetierPC->show();
}

void MainWindow::ouvrirConfSetierEN()
{
    ConfSetierEN *confSetierEN = new ConfSetierEN;
    confSetierEN->show();
}

void MainWindow::ouvrirApropos()
{
    QMessageBox::information(this, "A propos de VisuHydro", "Visualisation données Hydrologiques \n"
                                                            "Projet SETIER \n"
                                                            "Valérie QUATELA - Laboratoire IGE - CNRS \n"
                                                            "Licensed under the GNU General Public License v3.0");
}


MainWindow::~MainWindow()
{
    delete ui;
}

