/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierPCT.cpp
# -------------------------------------------------------
#  15/11/2022     |   Valérie QUATELA    | Version 0.5 |
##########################################################*/

#include "visuSetierPCT.h"
#include "visuSetierPC.h"
#include "ui_visuSetierPCT.h"
#include <qwt_scale_draw.h>
#include <QDateTime>
#include <qwt_date.h>
#include <qwt_plot_zoomer.h>
#include <math.h>
#include <QDebug>
#include <QFile>

VisuSetierPCT::VisuSetierPCT(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VisuSetierPCT)
{
    ui->setupUi(this);
    init();
}

void VisuSetierPCT::init()
{

    choixDate=0;

    plotTemp1 = new QwtPlot;                 // plot Qwt date
    courbeC1 = new QwtPlotCurve;              // courbe temperature capteurPH
    courbeC2 = new QwtPlotCurve;              // courbe temperature capteurRedox
    courbeC3 = new QwtPlotCurve;              // courbe temperature capteurO2
    courbeC4 = new QwtPlotCurve;              // courbe temperature capteurConductivite

    infoFicSetierPC = new InfoFichierSetierPC; // détail fichier

    ui->verticalLayout_courbe->addWidget(plotTemp1);
    connect(ui->pushButton_detail, SIGNAL(clicked(bool)),this,SLOT(ouvrirInfoFichier()));

}

void VisuSetierPCT::setNomfichierDATA(QString s)
{
    ui->lineEdit_nomFic->setText(s);
    this->nomfichierDATA=s;
}

void VisuSetierPCT::setChoixDate(int cd)
{
    this->choixDate=cd;
}

void VisuSetierPCT::setCadence(QString cadence)
{
    this->cadence=cadence.toDouble();
    ui->lineEdit_cadence->setText(cadence);
}

void VisuSetierPCT::setdateDebut(QDateTime DateTime)
{
    this->DT=DateTime;
}

void VisuSetierPCT::traiterFichierSetierPCT()
{
    qDebug() << "traitement fichier PC temperature"<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY1;          // axe energie capteur1
    QVector <double> axeY2;          // axe energie capteur2
    QVector <double> axeY3;          // axe energie capteur3
    QVector <double> axeY4;          // axe energie capteur4

    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Profondeur canal, Distance capteur/fond canal, Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    for (int i = 0; i< 30 ; i++) {flux >> mot;}
                    setCadence(mot);
                  }

             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;          //  date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double T;               // temperature
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Date", "Temperature (C°)" , axeX,axeY1,axeY2,axeY3,axeY4);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;

                           // dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm");
                            dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                            DTD = double(dateTime.toSecsSinceEpoch());

                            axeX.push_back(DTD);

                            // lecture de la ligne jusqu'à mot4=temperature capteurPH
                            for (int i = 0; i< 4 ; i++) {flux >> mot;}
                            T=mot.toDouble();
                            axeY1.push_back(T);
                            //qDebug() << "T1= "<<mot<< Qt::endl;

                            for (int i = 0; i< 6 ; i++) {flux >> mot;}
                            T=mot.toDouble();
                            axeY2.push_back(T);
                            //qDebug() << "T2= "<<mot<< Qt::endl;

                            for (int i = 0; i< 6 ; i++) {flux >> mot;}
                            T=mot.toDouble();
                            axeY3.push_back(T);
                           // qDebug() << "T3= "<<mot<< Qt::endl;

                            for (int i = 0; i< 6 ; i++) {flux >> mot;}
                            T=mot.toDouble();
                            axeY4.push_back(T);
                            //qDebug() << "T4= "<<mot<< Qt::endl;

                            for (int i = 0; i< 7 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1 = date/heure

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
     }


}

void VisuSetierPCT::remplirtabX(QVector <double> tabX)
{
    this->dataX=tabX;
}

void VisuSetierPCT::remplirtabY1(QVector <double> tabY)
{
    this->dataY1=tabY;
}

void VisuSetierPCT::remplirtabY2(QVector <double> tabY)
{
    this->dataY2=tabY;
}

void VisuSetierPCT::remplirtabY3(QVector <double> tabY)
{
    this->dataY3=tabY;
}

void VisuSetierPCT::remplirtabY4(QVector <double> tabY)
{
    this->dataY4=tabY;
}


void VisuSetierPCT::afficher(QString labelX, QString labelY)
{
        // configuration repere

        this->plotTemp1->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
        this->plotTemp1->setCanvasBackground( Qt::white ); // couleur de fond
        this->plotTemp1->setAxisTitle(QwtPlot::xBottom, "Date");
        this->plotTemp1->setAxisTitle(QwtPlot::yLeft, labelY);

        //  conversion des doubles en dates jj/mm/aaaa hh/mm/ss et rotation des labels
        TimeScaleDraw *draw = new TimeScaleDraw();
        //draw->setLabelRotation(-90);

        this->plotTemp1->setAxisScaleDraw(QwtPlot::xBottom, draw);

        plotTemp1->insertLegend(new QwtLegend(), QwtPlot::TopLegend);


        // creation grille
        QwtPlotGrid *grid = new QwtPlotGrid();
        grid->attach(this->plotTemp1);


        // configuration courbes
        this->courbeC1->setTitle("capteur_PH");
        this->courbeC1->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC1->setPen( Qt::blue, 2 );
        this->courbeC1->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point bleu cyan
        this->courbeC1->attach(this->plotTemp1);
        this->courbeC1->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC1->setSamples(this->dataX, this->dataY1);

        this->courbeC2->setTitle("capteur_Redox");
        this->courbeC2->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC2->setPen( Qt::green, 2 );
        this->courbeC2->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point vert
        this->courbeC2->attach(this->plotTemp1);
        this->courbeC2->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC2->setSamples(this->dataX, this->dataY2);

        this->courbeC3->setTitle("capteur_O2");
        this->courbeC3->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC3->setPen( Qt::gray, 2 );
        this->courbeC3->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point jaune
        this->courbeC3->attach(this->plotTemp1);
        this->courbeC3->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC3->setSamples(this->dataX, this->dataY3);

        this->courbeC4->setTitle("capteur_conductivite");
        this->courbeC4->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC4->setPen( Qt::red, 2 );
        this->courbeC4->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point rouge
        this->courbeC4->attach(this->plotTemp1);
        this->courbeC4->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC4->setSamples(this->dataX, this->dataY4);

        this->plotTemp1->replot();
        // affichage
        this->plotTemp1->show();

}

void VisuSetierPCT::afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y1 , QVector <double> Y2 ,QVector <double> Y3 , QVector <double> Y4 )
{
    if(this->choixDate==0) // "date initiale" selectionnée
    {
        double date = X[0];
        QDateTime t1 = QDateTime::fromTime_t((int)date);
        QString dateDebut = t1.toString("yyyy/MM/dd hh:mm:ss");
       // QString dateDebut = t1.toString("yyyy/MM/dd hh:mm");

        ui->lineEdit_date->setText(dateDebut);
        infoFicSetierPC->setdateDebut(dateDebut);

        this->remplirtabX(X);
        this->remplirtabY1(Y1);
        this->remplirtabY2(Y2);
        this->remplirtabY3(Y3);
        this->remplirtabY4(Y4);
        this->afficher(labelX, labelY);
    }
    if((this->choixDate==1) | (this->choixDate==2)) // 1 = "saisir date" selectionnée , 2 = "date fichier WIFI" selectionnée
    {
        double date;
        double DTD = double(this->DT.toSecsSinceEpoch());
        QDateTime QDT;

        // nouveaux vecteurs en fonction de date de début de courbe
        QVector <double> X1;
        QVector <double> YB1;
        QVector <double> YB2;
        QVector <double> YB3;
        QVector <double> YB4;


   //chercher dans X date de début de courbe immediatement superieur à dateDebut
        for (int i = 0; i < X.size(); ++i)
        {
            date = X[i];

            if (date > DTD )
            {

                for (int j = i ; j < X.size(); ++j)
                {
                    X1.push_back(X[j]);
                }

                for (int k = i ; k < Y1.size(); k++) //capteur1
                {
                    YB1.push_back(Y1[k]);
                }


                for (int k = i ; k < Y2.size(); k++) //capteur2
                {
                    YB2.push_back(Y2[k]);
                }

                for (int k = i ; k < Y3.size(); k++) //capteur3
                {
                    YB3.push_back(Y3[k]);
                }

                for (int k = i ; k < Y4.size(); k++) //capteur4
                {
                    YB4.push_back(Y4[k]);
                }

                i=X.size();
            }
       }


          //QString dateDebut = this->DT.toString("yyyy/MM/dd hh:mm");
          QString dateDebut = this->DT.toString("yyyy/MM/dd hh:mm:ss");
          ui->lineEdit_date->setText(dateDebut);
          infoFicSetierPC->setdateDebut(dateDebut);


          this->remplirtabX(X1);
          this->remplirtabY1(YB1); //capteur1
          this->remplirtabY2(YB2); //capteur2
          this->remplirtabY3(YB3); //capteur3
          this->remplirtabY4(YB4); //capteur4

          this->afficher(labelX, labelY);

    }


}

void VisuSetierPCT::ouvrirInfoFichier()
{
    this->infoFicSetierPC->setNomFichier(this->nomfichierDATA);
    this->infoFicSetierPC->show();
}



VisuSetierPCT::~VisuSetierPCT()
{
    delete ui;
}
