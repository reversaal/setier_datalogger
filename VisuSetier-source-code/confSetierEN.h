/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : confSetierEN.h
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#ifndef CONFSETIEREN_H
#define CONFSETIEREN_H

#include <QDialog>
#include <QDateTime>
#include <iostream>

using namespace std;

namespace Ui {
class ConfSetierEN;
}

class ConfSetierEN : public QDialog
{
    Q_OBJECT

public:
    explicit ConfSetierEN(QWidget *parent = nullptr);
    ~ConfSetierEN();

public slots:
    void ouvrirFichierDATA();
    void ouvrirFichierWIFI();
    void modifchoixDate(int cd);
    void ouvrirVisuSetierEN();

private:
    Ui::ConfSetierEN *ui;

    QString  nomfichierDATA;    // nom du fichier DATA
    QDateTime DT;               // date de debut de courbe
    void init();

};

#endif // CONFSETIEREN_H
