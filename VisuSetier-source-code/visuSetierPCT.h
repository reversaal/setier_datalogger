/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierPCT.h
# -------------------------------------------------------
#  15/11/2022     |   Valérie QUATELA    | Version 0.5 |
##########################################################*/

#ifndef VISUSETIERPCT_H
#define VISUSETIERPCT_H

#include <QDialog>
#include <iostream>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_date_scale_draw.h>
#include "infofichierSetierPC.h"

namespace Ui {
class VisuSetierPCT;
}

class VisuSetierPCT : public QDialog
{
    Q_OBJECT

public:
    explicit VisuSetierPCT(QWidget *parent = nullptr);
    ~VisuSetierPCT();
    void init();
    void setNomfichierDATA(QString s);
    void setChoixDate(int choixDate);
    void setdateDebut(QDateTime DateTime);
    void setCadence(QString cadence);
    void traiterFichierSetierPCT();
    void remplirtabX(QVector <double> tabX);
    void remplirtabY1(QVector <double> tabY);
    void remplirtabY2(QVector <double> tabY);
    void remplirtabY3(QVector <double> tabY);
    void remplirtabY4(QVector <double> tabY);
    void afficher(QString labelX, QString labelY);

private:
    Ui::VisuSetierPCT *ui;

    QString  nomfichierDATA;
    double cadence;             // cadence des mesures
    int choixDate;

    QVector <double> dataX;      // tableaux de point X = date/heure
    QVector <double> dataY1;     // tableaux de point Y = valeur temperature capteurPH
    QVector <double> dataY2;     // tableaux de point Y = valeur temperature capteurRedox
    QVector <double> dataY3;     // tableaux de point Y = valeur temperature capteurO2
    QVector <double> dataY4;     // tableaux de point Y = valeur temperature capteurConductivite

    QDateTime DT;                 // date de debut de courbe
    QwtPlot *plotTemp1;            // plot Qwt
    QwtPlotCurve *courbeC1;       // courbe temperature capteurPH
    QwtPlotCurve *courbeC2;       // courbe temperature capteurRedox
    QwtPlotCurve *courbeC3;       // courbe temperature capteurO2
    QwtPlotCurve *courbeC4;       // courbe temperature capteurConductivite

    InfoFichierSetierPC *infoFicSetierPC; // fenetre information

    void afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y1 , QVector <double> Y2 ,QVector <double> Y3 , QVector <double> Y4);

private slots:
    void ouvrirInfoFichier();
};

//class TimeScaleDraw: public QwtScaleDraw
//{

//    virtual QwtText label(double v) const
//    {
//        QDateTime t = QDateTime::fromTime_t((int)v);	//cf fromTime_t
//        return t.toString("dd/MM/yyyy\nhh:mm:ss");
//    }

//};

#endif // VISUSETIERPCT_H
