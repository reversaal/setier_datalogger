/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : confSetierPC.h
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#ifndef CONFSETIERPC_H
#define CONFSETIERPC_H

#include <QDialog>
#include <QDateTime>

using namespace std;


namespace Ui {
class ConfSetierPC;
}

class ConfSetierPC : public QDialog
{
    Q_OBJECT

public:
    explicit ConfSetierPC(QWidget *parent = nullptr);
    ~ConfSetierPC();

public slots:
    void ouvrirFichierDATA();
    void ouvrirFichierETALON();
    void ouvrirFichierWIFI();
    void modifchoixDate(int cd);
    void ouvrirVisuSetierPC();

private:
    Ui::ConfSetierPC *ui;

    QString  nomfichierDATA;    // nom du fichier DATA
    QString  nomfichierETALON;    // nom du fichier ETALON
    QDateTime DT;               // date de debut de courbe
    void init();

};

#endif // CONFSETIERPC_H
