/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierEN.h
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/


#ifndef VISUSETIEREN_H
#define VISUSETIEREN_H

#include <QDialog>
#include <iostream>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_date_scale_draw.h>
#include "infofichierSetierEN.h"
#include <QScrollArea>
#include <qwt_text.h>

namespace Ui {
class VisuSetierEN;
}

class VisuSetierEN : public QDialog
{
    Q_OBJECT

public:
    explicit VisuSetierEN(QWidget *parent = nullptr);
    ~VisuSetierEN();
    void init();
    void setNomfichierDATA(QString s);
    void setChoixDate(int choixDate);
    void setdateDebut(QDateTime DateTime);
    void setCapteur1(QString c);
    void setCapteur2(QString c);
    void setCapteur3(QString c);
    void setCapteur4(QString c);
    void setCadence(QString cadence);
    void traiterFichierSetierEN();
    void remplirtabX(QVector <double> tabX);
    void remplirtabY1(QVector <double> tabY);
    void remplirtabY2(QVector <double> tabY);
    void remplirtabY3(QVector <double> tabY);
    void remplirtabY4(QVector <double> tabY);
    void afficher(QString labelX, QString labelY);


private:
    Ui::VisuSetierEN *ui;

    QString  nomfichierDATA;
    double cadence;             // cadence des mesures
    bool C1=false,C2=false,C3=false,C4=false;           // présence capteurs 1 2 3 4
    int choixDate;

    QVector <double> dataX;      // tableaux de point X = date/heure
    QVector <double> dataY1;     // tableaux de point Y = valeur energie capteur1
    QVector <double> dataY2;     // tableaux de point Y = valeur energie capteur2
    QVector <double> dataY3;     // tableaux de point Y = valeur energie capteur3
    QVector <double> dataY4;     // tableaux de point Y = valeur energie capteur4

    QDateTime DT;                 // date de debut de courbe
    QwtPlot *plotTemp1;            // plot Qwt
    QwtPlot *plotTemp2;            // plot Qwt
    QwtPlot *plotTemp3;            // plot Qwt
    QwtPlot *plotTemp4;            // plot Qwt
    QwtPlotCurve *courbeC1;       // courbe energie capteur1
    QwtPlotCurve *courbeC2;       // courbe energie capteur2
    QwtPlotCurve *courbeC3;       // courbe energie capteur3
    QwtPlotCurve *courbeC4;       // courbe energie capteur4
    QScrollArea *scrollArea;

    InfoFichierSetierEN *infoFicSetierEN; // fenetre information

    void afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y1 , QVector <double> Y2 ,QVector <double> Y3 , QVector <double> Y4);


private slots:
    void ouvrirInfoFichier();
};

class TimeScaleDraw: public QwtScaleDraw
{

    virtual QwtText label(double v) const
    {
        QDateTime t = QDateTime::fromTime_t((int)v);	//cf fromTime_t
        return t.toString("dd/MM/yyyy\nhh:mm:ss");
    }
};

#endif // VISUSETIEREN_H
