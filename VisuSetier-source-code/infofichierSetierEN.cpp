/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : infofichierSetierEN.cpp
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/


#include "infofichierSetierEN.h"
#include "ui_infofichierSetierEN.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDateTime>


InfoFichierSetierEN::InfoFichierSetierEN(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoFichierSetierEN)
{
    ui->setupUi(this);
    connect(ui->pushButton_voirFic,SIGNAL(clicked(bool)),this,SLOT(voirFichier()));
}

void InfoFichierSetierEN::setNomFichier(QString F)
{
    ui->lineEdit_nomFic->setText(F);
    this->NomFic=F;
}


void InfoFichierSetierEN::setdateDebut(QString Db)
{
    ui->lineEdit_date->setText(Db);
    this->dateDebut=Db;
    //qDebug() << "InfoFichierSetierHE::setdateDebut = "<<Db << Qt::endl;

}


void InfoFichierSetierEN::voirFichier()
{

    QString texte;
    QFile fic(this->NomFic);  //Ouverture du fichier "nomfichier" en lecture

    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      QTextStream flux(&fic);
        texte = flux.readAll();
        ui->textEdit_fic->setText(texte);

        //  affichage date debut dans fichier

        QTextCharFormat format;
        format.setBackground(Qt::darkGreen);
        format.setFontWeight(QFont::Bold);

        QTextDocument *doc = ui->textEdit_fic->document();
        QTextCursor cursor;

        //qDebug() << "this->dateDebut = "<<this->dateDebut << Qt::endl;

        cursor= doc->find(this->dateDebut,0);
      //  qDebug() << "cursor = "<<cursor.position() << Qt::endl;

        cursor.setCharFormat(format);

     }

   else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }



}

InfoFichierSetierEN::~InfoFichierSetierEN()
{
    delete ui;
}
