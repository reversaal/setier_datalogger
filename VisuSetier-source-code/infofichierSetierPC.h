/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : infofichierSetierPC.h
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/


#ifndef INFOFICHIERSETIERPC_H
#define INFOFICHIERSETIERPC_H

#include <QDialog>
#include <iostream>
using namespace std;

namespace Ui {
class InfoFichierSetierPC;
}

class InfoFichierSetierPC : public QDialog
{
    Q_OBJECT

public:
    explicit InfoFichierSetierPC(QWidget *parent = nullptr);
    void setNomFichier(QString F);
    //void setNBdateInvalide(int N);
    //void setDateInvalide(QVector <QString> DI);
    //void dateInvalide(QString D);
    void setdateDebut(QString dateDeb);

    ~InfoFichierSetierPC();

private:
    Ui::InfoFichierSetierPC *ui;
    QString NomFic;
    int NBdateInvalide;
    QVector<QString> DInv;
    QString dateDebut;          // date de debut de courbe

private slots:
    void voirFichier();
};

#endif // INFOFICHIERSETIERPC_H
