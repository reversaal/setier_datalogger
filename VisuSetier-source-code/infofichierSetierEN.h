/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : infofichierSetierEN.h
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#ifndef INFOFICHIERSETIEREN_H
#define INFOFICHIERSETIEREN_H

#include <QDialog>
#include <iostream>
using namespace std;

namespace Ui {
class InfoFichierSetierEN;
}

class InfoFichierSetierEN : public QDialog
{
    Q_OBJECT

public:
    explicit InfoFichierSetierEN(QWidget *parent = nullptr);
    void setNomFichier(QString F);
    void setdateDebut(QString dateDeb);


    ~InfoFichierSetierEN();

private:
    Ui::InfoFichierSetierEN *ui;
    QString NomFic;
    QString dateDebut;          // date de debut de courbe

private slots:
    void voirFichier();

};

#endif // INFOFICHIERSETIEREN_H
