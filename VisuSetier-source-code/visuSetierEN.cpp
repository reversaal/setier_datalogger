/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierEN.cpp
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#include "visuSetierEN.h"
#include "ui_visuSetierEN.h"

#include <qwt_scale_draw.h>
#include <QDateTime>
#include <qwt_date.h>
#include <qwt_plot_zoomer.h>
#include <math.h>

#include <QDebug>
#include <QFile>


VisuSetierEN::VisuSetierEN(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VisuSetierEN)
{
    ui->setupUi(this);
    init();
}

void VisuSetierEN::init()
{
/*
    scrollArea= new QScrollArea; // ascensseur fenetre
    scrollArea->setWidget(this);
    scrollArea->resize(this->width(), this->height());
    scrollArea->show();
//    this->showFullScreen();
*/

    choixDate=0;

    infoFicSetierEN = new InfoFichierSetierEN; // détail fichier

    connect(ui->pushButton_detail, SIGNAL(clicked(bool)),this,SLOT(ouvrirInfoFichier()));

}

void VisuSetierEN::setNomfichierDATA(QString s)
{
    ui->lineEdit_nomFic->setText(s);
    this->nomfichierDATA=s;
}

void VisuSetierEN::setChoixDate(int cd)
{
    this->choixDate=cd;
}

void VisuSetierEN::setCapteur1(QString c)
{
    if (c!="999.00"){this->C1=true;}
}

void VisuSetierEN::setCapteur2(QString c)
{
    if (c!="999.00"){this->C2=true;}
}

void VisuSetierEN::setCapteur3(QString c)
{
    if (c!="999.00"){this->C3=true;}
}

void VisuSetierEN::setCapteur4(QString c)
{
    if (c!="999.00"){this->C4=true;}
}


void VisuSetierEN::setCadence(QString cadence)
{
    this->cadence=cadence.toDouble();
    ui->lineEdit_cadence->setText(cadence);
}

void VisuSetierEN::setdateDebut(QDateTime DateTime)
{
    this->DT=DateTime;
}

void VisuSetierEN::traiterFichierSetierEN()
{
    qDebug() << "traitement fichier "<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY1;          // axe energie capteur1
    QVector <double> axeY2;          // axe energie capteur2
    QVector <double> axeY3;          // axe energie capteur3
    QVector <double> axeY4;          // axe energie capteur4

    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Profondeur canal, Distance capteur/fond canal, Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;
                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    // lecture du mot4=capteur1 , mot5=capteur2 , mot6=capteur3 , mot7=capteur4, mot8=cadence

                        for (int i = 0; i< 6 ; i++) {flux >> mot;}
                        setCapteur1(mot);

                        for (int i = 0; i< 2 ; i++) {flux >> mot;}
                        setCapteur2(mot);

                        for (int i = 0; i< 2 ; i++) {flux >> mot;}
                        setCapteur3(mot);

                        for (int i = 0; i< 2 ; i++) {flux >> mot;}
                        setCapteur4(mot);

                        for (int i = 0; i< 2 ; i++) {flux >> mot;}
                        setCadence(mot);

                        qDebug() << "capteur1 = "<<this->C1<< Qt::endl;
                        qDebug() << "capteur2 = "<<this->C2<< Qt::endl;
                        qDebug() << "capteur3 = "<<this->C3<< Qt::endl;
                        qDebug() << "capteur4 = "<<this->C4<< Qt::endl;
                        qDebug() << "cadence = "<<this->cadence<< Qt::endl;

                  }
             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;          //  date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double E;               // hauteur d'eau
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Capteur 1", "Courant (A)" , axeX,axeY1,axeY2,axeY3,axeY4);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;


                            dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                            DTD = double(dateTime.toSecsSinceEpoch());

                            axeX.push_back(DTD);

                            // lecture de la ligne jusqu'à mot4=capteur1
                            for (int i = 0; i< 4 ; i++) {flux >> mot;}
                            if (C1==true)
                             {
                                 E=mot.toDouble();
                                 axeY1.push_back(E);
                                 //qDebug() << "C1= "<<mot<< Qt::endl;
                             }

                             for (int i = 0; i< 2 ; i++) {flux >> mot;}
                             if (C2==true)
                              {
                                  E=mot.toDouble();
                                  axeY2.push_back(E);
                                  //qDebug() << "C2= "<<mot<< Qt::endl;
                              }

                              for (int i = 0; i< 2 ; i++) {flux >> mot;}
                              if (C3==true)
                               {
                                  E=mot.toDouble();
                                  axeY3.push_back(E);
                                  //qDebug() << "C3= "<<mot<< Qt::endl;
                               }

                              for (int i = 0; i< 2 ; i++) {flux >> mot;}
                              if (C4==true)
                              {
                                  E=mot.toDouble();
                                  axeY4.push_back(E);
                                  //qDebug() << "C4= "<<mot<< Qt::endl;
                              }

                              for (int i = 0; i< 3 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1 = date/heure

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
     }


}

void VisuSetierEN::remplirtabX(QVector <double> tabX)
{
    this->dataX=tabX;
}

void VisuSetierEN::remplirtabY1(QVector <double> tabY)
{
    this->dataY1=tabY;
}

void VisuSetierEN::remplirtabY2(QVector <double> tabY)
{
    this->dataY2=tabY;
}

void VisuSetierEN::remplirtabY3(QVector <double> tabY)
{
    this->dataY3=tabY;
}

void VisuSetierEN::remplirtabY4(QVector <double> tabY)
{
    this->dataY4=tabY;
}


void VisuSetierEN::afficher(QString labelX, QString labelY)
{

    //  conversion des doubles en dates jj/mm/aaaa hh/mm/ss et rotation des labels
    TimeScaleDraw *draw = new TimeScaleDraw();
    //draw->setLabelRotation(-90);


    if(C1==true)    // si presence capteur 1
    {

        plotTemp1 = new QwtPlot;         // plot Qwt
        courbeC1 = new QwtPlotCurve;      // courbe energie capteur1
        ui->verticalLayout_courbe->addWidget(plotTemp1);


        // configuration repere

        this->plotTemp1->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
        this->plotTemp1->setCanvasBackground( Qt::white ); // couleur de fond
        this->plotTemp1->setTitle( "Capteur 1");
        this->plotTemp1->setAxisTitle(QwtPlot::yLeft, labelY);

        this->plotTemp1->setAxisScaleDraw(QwtPlot::xBottom, draw);


        // creation grille
        QwtPlotGrid *grid = new QwtPlotGrid();
        grid->attach(this->plotTemp1);


        // configuration courbe

        this->courbeC1->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC1->setPen( Qt::blue, 2 );
        this->courbeC1->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point bleu cyan
        this->courbeC1->attach(this->plotTemp1);
        this->courbeC1->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC1->setSamples(this->dataX, this->dataY1);

        this->plotTemp1->replot();
        // affichage
        this->plotTemp1->show();
    }


    if(C2==true)    // idem capteur 2
    {
        plotTemp2 = new QwtPlot;         // plot Qwt
        courbeC2 = new QwtPlotCurve;      // courbe energie capteur2
        ui->verticalLayout_courbe->addWidget(plotTemp2);

        this->plotTemp2->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
        this->plotTemp2->setCanvasBackground( Qt::white ); // couleur de fond
        this->plotTemp2->setTitle( "Capteur 2");
        this->plotTemp2->setAxisTitle(QwtPlot::yLeft, labelY);

        this->plotTemp2->setAxisScaleDraw(QwtPlot::xBottom, draw);

        QwtPlotGrid *grid2 = new QwtPlotGrid();
        grid2->attach(this->plotTemp2);

        this->courbeC2->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC2->setPen( Qt::green, 2 );
        this->courbeC2->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point vert
        this->courbeC2->attach(this->plotTemp2);
        this->courbeC2->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC2->setSamples(this->dataX, this->dataY2);

        this->plotTemp2->replot();
        this->plotTemp2->show();
    }

    if(C3==true)    // idem capteur 3
    {

        plotTemp3 = new QwtPlot;         // plot Qwt
        courbeC3 = new QwtPlotCurve;      // courbe energie capteur3
        ui->verticalLayout_courbe->addWidget(plotTemp3);

        this->plotTemp3->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
        this->plotTemp3->setCanvasBackground( Qt::white ); // couleur de fond
        this->plotTemp3->setTitle( "Capteur 3");
        this->plotTemp3->setAxisTitle(QwtPlot::yLeft, labelY);

        this->plotTemp3->setAxisScaleDraw(QwtPlot::xBottom, draw);

        QwtPlotGrid *grid3 = new QwtPlotGrid();
        grid3->attach(this->plotTemp3);

        this->courbeC3->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC3->setPen( Qt::gray, 2 );
        this->courbeC3->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point jaune
        this->courbeC3->attach(this->plotTemp3);
        this->courbeC3->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC3->setSamples(this->dataX, this->dataY3);

        this->plotTemp3->replot();
        this->plotTemp3->show();

     }


    if(C4==true)    // idem capteur 4
    {

        plotTemp4 = new QwtPlot;         // plot Qwt
        courbeC4 = new QwtPlotCurve;      // courbe energie capteur4
        ui->verticalLayout_courbe->addWidget(plotTemp4);


        this->plotTemp4->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
        this->plotTemp4->setCanvasBackground( Qt::white ); // couleur de fond
        this->plotTemp4->setTitle( "Capteur 4");
        this->plotTemp4->setAxisTitle(QwtPlot::yLeft, labelY);

        this->plotTemp4->setAxisScaleDraw(QwtPlot::xBottom, draw);

        QwtPlotGrid *grid4 = new QwtPlotGrid();
        grid4->attach(this->plotTemp4);

        this->courbeC4->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        this->courbeC4->setPen( Qt::red, 2 );
        this->courbeC4->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point rouge
        this->courbeC4->attach(this->plotTemp4);
        this->courbeC4->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeC4->setSamples(this->dataX, this->dataY4);

        this->plotTemp4->replot();
        this->plotTemp4->show();
    }
}

void VisuSetierEN::afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y1 , QVector <double> Y2 ,QVector <double> Y3 , QVector <double> Y4 )
{
    if(this->choixDate==0) // "date initiale" selectionnée
    {
        double date = X[0];
        QDateTime t1 = QDateTime::fromTime_t((int)date);
        QString dateDebut = t1.toString("yyyy/MM/dd hh:mm:ss");

        ui->lineEdit_date->setText(dateDebut);
        infoFicSetierEN->setdateDebut(dateDebut);

        this->remplirtabX(X);
        this->remplirtabY1(Y1);
        this->remplirtabY2(Y2);
        this->remplirtabY3(Y3);
        this->remplirtabY4(Y4);
        this->afficher(labelX, labelY);
    }
    if((this->choixDate==1) | (this->choixDate==2)) // 1 = "saisir date" selectionnée , 2 = "date fichier WIFI" selectionnée
    {
        double date;
        double DTD = double(this->DT.toSecsSinceEpoch());
        QDateTime QDT;

        // nouveaux vecteurs en fonction de date de début de courbe
        QVector <double> X1;
        QVector <double> Y1B;
        QVector <double> Y2B;
        QVector <double> Y3B;
        QVector <double> Y4B;


   //chercher dans X date de début de courbe immediatement superieur à dateDebut
        for (int i = 0; i < X.size(); ++i)
        {
            date = X[i];

            if (date > DTD )
            {

                for (int j = i ; j < X.size(); ++j)
                {
                    X1.push_back(X[j]);
                }

                for (int k = i ; k < Y1.size(); k++) //capteur1
                {
                    Y1B.push_back(Y1[k]);
                }

                for (int k = i ; k < Y2.size(); k++) //capteur2
                {
                    Y2B.push_back(Y2[k]);
                }

                for (int k = i ; k < Y3.size(); k++) //capteur3
                {
                    Y3B.push_back(Y3[k]);
                }

                for (int k = i ; k < Y4.size(); k++) //capteur4
                {
                    Y4B.push_back(Y4[k]);
                }

                i=X.size();
            }
       }


          QString dateDebut = this->DT.toString("yyyy/MM/dd hh:mm:ss");
          ui->lineEdit_date->setText(dateDebut);
          infoFicSetierEN->setdateDebut(dateDebut);


          this->remplirtabX(X1);
          this->remplirtabY1(Y1B); //capteur1
          this->remplirtabY2(Y2B); //capteur2
          this->remplirtabY3(Y3B); //capteur3
          this->remplirtabY4(Y4B); //capteur4

          this->afficher(labelX, labelY);

    }


}

void VisuSetierEN::ouvrirInfoFichier()
{
    this->infoFicSetierEN->setNomFichier(this->nomfichierDATA);
    this->infoFicSetierEN->show();
}

VisuSetierEN::~VisuSetierEN()
{
    delete ui;
}
