/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : confSetierHE.cpp
# -------------------------------------------------------
#  13/05/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#include "confSetierHE.h"
#include "visuSetierHE.h"
#include "ui_confSetierHE.h"

#include <QDebug>
#include <QFileDialog>

ConfSetierHE::ConfSetierHE(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfSetierHE)
{
    ui->setupUi(this);
    init();
}


void ConfSetierHE::init()
{

    ui->buttonGroupChoixDate->setId(ui->radioButton_dateinitiale,0);
    ui->buttonGroupChoixDate->setId(ui->radioButton_saisirdate,1);
    ui->buttonGroupChoixDate->setId(ui->radioButton_wifi,2);

    connect(ui->toolButtonDATA,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierDATA()));
    connect(ui->toolButtonWIFI,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierWIFI()));
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(ouvrirVisuSetierHE()));
    connect(ui->typeSBox,SIGNAL(valueChanged(int)),this,SLOT(updateDEBIT(int)));

}


void ConfSetierHE::ouvrirFichierDATA()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditDATA->setText(nomfichier);
            this->nomfichierDATA=nomfichier;
            qDebug() << "nomfichierDATA = "<<nomfichierDATA<< Qt::endl;
        }

}

void ConfSetierHE::ouvrirFichierWIFI()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditWIFI->setText(nomfichier);
            qDebug() << "nomfichierWIFI = "<<nomfichier<< Qt::endl;

            // recuperer date dans fichier WIFI et remlir la liste graphique des dates
            QFile fic(nomfichier);  //Ouverture du fichier WIFI en lecture

            if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
            {

                QTextStream flux(&fic);
                QString mot;            // mot courant
                QString ligne;          // ligne date
                bool tagStart = false;  // presence tag debut des data
                bool tagEnd = false;    // presence tag fin des data

                while(! flux.atEnd())
                        {
                            flux >> mot;
                            if (mot == "START_DATA")        //début du traitement des valeurs
                            {
                                tagStart = true;
                                while(! flux.atEnd())
                                {
                                    flux >> mot;

                                   if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                                     {
                                        tagEnd = true;
                                        qDebug() << "fin de traitement"<< Qt::endl;
                                        return;
                                   }
                                   else
                                   {
                                       ligne=mot;
                                       flux >> mot;
                                       ligne+=" ";
                                       ligne+=mot;
                                       ui->comboBox->addItem(ligne);
                                   }
                                }
                            }
                }
                qDebug() << "fin de fichier"<< Qt::endl;
                qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;


            }
            else
            {
                cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
            }


        }
}

void ConfSetierHE::modifchoixDate(int cd)
{
    if(cd==0)        // cas courbe complete
    {

        qDebug() << "DT choix 0 "<< Qt::endl;

    }

    if(cd==1)        // cas saisir date
    {
        DT= ui->dateTimeEdit->dateTime() ;
        qDebug() << "DT choix 1 = "<<DT<< Qt::endl;

    }

    if(cd==2)        // cas date fichier WIFI
    {
        // recuperer choix STRING dans liste et transformer en DateTime
        QString datetime_string = ui->comboBox->currentText();
        QDateTime dateT = QDateTime::fromString(datetime_string,"yyyy/MM/dd hh:mm");
        DT= dateT ;
        qDebug() << "DT choix 2 = "<<DT<< Qt::endl;

     }
 }

void ConfSetierHE::ouvrirVisuSetierHE()
{
  VisuSetierHE *visuSetierHE = new VisuSetierHE;
  visuSetierHE->setNomfichierDATA(this->nomfichierDATA);
  visuSetierHE->setChoixDate(ui->buttonGroupChoixDate->checkedId());
  this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
  visuSetierHE->setdateDebut(this->DT);
  visuSetierHE->setChoixProf(ui->profondeurSBox->value());
  visuSetierHE->setChoixDist(ui->distanceSBox->value());
  visuSetierHE->setType(ui->typeSBox->value());

  qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;

  visuSetierHE->traiterFichierSetierHE();

  visuSetierHE->show();

}

void ConfSetierHE::updateDEBIT(int TC)   // equation debit fonction du Type de Canal
{
    if (TC==0) {this->ui->lineEditDebit->setText(" ");}
    if (TC==1) {this->ui->lineEditDebit->setText(" Q = -0.19xh + 672.7xh² - 734xh³ + 11400xh⁴ ");}
    if (TC==2) {this->ui->lineEditDebit->setText(" Q = -0.3311xh + 735.1xh² + 80.7xh³ + 6210xh⁴ ");}
    if (TC==3) {this->ui->lineEditDebit->setText(" Q = -0.58461xh + 1156.085xh² - 1125xh³ + 6550xh⁴ ");}
    if (TC==4) {this->ui->lineEditDebit->setText(" Q = -1.535xh + 1537.5xh² - 834xh³ + 2820xh⁴");}
    if (TC==5) {this->ui->lineEditDebit->setText(" Q = -3.171xh + 2056xh² - 470xh³ + 1050xh⁴");}
    if (TC==6) {this->ui->lineEditDebit->setText(" Q = -4.423xh + 2661.66xh² - 1360xh³ + 1690xh⁴");}
    if (TC==7) {this->ui->lineEditDebit->setText(" Q = -7.223xh + 2873.2xh² - 766xh³ + 770xh⁴");}



}


ConfSetierHE::~ConfSetierHE()
{
    delete ui;
}
