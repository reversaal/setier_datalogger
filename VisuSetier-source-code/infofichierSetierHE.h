/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : infofichierSetierHE.h
# -------------------------------------------------------
#  13/05/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#ifndef INFOFICHIERSETIERHE_H
#define INFOFICHIERSETIERHE_H

#include <QDialog>
#include <iostream>
using namespace std;

namespace Ui {
class InfoFichierSetierHE;
}

class InfoFichierSetierHE : public QDialog
{
    Q_OBJECT

public:
    explicit InfoFichierSetierHE(QWidget *parent = nullptr);
    void setNomFichier(QString F);
    void setNBdateInvalide(int N);
    void setDateInvalide(QVector <QString> DI);
    void dateInvalide(QString D);
    void setdateDebut(QString dateDeb);

    ~InfoFichierSetierHE();

private:
    Ui::InfoFichierSetierHE *ui;
    QString NomFic;
    int NBdateInvalide;
    QVector<QString> DInv;
    QString dateDebut;          // date de debut de courbe

private slots:
    void voirFichier();
};

#endif // INFOFICHIERSETIERHE_H
