/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierHE.cpp
# -------------------------------------------------------
#  13/05/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#include "visuSetierHE.h"
#include "ui_visuSetierHE.h"
#include <qwt_scale_draw.h>
#include <QDateTime>
#include <qwt_date.h>
#include <qwt_plot_zoomer.h>
#include <math.h>

#include <QDebug>
#include <QFile>

VisuSetierHE::VisuSetierHE(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VisuSetierHE)
{
    ui->setupUi(this);
    init();
}

void VisuSetierHE::init()
{
    choixDate=0;

    plotTemp = new QwtPlot;         // plot Qwt
    courbe = new QwtPlotCurve;      // courbe
    courbeDI = new QwtPlotCurve;    // courbe date invalide
    courbeDA = new QwtPlotCurve;    // courbe date Alerte
    courbeQ = new QwtPlotCurve;     // courbe debit

    infoFicSetierHE = new InfoFichierSetierHE; // date invalide

    ui->verticalLayout_courbe->addWidget(plotTemp);
    connect(ui->pushButton_detail, SIGNAL(clicked(bool)),this,SLOT(ouvrirInfoFichier()));

    ui->lineEdit_rouge->setStyleSheet("QLineEdit {background-color: red;}");    //rectangle rouge
    ui->lineEdit_jaune->setStyleSheet("QLineEdit {background-color: yellow;}"); //rectangle jaune


}

void VisuSetierHE::setNomfichierDATA(QString s)
{
    ui->lineEdit_nomFic->setText(s);
    this->nomfichierDATA=s;
}

void VisuSetierHE::setChoixDate(int cd)
{
    this->choixDate=cd;
}


void VisuSetierHE::setChoixProf(double choixProf)
{
    this->ProfR=choixProf;
    ui->lineEdit_prof_R->setText(QString::number(choixProf));
}

void VisuSetierHE::setImportProf(QString importProf)
{
    this->ProfF=importProf.toDouble();
    ui->lineEdit_prof_F->setText(importProf);
}


void VisuSetierHE::setChoixDist(double choixDist)
{
    this->DistR=choixDist;
    ui->lineEdit_dist_R->setText(QString::number(choixDist));
}

void VisuSetierHE::setImportDist(QString importDist)
{
    this->DistF=importDist.toDouble();
    ui->lineEdit_dist_F->setText(importDist);
}

void VisuSetierHE::setCadence(QString cadence)
{
    this->cadence=cadence.toDouble();
    ui->lineEdit_cadence->setText(cadence);
}

void VisuSetierHE::setdateDebut(QDateTime DateTime)
{
    this->DT=DateTime;
}

void VisuSetierHE::setType(int TC)
{
    this->type= TC;
    ui->lineEdit_type->setText(QString::number(TC));
}

void VisuSetierHE::traiterFichierSetierHE()
{
    qDebug() << "traitement fichier "<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY;          // axe hauteur d'eau
    QVector <QString> dateInv;      // vecteur date invalide
    QVector <QString> dateAlerte;   // vecteur date alerte


    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Profondeur canal, Distance capteur/fond canal, Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    // lecture du mot6=DistF , mot8=ProfF , mot9=cadence

                        for (int i = 0; i< 12 ; i++) {flux >> mot;}
                        setImportDist(mot);

                        for (int i = 0; i< 4 ; i++) {flux >> mot;}
                        setImportProf(mot);

                        for (int i = 0; i< 2 ; i++) {flux >> mot;}
                        setCadence(mot);

                        qDebug() << "DistF = "<<this->DistF<< Qt::endl;
                        qDebug() << "ProfF = "<<this->ProfF<< Qt::endl;
                        qDebug() << "cadence = "<<this->cadence<< Qt::endl;

                  }
             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;          //  date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double h;               // hauteur d'eau
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;
                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Date", "Hauteur d'eau en mm" , axeX,axeY,dateInv,dateAlerte);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;


                               dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                               DTD = double(dateTime.toSecsSinceEpoch());

                               axeX.push_back(DTD);

                           // lecture de la ligne jusqu'à mot7=h
                           for (int i = 0; i< 12 ; i++){flux >> mot;}
                           h = mot.toDouble();

                           // traitement des erreurs sur h
                           if( (h < 0) | (h > this->DistF))
                           {
                               h =0 ;
                               dateInv.push_back(date);
                              // qDebug() << "date INV= "<<date<< Qt::endl;
                           }

                           if( (h > this->ProfF) & (h <= this->DistF))
                           {
                              dateAlerte.push_back(date);
                            //  qDebug() << "date ALERTE= "<<date<< Qt::endl;
                           }



                           axeY.push_back(h);
                           for (int i = 0; i< 5 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
     }


}

void VisuSetierHE::remplirtabX(QVector <double> tabX)
{
    this->dataX=tabX;
}

void VisuSetierHE::remplirtabY(QVector <double> tabY)
{
    this->dataY=tabY;
}

void VisuSetierHE::remplirtabI(QVector <QString> tabI)
{
    this->dataI=tabI;
}

void VisuSetierHE::remplirtabA(QVector <QString> tabA)
{
    this->dataA=tabA;
}


void VisuSetierHE::afficher(QString labelX, QString labelY)
{

    // configuration repere
    this->plotTemp->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
    this->plotTemp->setCanvasBackground( Qt::white ); // couleur de fond
    this->plotTemp->setAxisTitle(QwtPlot::xBottom, labelX);
    this->plotTemp->setAxisTitle(QwtPlot::yLeft, labelY);

    //  conversion des doubles en dates jj/mm/aaaa hh/mm/ss et rotation des labels
    TimeScaleDraw *draw = new TimeScaleDraw();
    //draw->setLabelRotation(-90);
    this->plotTemp->setAxisScaleDraw(QwtPlot::xBottom, draw);


    plotTemp->insertLegend(new QwtLegend(), QwtPlot::TopLegend);


    // creation grille
    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->attach(this->plotTemp );

    // creation zoom
    //QwtPlotZoomer  zoomer = new QwtPlotZoomer(plotTemp.canvas());


    // configuration courbe
    this->courbe->setTitle("hauteur d'eau");
    this->courbe->setLegendAttribute(QwtPlotCurve::LegendNoAttribute, true);
    this->courbe->setItemAttribute(QwtPlotItem::Legend, true);
    this->courbe->setPen( Qt::blue, 2 );
    this->courbe->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point bleu cyan
    this->courbe->attach(this->plotTemp);
    this->courbe->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    this->courbe->setSamples(this->dataX, this->dataY);



    // affichage en rouge des points date erreur : nouvelle courbe
    this->courbeDI->setItemAttribute(QwtPlotItem::Legend, false);
    this->courbeDI->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::red, QPen(Qt::black), QSize(5, 5))); // style point
    this->courbeDI->setStyle(QwtPlotCurve::NoCurve); // point
    for (int i =0 ; i< this->dataY.size(); i++)
    {
        if (this->dataY[i]==0)
        {
            this->dataXDI.push_back(this->dataX[i]);
            this->dataYDI.push_back(this->dataY[i]);
        }
    }
    this->courbeDI->attach(this->plotTemp);
    this->courbeDI->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    this->courbeDI->setSamples(this->dataXDI, this->dataYDI);


    // affichage en bleu cyan des points date Alerte : nouvelle courbe
    this->courbeDA->setItemAttribute(QwtPlotItem::Legend, false);
    this->courbeDA->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::yellow, QPen(Qt::black), QSize(5, 5))); // style point jaune
    this->courbeDA->setStyle(QwtPlotCurve::NoCurve); // point


    for (int i =0 ; i< this->dataY.size(); i++)
    {
        if( (this->dataY[i] > this->ProfF) & (this->dataY[i] <= this->DistF))
        {
            this->dataXDA.push_back(this->dataX[i]);
            this->dataYDA.push_back(this->dataY[i]);
        }
    }


     this->courbeDA->attach(this->plotTemp);
     this->courbeDA->setRenderHint( QwtPlotItem::RenderAntialiased, true );
     this->courbeDA->setSamples(this->dataXDA, this->dataYDA);



    //****************** Courbe debit ******************************************************************

    if (type!=0)
    {
        QString labelY2="Debit en m³/h";
        double Q;
        double QMax;
        double Profondeur;
        int AlerteDebit=0;
        double a,b,c,d;
        double A[7]={ -0.19 , -0.3311 , -0.58461 , -1.535 , -3.171 , -4.423 , -7.223 };
        double B[7]={ 672.7 , 735.1 , 1156.085, 1537.5 , 2056 , 2661.66 , 2573.2 };
        double C[7]={ -734 , 80.7 , -1125 , -834 , -470 , -1360 , -766 };
        double D[7]={ 11400 , 6210 , 6550 , 2820 , 1050 , 1690 , 770 };

        a=A[type];
        b=B[type];
        c=C[type];
        d=D[type];

        // calcul QMax en fonction de profondeur canal : QMax=Q(prof) avec profondeur importé du fichier (ProfF) ou profondeur saisie/renseignée (ProfR)
        if (this->ProfR==0) {Profondeur=this->ProfF;}
        else {Profondeur=this->ProfR;}

        QMax = a*(Profondeur/1000) + b*pow(Profondeur/1000,2) + c*pow(Profondeur/1000,3) + d*pow(Profondeur/1000,4);
        qDebug() << "debit Max= "<<QMax<< Qt::endl;

       // QVector <double> axeY2;          // axe debit
        for (int i=0; i<this->dataY.size(); i++)
        {
            Q=a*(this->dataY[i]/1000) + b*pow((this->dataY[i])/1000,2) + c*pow((this->dataY[i])/1000,3) + d*pow((this->dataY[i])/1000,4);
          //  qDebug() << "debit= "<<Q<< Qt::endl;

            // TODO

            // corriger si Q > QMax alors Q=QMax et afficher Q en invalide
            if (Q>QMax)
            {
                Q=QMax;
                AlerteDebit = AlerteDebit + 1;
            }

            this->dataY2.push_back(Q);
            //qDebug() << "AlerteDebit= "<<AlerteDebit<< Qt::endl;
        }

        this->plotTemp->enableAxis(QwtPlot::yRight);
        this->plotTemp->setAxisTitle(QwtPlot::yRight, labelY2);
        this->courbeQ->setTitle("débit");
        this->courbeQ->setLegendAttribute(QwtPlotCurve::LegendNoAttribute, true);
        this->courbeQ->setPen( Qt::green, 2 );
        this->courbeQ->setYAxis(QwtPlot::yRight);
        this->courbeQ->attach(this->plotTemp);
        this->courbeQ->setRenderHint( QwtPlotItem::RenderAntialiased, true );
        this->courbeQ->setSamples(this->dataX, this->dataY2);
    }

    //****************************************************************************************************************************



    this->plotTemp->replot();

    // affichage
    this->plotTemp->show();

}


void VisuSetierHE::afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y, QVector <QString> DI, QVector<QString> DA)
{
    if(this->choixDate==0) // "date initiale" selectionnée
    {
        double date = X[0];
        QDateTime t1 = QDateTime::fromTime_t((int)date);
        QString dateDebut = t1.toString("yyyy/MM/dd hh:mm:ss");

        ui->lineEdit_dateInvalide->setText(QString::number(DI.size()));
        infoFicSetierHE->setNBdateInvalide(DI.size());

        //*****************************************************

          // TODO traiter date Alerte dans infoFicSetierHE

        //*****************************************************

        ui->lineEdit_dateAlerte->setText(QString::number(DA.size()));

        for(int i=0 ; i< DI.size(); i++)
        {
            infoFicSetierHE->dateInvalide(DI[i]);
        }

        ui->lineEdit_date->setText(dateDebut);
        infoFicSetierHE->setdateDebut(dateDebut);


        this->remplirtabX(X);
        this->remplirtabY(Y);
        this->afficher(labelX, labelY);
    }

    if((this->choixDate==1) | (this->choixDate==2)) // 1 = "saisir date" selectionnée , 2 = "date fichier WIFI" selectionnée
    {
        double date;
        double DTD = double(this->DT.toSecsSinceEpoch());
        QDateTime QDT;

        // nouveaux vecteurs en fonction de date de début de courbe
        QVector <double> X1;
        QVector <double> Y1;
        QVector <QString> DI1; // Dates invalides sur la periode
        QVector <QString> DA1; // Dates alerte sur la periode


      //*****************************************************

        // TODO traiter date Alerte

      //*****************************************************



        //chercher dans X date de début de courbe immediatement superieur à dateDebut
        for (int i = 0; i < X.size(); ++i)
        {
            date = X[i];

            if (date > DTD )
            {

                for (int j = i ; j < X.size(); ++j)
                {
                    X1.push_back(X[j]);
                }

                for (int k = i ; k < Y.size(); k++)
                {
                    Y1.push_back(Y[k]);
                }

                i=X.size();
            }
       }


         // cherher les dates invalides uniquement dans cette periode
         for (int i=0; i<DI.size() ; i++)

         {
             QDT = QDateTime::fromString(DI[i],"yyyy/MM/dd hh:mm:ss");

             if ( QDT > DT )
                    {
                       for (int j=i; j<DI.size(); j++)
                       {
                         DI1.push_back(DI[j]);
                       }

                       i=DI.size();
                    }
          }


          ui->lineEdit_dateInvalide->setText(QString::number(DI1.size()));
          infoFicSetierHE->setNBdateInvalide(DI1.size());



          // cherher les dates Alerte uniquement dans cette periode
          for (int i=0; i<DA.size() ; i++)

          {
              QDT = QDateTime::fromString(DA[i],"yyyy/MM/dd hh:mm:ss");

              if ( QDT > DT )
                     {
                        for (int j=i; j<DA.size(); j++)
                        {
                          DA1.push_back(DA[j]);
                        }

                        i=DA.size();
                     }
           }


           ui->lineEdit_dateAlerte->setText(QString::number(DA1.size()));


          //*****************************************************

            // TODO traiter date Alerte dans infoFicSetierHE

          //*****************************************************
            //  infoFicSetierHE->setNBdateInvalide(DI1.size());



          //  traiter date invalides dans infoFicSetierHE

          QString dateDebut = this->DT.toString("yyyy/MM/dd hh:mm:ss");
          ui->lineEdit_date->setText(dateDebut);
          infoFicSetierHE->setdateDebut(dateDebut);

          for(int i=0 ; i< DI1.size(); i++)
          {
             infoFicSetierHE->dateInvalide(DI1[i]);
          }


          // Remplir tableau de données, dates invalides , dates alertes

          this->remplirtabX(X1);
          this->remplirtabY(Y1);
          this->remplirtabI(DI1);
          this->remplirtabA(DA1);

          // Affichage

          this->afficher(labelX, labelY);

    }
}





void VisuSetierHE::ouvrirInfoFichier()
{
    this->infoFicSetierHE->setNomFichier(this->nomfichierDATA);
    this->infoFicSetierHE->show();
}


VisuSetierHE::~VisuSetierHE()
{
    delete ui;
}
