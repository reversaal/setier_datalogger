/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : infofichierSetierHE.cpp
# -------------------------------------------------------
#  13/05/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#include "infofichierSetierHE.h"
#include "ui_infofichierSetierHE.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDateTime>

InfoFichierSetierHE::InfoFichierSetierHE(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoFichierSetierHE)
{
    ui->setupUi(this);
    connect(ui->pushButton_voirFic,SIGNAL(clicked(bool)),this,SLOT(voirFichier()));
}

void InfoFichierSetierHE::setNomFichier(QString F)
{
    ui->lineEdit_nomFic->setText(F);
    this->NomFic=F;
}


void InfoFichierSetierHE::setNBdateInvalide(int N)
{
     ui->lineEditDateInvalide->setText(QString::number(N));
    this->NBdateInvalide=N;
}

void InfoFichierSetierHE::dateInvalide(QString D)
{
    ui->listeDateInvalide->append(D);
    DInv.push_back(D);
}

void InfoFichierSetierHE::setdateDebut(QString Db)
{
    ui->lineEdit_date->setText(Db);
    this->dateDebut=Db;
    //qDebug() << "InfoFichierSetierHE::setdateDebut = "<<Db << Qt::endl;

}

void InfoFichierSetierHE::voirFichier()
{
    QString texte;
    QFile fic(this->NomFic);  //Ouverture du fichier "nomfichier" en lecture

    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      QTextStream flux(&fic);
        texte = flux.readAll();
        ui->textEdit_fic->setText(texte);

        //  affichage date invalide dans fichier

        QTextCharFormat format;
        format.setBackground(Qt::red);
        format.setFontWeight(QFont::Bold);

        QTextCharFormat format2;
        format2.setBackground(Qt::darkGreen);
        format2.setFontWeight(QFont::Bold);

        QTextDocument *doc = ui->textEdit_fic->document();
        QTextCursor cursor, cursor2;
        QString date ;

        //qDebug() << "this->dateDebut = "<<this->dateDebut << Qt::endl;

        cursor= doc->find(this->dateDebut,0);
      //  qDebug() << "cursor = "<<cursor.position() << Qt::endl;

        cursor2=cursor;
        cursor2.setCharFormat(format2);

        for (int i = 0; i < DInv.size(); ++i)
        {
            date= DInv[i];
            cursor= doc->find(date,cursor);
            cursor.setCharFormat(format);
        }

    }

   else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }

}

InfoFichierSetierHE::~InfoFichierSetierHE()
{
    delete ui;
}
