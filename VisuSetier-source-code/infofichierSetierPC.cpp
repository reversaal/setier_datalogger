/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : infofichierSetierPC.cpp
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#include "infofichierSetierPC.h"
#include "ui_infofichierSetierPC.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDateTime>

InfoFichierSetierPC::InfoFichierSetierPC(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoFichierSetierPC)
{
    ui->setupUi(this);
    connect(ui->pushButton_voirFic,SIGNAL(clicked(bool)),this,SLOT(voirFichier()));
}

void InfoFichierSetierPC::setNomFichier(QString F)
{
   ui->lineEdit_nomFic->setText(F);
   this->NomFic=F;
}

/*

void InfoFichierSetierPC::setNBdateInvalide(int N)
{
    ui->lineEditDateInvalide->setText(QString::number(N));
    this->NBdateInvalide=N;
}

void InfoFichierSetierPC::dateInvalide(QString D)
{
    ui->listeDateInvalide->append(D);
    DInv.push_back(D);
}

*/

void InfoFichierSetierPC::setdateDebut(QString Db)
{
    ui->lineEdit_date->setText(Db);
    this->dateDebut=Db;
    //qDebug() << "InfoFichierSetierHE::setdateDebut = "<<Db << Qt::endl;

}

void InfoFichierSetierPC::voirFichier()
{

    QString texte;
    QFile fic(this->NomFic);  //Ouverture du fichier "nomfichier" en lecture

    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream flux(&fic);
          texte = flux.readAll();
          ui->textEdit_fic->setText(texte);

          //  affichage date debut dans fichier

          QTextCharFormat format;
          format.setBackground(Qt::darkGreen);
          format.setFontWeight(QFont::Bold);

          QTextDocument *doc = ui->textEdit_fic->document();
          QTextCursor cursor;

          //qDebug() << "this->dateDebut = "<<this->dateDebut << Qt::endl;

          cursor= doc->find(this->dateDebut,0);
        //  qDebug() << "cursor = "<<cursor.position() << Qt::endl;

          cursor.setCharFormat(format);

       }

     else
      {
          cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
      }


}

InfoFichierSetierPC::~InfoFichierSetierPC()
{
    delete ui;
}
