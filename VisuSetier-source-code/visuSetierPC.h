/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierPC.h
# -------------------------------------------------------
#  15/11/2022     |   Valérie QUATELA    | Version 0.5 |
##########################################################*/

#ifndef VISUSETIERPC_H
#define VISUSETIERPC_H

#include <QDialog>
#include <iostream>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_date_scale_draw.h>
#include "infofichierSetierPC.h"
#include <qwt_text.h>

namespace Ui {
class VisuSetierPC;
}

class VisuSetierPC : public QDialog
{
    Q_OBJECT

public:
    explicit VisuSetierPC(QWidget *parent = nullptr);
    ~VisuSetierPC();
    void init();
        void setNomfichierDATA(QString s);
        void setNomfichierETALON(QString s);
        void setChoixDate(int choixDate);
        void setdateDebut(QDateTime DateTime);
        void setCadence(QString cadence);
        void traiterFichierSetierPC_PH();
        void traiterFichierSetierPC_Redox();
        void traiterFichierSetierPC_O2();
        void traiterFichierSetierPC_Conducti();
        void traiterFichierSetierPC_Temp();
        void remplirtabX(QVector <double> tabX);
        void remplirtabY(QVector <double> tabY);
        void remplirtabY2(QVector <double> tabY2);
        void remplirtabXE(QVector <double> tabE);
        void remplirtabYE();
        //void remplirtabI(QVector <QString> tabI);
        //void remplirtabA(QVector <QString> tabA);
        void afficher(QString labelX, QString labelY);

private:
        Ui::VisuSetierPC *ui;
        QString  nomfichierDATA;
        QString  nomfichierETALON;
        double cadence;             // cadence des mesures
        int choixDate;

        QVector <double> dataX;     // tableaux de point X = date/heure
        QVector <double> dataY;     // tableaux de point Y = valeur Capteur
        QVector <double> dataY2;    // tableaux de point Y2 = valeur Tension Capteur
        QVector <double> dataXDE;   // tableaux de point XDE = date/heure Date ETALON
        QVector <double> dataYDE;   // tableaux de point YDE = marque Date ETALON


        /* QVector <QString> dataI;    // tableaux de I = date Invalide
           QVector <double> dataXDI;   // tableaux de point X Date Invalide : date de mesure
           QVector <double> dataYDI;   // tableaux de point Y Date Invalide : hauteur d'eau = 0

           QVector <QString> dataA;    // tableaux de A = date Alerte
           QVector <double> dataXDA;   // tableaux de point X Date Alerte = date de mesure
           QVector <double> dataYDA;   // tableaux de point Y Date Alerte = hauteur d'eau


       */

        QDateTime DT;                   // date de debut de courbe
        QwtPlot *plotTemp;              // plot Qwt date
        QwtPlotCurve *courbe;           // courbe capteur
        QwtPlotCurve *courbeTension;    // courbe tension capteur
        QwtPlotCurve *courbeEtalon;     // courbe date d'étalonnage capteur


     //QwtPlotCurve *courbeDI;     // courbe date invalide
     //QwtPlotCurve *courbeDA;     // courbe date Alerte


        InfoFichierSetierPC *infoFicSetierPC; // fenetre information

        void afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y, QVector <double> Y2, QVector <double> DE) ; //, QVector<QString> DI, QVector<QString> DA);


    private slots:
        void ouvrirInfoFichier();
    };

    class TimeScaleDraw: public QwtScaleDraw
    {

        virtual QwtText label(double v) const
        {
            QDateTime t = QDateTime::fromTime_t((int)v);	//cf fromTime_t
            return t.toString("dd/MM/yyyy\nhh:mm:ss");
        }


    };

#endif // VISUSETIERPC_H
