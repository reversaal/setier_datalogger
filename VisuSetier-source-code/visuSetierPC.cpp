/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierPC.cpp
# -------------------------------------------------------
#  15/11/2022     |   Valérie QUATELA    | Version 0.5 |
##########################################################*/

#include "visuSetierPC.h"
#include "ui_visuSetierPC.h"

#include <qwt_scale_draw.h>
#include <QDateTime>
#include <qwt_date.h>
#include <qwt_plot_zoomer.h>
#include <math.h>
#include <QDebug>
#include <QFile>

VisuSetierPC::VisuSetierPC(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VisuSetierPC)
{
    ui->setupUi(this);
    init();
}

void VisuSetierPC::init()
{
    choixDate=0;

    plotTemp = new QwtPlot;                 // plot Qwt date
    courbe = new QwtPlotCurve;              // courbe capteur
    courbeTension = new QwtPlotCurve;       // courbe tension
    courbeEtalon = new QwtPlotCurve;       // courbe Etalon


 //   courbeDI = new QwtPlotCurve;    // courbe date invalide
 //   courbeDA = new QwtPlotCurve;    // courbe date Alerte


    infoFicSetierPC = new InfoFichierSetierPC; // date invalide

    ui->verticalLayout_courbe->addWidget(plotTemp);
    connect(ui->pushButton_detail, SIGNAL(clicked(bool)),this,SLOT(ouvrirInfoFichier()));

}

void VisuSetierPC::setNomfichierDATA(QString s)
{
    ui->lineEdit_nomFic->setText(s);
    this->nomfichierDATA=s;
}

void VisuSetierPC::setNomfichierETALON(QString s)
{
    this->nomfichierETALON=s;
}

void VisuSetierPC::setChoixDate(int cd)
{
    this->choixDate=cd;
}

void VisuSetierPC::setCadence(QString cadence)
{
    this->cadence=cadence.toDouble();
    ui->lineEdit_cadence->setText(cadence);
}

void VisuSetierPC::setdateDebut(QDateTime DateTime)
{
    this->DT=DateTime;
}

void VisuSetierPC::traiterFichierSetierPC_PH()
{
    qDebug() << "traitement Capteur_PH"<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY;          // axe capteur
    QVector <double> axeY2;          // axe tension capteur
    QVector <double> dateEtalon;    // date ETALON

    //QVector <QString> dateInv;      // vecteur date invalide
    //QVector <QString> dateAlerte;   // vecteur date alerte

    // traitement date d'étalonnage

    QFile ficE(this->nomfichierETALON);  //Ouverture du fichier "nomfichier" en lecture

    if(ficE.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream fluxE(&ficE);

        QString mot;                        // mot courant
        QString date;                       // date + heure
        double coefA=0, coefB=0;            // coef A et coef B d'étalonnage pour Capteur_Conductivite
        double coefA_pre=0, coefB_pre=0;    // coef A et coef B ligne precedente
        QDateTime dateTime;                 // date format dateTime
        double DTD;                         // date format double utc
        bool tagEndE = false;               // presence tag fin des data


        while(! fluxE.atEnd())
            {

                fluxE >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {

                    qDebug() << "START_DATA_Etalon "<< Qt::endl;
                    while(! fluxE.atEnd() & !tagEndE)
                    {
                        coefA_pre=coefA;
                        coefB_pre=coefB;

                        fluxE >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            qDebug() << "fin de traitement _Etalon "<< Qt::endl;
                            tagEndE=true;

                         }

                        else        //remplissage du tableau de valeur date
                         {

                            date = mot ;    // lecture du mot1 = date/heure
                            fluxE >> mot;
                            date+=" ";
                            date+=mot;

                            // lecture de la ligne jusqu'à coefA
                            for (int i = 0; i< 2 ; i++){fluxE >> mot;}
                            coefA = mot.toDouble();
                           // qDebug() << "coefA "<< coefA<< Qt::endl;
                            fluxE >> mot;
                            fluxE >> mot;
                            coefB = mot.toDouble();
                            //qDebug() << "coefB "<< coefB<< Qt::endl;

                            // condition retenir date etalon

                            if (((coefA_pre!=coefA)&(coefA!=0))|((coefB_pre!=coefB)&(coefB!=0)))
                            {

                                dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                                DTD = double(dateTime.toSecsSinceEpoch());
                                dateEtalon.push_back(DTD);

                            }

                              for (int i = 0; i< 13 ; i++){fluxE >> mot;} // retour mot1
                            }
                        }
                   }
        }
        ficE.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier etalon en lecture." << endl;
    }

    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    for (int i = 0; i< 30 ; i++) {flux >> mot;}
                    setCadence(mot);
                  }
             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;           // date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double mesure;          // Mesure
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;


                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Date", "PH" , axeX,axeY,axeY2, dateEtalon);//,dateInv,dateAlerte);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;

                               // dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm");
                              dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                               //dateTime = QDateTime::fromString(date,"dd/MM/yyyy hh:mm");
                               DTD = double(dateTime.toSecsSinceEpoch());

                               axeX.push_back(DTD);


                           // lecture de la ligne jusqu'à mot6=mesure
                           for (int i = 0; i< 6 ; i++){flux >> mot;}
                           mesure = mot.toDouble();

                           // traitement des erreurs sur h
                        /*   if( (h < 0) | (h > this->DistF))
                           {
                               h =0 ;
                               dateInv.push_back(date);
                              // qDebug() << "date INV= "<<date<< Qt::endl;
                           }

                           if( (h > this->ProfF) & (h <= this->DistF))
                           {
                              dateAlerte.push_back(date);
                            //  qDebug() << "date ALERTE= "<<date<< Qt::endl;
                           }

                        */

                           axeY.push_back(mesure);
                           flux >> mot;
                           flux >> mot;
                           mesure = mot.toDouble();
                           axeY2.push_back(mesure);
                           for (int i = 0; i< 21 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
     }
}

void VisuSetierPC::traiterFichierSetierPC_Redox()
{
    qDebug() << "traitement Capteur_Redox "<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY;          // axe capteur
    QVector <double> axeY2;          // axe tension capteur
    QVector <double> dateEtalon;    // date ETALON

    //QVector <QString> dateInv;      // vecteur date invalide
    //QVector <QString> dateAlerte;   // vecteur date alerte

    // traitement date d'étalonnage

    QFile ficE(this->nomfichierETALON);  //Ouverture du fichier "nomfichier" en lecture

    if(ficE.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream fluxE(&ficE);

        QString mot;                        // mot courant
        QString date;                       // date + heure
        double coefA=0, coefB=0;            // coef A et coef B d'étalonnage pour Capteur_Conductivite
        double coefA_pre=0, coefB_pre=0;    // coef A et coef B ligne precedente
        QDateTime dateTime;                 // date format dateTime
        double DTD;                         // date format double utc
        bool tagEndE = false;               // presence tag fin des data


        while(! fluxE.atEnd())
            {

                fluxE >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {

                    qDebug() << "START_DATA_Etalon "<< Qt::endl;
                    while(! fluxE.atEnd() & !tagEndE)
                    {
                        coefA_pre=coefA;
                        coefB_pre=coefB;

                        fluxE >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            qDebug() << "fin de traitement _Etalon "<< Qt::endl;
                            tagEndE=true;

                         }

                        else        //remplissage du tableau de valeur date
                         {

                            date = mot ;    // lecture du mot1 = date/heure
                            fluxE >> mot;
                            date+=" ";
                            date+=mot;

                            // lecture de la ligne jusqu'à coefA
                            for (int i = 0; i< 6 ; i++){fluxE >> mot;}
                            coefA = mot.toDouble();
                           // qDebug() << "coefA "<< coefA<< Qt::endl;
                            fluxE >> mot;
                            fluxE >> mot;
                            coefB = mot.toDouble();
                            //qDebug() << "coefB "<< coefB<< Qt::endl;

                            // condition retenir date etalon

                            if (((coefA_pre!=coefA)&(coefA!=0))|((coefB_pre!=coefB)&(coefB!=0)))
                            {

                                dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                                DTD = double(dateTime.toSecsSinceEpoch());
                                dateEtalon.push_back(DTD);

                            }

                              for (int i = 0; i< 9 ; i++){fluxE >> mot;} // retour mot1
                            }
                        }
                   }
        }
        ficE.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier etalon en lecture." << endl;
    }


    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    for (int i = 0; i< 30 ; i++) {flux >> mot;}
                    setCadence(mot);
                  }
             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;           // date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double mesure;          // Mesure
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;
                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Date", "Redox (mV)" , axeX,axeY,axeY2, dateEtalon);//,dateInv,dateAlerte);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;

                              // dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm");
                               dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                              // dateTime = QDateTime::fromString(date,"dd/MM/yyyy hh:mm");
                               DTD = double(dateTime.toSecsSinceEpoch());

                               axeX.push_back(DTD);


                           // lecture de la ligne jusqu'à mot6=mesure
                           for (int i = 0; i< 12 ; i++){flux >> mot;}
                           mesure = mot.toDouble();

                           // traitement des erreurs sur h
                        /*   if( (h < 0) | (h > this->DistF))
                           {
                               h =0 ;
                               dateInv.push_back(date);
                              // qDebug() << "date INV= "<<date<< Qt::endl;
                           }

                           if( (h > this->ProfF) & (h <= this->DistF))
                           {
                              dateAlerte.push_back(date);
                            //  qDebug() << "date ALERTE= "<<date<< Qt::endl;
                           }

                        */

                           axeY.push_back(mesure);
                           flux >> mot;
                           flux >> mot;
                           mesure = mot.toDouble();
                           axeY2.push_back(mesure);
                           for (int i = 0; i< 15 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
     }

}


void VisuSetierPC::traiterFichierSetierPC_O2()
{
    qDebug() << "traitement Capteur_O2 "<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY;          // axe capteur
    QVector <double> axeY2;          // axe tension capteur
    QVector <double> dateEtalon;    // date ETALON

    //QVector <QString> dateInv;      // vecteur date invalide
    //QVector <QString> dateAlerte;   // vecteur date alerte


    // traitement date d'étalonnage

    QFile ficE(this->nomfichierETALON);  //Ouverture du fichier "nomfichier" en lecture

    if(ficE.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream fluxE(&ficE);

        QString mot;                        // mot courant
        QString date;                       // date + heure
        double coefA=0, coefB=0;            // coef A et coef B d'étalonnage pour Capteur_Conductivite
        double coefA_pre=0, coefB_pre=0;    // coef A et coef B ligne precedente
        QDateTime dateTime;                 // date format dateTime
        double DTD;                         // date format double utc
        bool tagEndE = false;               // presence tag fin des data


        while(! fluxE.atEnd())
            {

                fluxE >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {

                    qDebug() << "START_DATA_Etalon "<< Qt::endl;
                    while(! fluxE.atEnd() & !tagEndE)
                    {
                        coefA_pre=coefA;
                        coefB_pre=coefB;

                        fluxE >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            qDebug() << "fin de traitement _Etalon "<< Qt::endl;
                            tagEndE=true;

                         }

                        else        //remplissage du tableau de valeur date
                         {

                            date = mot ;    // lecture du mot1 = date/heure
                            fluxE >> mot;
                            date+=" ";
                            date+=mot;

                            // lecture de la ligne jusqu'à coefA
                            for (int i = 0; i< 10 ; i++){fluxE >> mot;}
                            coefA = mot.toDouble();
                           // qDebug() << "coefA "<< coefA<< Qt::endl;
                            fluxE >> mot;
                            fluxE >> mot;
                            coefB = mot.toDouble();
                            //qDebug() << "coefB "<< coefB<< Qt::endl;

                            // condition retenir date etalon

                            if (((coefA_pre!=coefA)&(coefA!=0))|((coefB_pre!=coefB)&(coefB!=0)))
                            {

                                dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                                DTD = double(dateTime.toSecsSinceEpoch());
                                dateEtalon.push_back(DTD);


                            }

                              for (int i = 0; i< 5 ; i++){fluxE >> mot;} // retour mot1
                            }
                        }
                   }
        }
        ficE.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier etalon en lecture." << endl;
    }


    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    for (int i = 0; i< 30 ; i++) {flux >> mot;}
                    setCadence(mot);
                  }
             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;           // date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double mesure;          // Mesure
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;
                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Date", "O2" , axeX,axeY,axeY2, dateEtalon);//,dateInv,dateAlerte);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;


                           // dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm");
                             dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                              // dateTime = QDateTime::fromString(date,"dd/MM/yyyy hh:mm");
                               DTD = double(dateTime.toSecsSinceEpoch());

                               axeX.push_back(DTD);
                           //     qDebug() << "DTD O2 = "<<DTD<< Qt::endl;

                           // lecture de la ligne jusqu'à mot6=mesure
                           for (int i = 0; i< 18 ; i++){flux >> mot;}
                           mesure = mot.toDouble();

                           // traitement des erreurs sur h
                        /*   if( (h < 0) | (h > this->DistF))
                           {
                               h =0 ;
                               dateInv.push_back(date);
                              // qDebug() << "date INV= "<<date<< Qt::endl;
                           }

                           if( (h > this->ProfF) & (h <= this->DistF))
                           {
                              dateAlerte.push_back(date);
                            //  qDebug() << "date ALERTE= "<<date<< Qt::endl;
                           }

                        */

                           axeY.push_back(mesure);
                           flux >> mot;
                           flux >> mot;
                           mesure = mot.toDouble();
                           axeY2.push_back(mesure);
                           for (int i = 0; i< 9 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
     }



}

void VisuSetierPC::traiterFichierSetierPC_Conducti()
{
    qDebug() << "traitement Capteur_Conductivite "<< Qt::endl;

    QVector <double> axeX;          // axe date/heure
    QVector <double> axeY;          // axe capteur
    QVector <double> axeY2;         // axe tension capteur
    QVector <double> dateEtalon;    // date ETALON

    //QVector <QString> dateInv;      // vecteur date invalide
    //QVector <QString> dateAlerte;   // vecteur date alerte


    // traitement date d'étalonnage

    QFile ficE(this->nomfichierETALON);  //Ouverture du fichier "nomfichier" en lecture

    if(ficE.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream fluxE(&ficE);

        QString mot;                        // mot courant
        QString date;                       // date + heure
        double coefA=0, coefB=0;            // coef A et coef B d'étalonnage pour Capteur_Conductivite
        double coefA_pre=0, coefB_pre=0;    // coef A et coef B ligne precedente
        QDateTime dateTime;                 // date format dateTime
        double DTD;                         // date format double utc
        bool tagEndE = false;               // presence tag fin des data



        while(! fluxE.atEnd())
            {

                fluxE >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {

                    qDebug() << "START_DATA_Etalon "<< Qt::endl;
                    while(! fluxE.atEnd() & !tagEndE)
                    {
                        coefA_pre=coefA;
                        coefB_pre=coefB;

                        fluxE >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            qDebug() << "fin de traitement _Etalon "<< Qt::endl;
                            tagEndE=true;

                         }

                        else        //remplissage du tableau de valeur date
                         {

                            date = mot ;    // lecture du mot1 = date/heure
                            fluxE >> mot;
                            date+=" ";
                            date+=mot;

                            // lecture de la ligne jusqu'à coefA
                            for (int i = 0; i< 14 ; i++){fluxE >> mot;}
                            coefA = mot.toDouble();
                           // qDebug() << "coefA "<< coefA<< Qt::endl;
                            fluxE >> mot;
                            fluxE >> mot;
                            coefB = mot.toDouble();
                            //qDebug() << "coefB "<< coefB<< Qt::endl;

                            // condition retenir date etalon

                            if (((coefA_pre!=coefA)&(coefA!=0))|((coefB_pre!=coefB)&(coefB!=0)))
                            {

                                dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                                DTD = double(dateTime.toSecsSinceEpoch());
                                dateEtalon.push_back(DTD);

                            }

                              fluxE >> mot;
                            }
                        }
                   }
        }
        ficE.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier etalon en lecture." << endl;
    }


    QFile fic(this->nomfichierDATA);  //Ouverture du fichier "nomfichier" en lecture

    // récupération dans fichier DATA : Cadence
    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant

        while(! flux.atEnd())
            {
                flux >> mot;

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    for (int i = 0; i< 30 ; i++) {flux >> mot;}
                    setCadence(mot);
                  }
             }
        fic.close();

    }
    else
    {
     cout << "ERREUR: Impossible d'ouvrir le fichier data en lecture." << endl;
    }


    if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream flux(&fic);

        QString mot;            // mot courant
        QString date;           // date + heure
        QDateTime dateTime;     // date format dateTime
        double DTD;             // date format double utc
        double mesure;          // Mesure
        bool tagStart = false;  // presence tag debut des data
        bool tagEnd = false;    // presence tag fin des data

        while(! flux.atEnd())
            {
                flux >> mot;
                if (mot == "ID_NAME")        // Nom data logger
                {
                    flux >> mot;
                    ui->lineEditNom->setText(mot);
                }

                if (mot == "START_DATA")        //début du traitement des valeurs
                {
                    tagStart = true;
                    qDebug() << "START_DATA "<< Qt::endl;
                    while(! flux.atEnd())
                    {
                        flux >> mot;

                       if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                         {
                            tagEnd = true;

                            qDebug() << "fin de traitement"<< Qt::endl;

                            afficherCourbe("Date", "Conductivite (µs/cm)" , axeX,axeY,axeY2, dateEtalon);//,dateInv,dateAlerte);

                            qDebug() << "fin de fichier"<< Qt::endl;
                            qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                            qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;

                            return;
                         }

                        else        //remplissage du tableau de valeur date
                         {
                            date = mot ;    // lecture du mot1 = date/heure
                            flux >> mot;
                            date+=" ";
                            date+=mot;


                               dateTime = QDateTime::fromString(date,"yyyy/MM/dd hh:mm:ss");
                              // dateTime = QDateTime::fromString(date,"dd/MM/yyyy hh:mm");
                               DTD = double(dateTime.toSecsSinceEpoch());

                               axeX.push_back(DTD);
                               //qDebug() << "DTD conducti = "<<DTD<< Qt::endl;


                           // lecture de la ligne jusqu'à mot6=mesure
                           for (int i = 0; i< 24 ; i++){flux >> mot;}
                           mesure = mot.toDouble();

                           // traitement des erreurs sur h
                        /*   if( (h < 0) | (h > this->DistF))
                           {
                               h =0 ;
                               dateInv.push_back(date);
                              // qDebug() << "date INV= "<<date<< Qt::endl;
                           }

                           if( (h > this->ProfF) & (h <= this->DistF))
                           {
                              dateAlerte.push_back(date);
                            //  qDebug() << "date ALERTE= "<<date<< Qt::endl;
                           }

                        */

                           axeY.push_back(mesure);
                           flux >> mot;
                           flux >> mot;
                           mesure = mot.toDouble();
                           axeY2.push_back(mesure);
                           for (int i = 0; i< 3 ; i++){flux >> mot;} // lecture jusqu'au prochain mot1

                          }

                     }

                 }
             }

     }
     else
     {
      cout << "ERREUR: Impossible d'ouvrir le fichier data en lecture." << endl;
     }




}


void VisuSetierPC::remplirtabX(QVector <double> tabX)
{
    this->dataX=tabX;
}

void VisuSetierPC::remplirtabY(QVector <double> tabY)
{
    this->dataY=tabY;
}

void VisuSetierPC::remplirtabY2(QVector <double> tabY2)
{
    this->dataY2=tabY2;
}

void VisuSetierPC::remplirtabXE(QVector <double> tabE)
{
    this->dataXDE=tabE;
}

void VisuSetierPC::remplirtabYE()
{
    for (int i=0; i<this->dataXDE.size(); i++)
    {  this->dataYDE.push_back(0);}
}

/*
void VisuSetierPC::remplirtabI(QVector <QString> tabI)
{
    this->dataI=tabI;
}

void VisuSetierPC::remplirtabA(QVector <QString> tabA)
{
    this->dataA=tabA;
}

*/

void VisuSetierPC::afficher(QString labelX, QString labelY)
{

    // configuration repere
    this->plotTemp->setGeometry(40, 40, ui->verticalLayout_courbe->geometry().width(), ui->verticalLayout_courbe->geometry().height());
    this->plotTemp->setCanvasBackground( Qt::white ); // couleur de fond
    this->plotTemp->setAxisTitle(QwtPlot::xBottom, labelX);
    this->plotTemp->setAxisTitle(QwtPlot::yLeft, labelY);

    //  conversion des doubles en dates jj/mm/aaaa hh/mm/ss et rotation des labels
    TimeScaleDraw *draw = new TimeScaleDraw();
    //draw->setLabelRotation(-90);
    this->plotTemp->setAxisScaleDraw(QwtPlot::xBottom, draw);


    plotTemp->insertLegend(new QwtLegend(), QwtPlot::TopLegend);


    // creation grille
    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->attach(this->plotTemp );

    // creation zoom
    //QwtPlotZoomer  zoomer = new QwtPlotZoomer(plotTemp.canvas());


    // configuration courbe capteur
    this->courbe->setTitle("capteur");
    this->courbe->setLegendAttribute(QwtPlotCurve::LegendNoAttribute, true);
    this->courbe->setItemAttribute(QwtPlotItem::Legend, true);
    this->courbe->setPen( Qt::blue, 2 );
    this->courbe->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::cyan, QPen(Qt::black), QSize(5, 5))); // style point bleu cyan
    this->courbe->attach(this->plotTemp);
    this->courbe->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    this->courbe->setSamples(this->dataX, this->dataY);

    // configuration courbe tension capteur
    this->plotTemp->enableAxis(QwtPlot::yRight);
    this->plotTemp->setAxisTitle(QwtPlot::yRight, "Tension (mV)");
    this->courbeTension->setTitle("tension");
    this->courbeTension->setLegendAttribute(QwtPlotCurve::LegendNoAttribute, true);
    this->courbeTension->setPen( Qt::green, 2 );
    this->courbeTension->setYAxis(QwtPlot::yRight);
    this->courbeTension->attach(this->plotTemp);
    this->courbeTension->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    this->courbeTension->setSamples(this->dataX, this->dataY2);

    // affichage en rouge des points date etalon : nouvelle courbe
    this->courbeEtalon->setTitle("Etalonnage");
    this->courbeEtalon->setLegendAttribute(QwtPlotCurve::LegendNoAttribute, true);
    this->courbeEtalon->setItemAttribute(QwtPlotItem::Legend, true);
    //this->courbeEtalon->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::red, QPen(Qt::red), QSize(10, 10))); // style point
    this->courbeEtalon->setSymbol(new QwtSymbol(QwtSymbol::Triangle, Qt::red, QPen(Qt::red), QSize(10, 10))); // style point
    this->courbeEtalon->setStyle(QwtPlotCurve::NoCurve); // point
    this->courbeEtalon->attach(this->plotTemp);
    this->courbeEtalon->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    this->courbeEtalon->setSamples(this->dataXDE, this->dataYDE);

/*

    // affichage en rouge des points date erreur : nouvelle courbe
    this->courbeDI->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::red, QPen(Qt::black), QSize(5, 5))); // style point
    this->courbeDI->setStyle(QwtPlotCurve::NoCurve); // point
    for (int i =0 ; i< this->dataY.size(); i++)
    {
        if (this->dataY[i]==0)
        {
            this->dataXDI.push_back(this->dataX[i]);
            this->dataYDI.push_back(this->dataY[i]);
        }
    }
    this->courbeDI->attach(this->plotTemp);
    this->courbeDI->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    this->courbeDI->setSamples(this->dataXDI, this->dataYDI);


    // affichage en bleu cyan des points date Alerte : nouvelle courbe
    this->courbeDA->setSymbol(new QwtSymbol(QwtSymbol::Ellipse, Qt::yellow, QPen(Qt::black), QSize(5, 5))); // style point jaune
    this->courbeDA->setStyle(QwtPlotCurve::NoCurve); // point


    for (int i =0 ; i< this->dataY.size(); i++)
    {
        if( (this->dataY[i] > this->ProfF) & (this->dataY[i] <= this->DistF))
        {
            this->dataXDA.push_back(this->dataX[i]);
            this->dataYDA.push_back(this->dataY[i]);
        }
    }


     this->courbeDA->attach(this->plotTemp);
     this->courbeDA->setRenderHint( QwtPlotItem::RenderAntialiased, true );
     this->courbeDA->setSamples(this->dataXDA, this->dataYDA);

*/



    this->plotTemp->replot();

    // affichage
    this->plotTemp->show();

}

void VisuSetierPC::afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y, QVector <double> Y2, QVector <double> DE ) //, QVector <QString> DI, QVector<QString> DA)
{

    if(this->choixDate==0) // "date initiale" selectionnée
    {
        double date = X[0];
        QDateTime t1 = QDateTime::fromTime_t((int)date);
        QString dateDebut = t1.toString("yyyy/MM/dd hh:mm:ss");

     //   ui->lineEdit_dateInvalide->setText(QString::number(DI.size()));
     //   infoFicSetierPC->setNBdateInvalide(DI.size());

        //----------------------------------------------------

          // TODO traiter date Alerte dans infoFicSetierPC

        //----------------------------------------------------

   /*     ui->lineEdit_dateAlerte->setText(QString::number(DA.size()));

        for(int i=0 ; i< DI.size(); i++)
        {
            infoFicSetierPC->dateInvalide(DI[i]);
        }
*/
        ui->lineEdit_date->setText(dateDebut);
        infoFicSetierPC->setdateDebut(dateDebut);


        this->remplirtabX(X);
        this->remplirtabY(Y);
        this->remplirtabY2(Y2);
        this->remplirtabXE(DE);
        this->remplirtabYE();
        this->afficher(labelX, labelY);
    }

    if((this->choixDate==1) | (this->choixDate==2)) // 1 = "saisir date" selectionnée , 2 = "date fichier WIFI" selectionnée
    {
        double date;
        double DTD = double(this->DT.toSecsSinceEpoch());
        QDateTime QDT;

        // nouveaux vecteurs en fonction de date de DEBUT de courbe
        QVector <double> Xdeb;
        QVector <double> Ydeb;
        QVector <double> Y2deb;
        QVector <double> DEdeb; // Dates ETALON sur la periode

      //  QVector <QString> DIdeb; // Dates invalides sur la periode
      //  QVector <QString> DAdeb; // Dates alerte sur la periode


      //----------------------------------------------------

        // TODO traiter date Alerte

      //----------------------------------------------------



        //chercher dans X date de début de courbe immediatement superieur à dateDebut
        for (int i = 0; i < X.size(); ++i)
        {
            date = X[i];

            if (date > DTD )
            {

                for (int j = i ; j < X.size(); ++j)
                {
                    Xdeb.push_back(X[j]);
                }

                for (int k = i ; k < Y.size(); k++)
                {
                    Ydeb.push_back(Y[k]);
                }

                for (int k = i ; k < Y2.size(); k++)
                {
                    Y2deb.push_back(Y2[k]);
                }

                i=X.size();
            }
       }



 // cherher les dates ETALON uniquement dans cette periode

        for (int i=0; i< DE.size() ; i++)

        {
            date = DE[i];
            qDebug() << "date DE[j]= "<<date<< Qt::endl; // A VOIR !!


            if (date > DTD )
            {

                for (int j = i ; j < DE.size(); ++j)
                {
                    DEdeb.push_back(DE[j]);
                    qDebug() << "date DE[j]= "<<date<< Qt::endl;

                }

                      i=DE.size();
                   }
         }







 /*       // cherher les dates invalides uniquement dans cette periode
         for (int i=0; i<DI.size() ; i++)

         {
             QDT = QDateTime::fromString(DI[i],"yyyy/MM/dd hh:mm:ss");

             if ( QDT > DT )
                    {
                       for (int j=i; j<DI.size(); j++)
                       {
                         DIdeb.push_back(DI[j]);
                       }

                       i=DI.size();
                    }
          }


          ui->lineEdit_dateInvalide->setText(QString::number(DIdeb.size()));
          infoFicSetierHE->setNBdateInvalide(DIdeb.size());



          // cherher les dates Alerte uniquement dans cette periode
          for (int i=0; i<DA.size() ; i++)

          {
              QDT = QDateTime::fromString(DA[i],"yyyy/MM/dd hh:mm:ss");

              if ( QDT > DT )
                     {
                        for (int j=i; j<DA.size(); j++)
                        {
                          DAdeb.push_back(DA[j]);
                        }

                        i=DA.size();
                     }
           }


           ui->lineEdit_dateAlerte->setText(QString::number(DAdeb.size()));


          //----------------------------------------------------

            // TODO traiter date Alerte dans infoFicSetierPC

          //----------------------------------------------------
            //  infoFicSetierPC->setNBdateInvalide(DIdeb.size());



          //  traiter date invalides dans infoFicSetierPC

          QString dateDebut = this->DT.toString("dd/MM/yyyy hh:mm");
          ui->lineEdit_date->setText(dateDebut);
          infoFicSetierPC->setdateDebut(dateDebut);

          for(int i=0 ; i< DIdeb.size(); i++)
          {
             infoFicSetierPC->dateInvalide(DIdeb[i]);
          }

*/

        QString dateDebut = this->DT.toString("yyyy/MM/dd hh:mm");
        ui->lineEdit_date->setText(dateDebut);
        infoFicSetierPC->setdateDebut(dateDebut);

          // Remplir tableau de données, dates invalides , dates alertes

          this->remplirtabX(Xdeb);
          this->remplirtabY(Ydeb);
          this->remplirtabY2(Y2deb);
          this->remplirtabXE(DEdeb);
          this->remplirtabYE();

       // this->remplirtabI(DIdeb);
       // this->remplirtabA(DAdeb);

          // Affichage

          this->afficher(labelX, labelY);

    }



}

void VisuSetierPC::ouvrirInfoFichier()
{
    this->infoFicSetierPC->setNomFichier(this->nomfichierDATA);
    this->infoFicSetierPC->show();
}

VisuSetierPC::~VisuSetierPC()
{
    delete ui;
}
